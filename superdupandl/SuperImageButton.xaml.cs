﻿using System.Windows.Controls;
using System.Windows.Media;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for SuperImageButton.xaml
    /// </summary>
    public partial class SuperImageButton : Button
    {
        public SuperImageButton()
        {
            InitializeComponent();
        }

        public ImageSource Source
        {
            get { return btnImage.Source; }
            set { btnImage.Source = value; }
        }

        public string ButtonText
        {
            get { return btnText.Text; }
            set { btnText.Text = value; }
        }

        public string ToolTipText
        {
            get { return textBlock_ToolTip.Text; }
            set { textBlock_ToolTip.Text = value; }
        }
    }
}