﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for AccountManageWindow.xaml
    /// </summary>
    public partial class AccountManageWindow : Window
    {
        private MyBaiduDownloader _downloader;

        private string _currentUid = MyBaiduDownloader.INVALID_UID;

        public AccountManageWindow(MyBaiduDownloader downloader, MyBaiduDownloader.LoginCredentialConfig credentialConfig)
        {
            this.Owner  = App.Current.MainWindow;
           _downloader = downloader;
            InitializeComponent();
            updateComboBoxAccountInfo();
        }

        // create and init menu items, show the active account, and add other account in the menu
        private void updateComboBoxAccountInfo()
        {
            Dictionary<string, MyBaiduDownloader.DupanLoginCredential> loginCredDict
                = _downloader.getLoginCredentialList();

            // return if no credential at all
            if (loginCredDict != null)
            {
                // we have loginCredDic, so add all of them in the combobox
                foreach (KeyValuePair<string, MyBaiduDownloader.DupanLoginCredential> keyCred in loginCredDict)
                {
                    MyBaiduDownloader.DupanLoginCredential credential = keyCred.Value as MyBaiduDownloader.DupanLoginCredential;

                    //string uname = credential.username;
                    comboBox_accounts.Items.Add(credential);

                    MyBaiduDownloader.DupanLoginCredential activeloginCred = _downloader.getActiveAccountLoginCredential();
                    if (activeloginCred == null)
                    {
                        // !!!  no current active login credential
                    }
                    else
                    {
                        if (credential.uid == activeloginCred.uid)
                        {
                            comboBox_accounts.SelectedItem = credential;
                            _currentUid = credential.uid;
                        }
                    }
                }
            }
            // ========================================
            //finally, add a new Account item
            // If choose this item, will openning a new web browser
            comboBox_accounts.Items.Add(new MyBaiduDownloader.DupanLoginCredential());

            if (comboBox_accounts.SelectedItem == null)
            {
                comboBox_accounts.SelectedItem = comboBox_accounts.Items[0];
            }
        }

        /*
         MenuItem menuitem_SwitchAccount = this.menu.Items[0] as MenuItem;

           //  < MenuItem IsCheckable = "True" Header = "account1" HorizontalAlignment = "Left" Margin = "0,3" />

           //  < MenuItem IsCheckable = "True" Header = "account2" HorizontalAlignment = "Left" Margin = "0,3" />

           Dictionary<string, MyBaiduDownloader.DupanLoginCredential> loginCredDict
               = _myDupanDownloader.getLoginCredentialList();

           //local config file doesn't exist, can not updat menu now
           if (loginCredDict == null)
           {
               return;
           }

           foreach (KeyValuePair<string, MyBaiduDownloader.DupanLoginCredential> cred in loginCredDict)
           {
               MenuItem menuItemAccount_temp = new MenuItem();
               menuItemAccount_temp.Header = cred.Value.username;
               menuItemAccount_temp.IsCheckable = true;
               if (cred.Key == _myDupanDownloader.getActiveAccountLoginCredential().uid)
               {
                   menuItemAccount_temp.IsChecked = true;
                   menuitem_SwitchAccount.Items.Insert(0, menuItemAccount_temp);
               }
           }
          */

        private void buttonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void button_useAccount_Click(object sender, RoutedEventArgs e)
        {
            MyBaiduDownloader.DupanLoginCredential credential;
            credential = comboBox_accounts.SelectedItem as MyBaiduDownloader.DupanLoginCredential;
            if (credential == null)
            {
                MessageBoxBig.show("先选择一个可用的账号.");
                return;
            }

            if (credential.uid == MyBaiduDownloader.INVALID_UID)
            {
                // Choose to use new account
                Console.WriteLine("New Account");
                // open a new browser page to login
                MessageBoxResult res = MessageBoxBig.show("即将打开浏览器度盘登录页面, 请手工登录一次.", "消息", MessageBoxButton.OKCancel);
                if (res == MessageBoxResult.OK)
                {
                    Close();
                    _downloader.openDupanDefaultLoginPageToGetLoginCredential();

                }
                else
                {
                    return;
                }
            }
            else
            {  // choose another existing account cred.
                if (_currentUid == credential.uid)
                {
                    MessageBoxBig.show("这本来就是当前正在使用的账号");
                    Close();
                    // selection is not changed. do nothing
                    Console.WriteLine("selection is not changed. do nothing");
                    return;
                }
                // switch to another existing account credential
                _downloader.setActiveLoginCredential(credential.uid);
                this.Close();
            }
        }

        private void buttonDeleteAccount_Click(object sender, RoutedEventArgs e)
        {
            MyBaiduDownloader.DupanLoginCredential credential;

            if (comboBox_accounts.SelectedItem == null)
            {
                return;
            }
            credential = comboBox_accounts.SelectedItem as MyBaiduDownloader.DupanLoginCredential;

            // clear the comboBox
            //comboBox_accounts.SelectedItem = null;

            if (credential.uid == MyBaiduDownloader.INVALID_UID)
            {
                // Choose to use new account
                Console.WriteLine("Delete a non-existing New Account?? ");
                MessageBoxBig.show("删除新用户?  怎么样删除一个根本不存在的东西?? ");
                return;
            }

            MessageBoxResult resOK = MessageBoxBig.show("请确认, 真的要删除这个账号的登录信息吗?" +
                credential.username + "\n 正在下载的这个账号的有关文件将不能下载.", "注意", MessageBoxButton.OKCancel);
            if (resOK == MessageBoxResult.OK)
            {
                /*if (credential.uid == MyBaiduDownloader.INVALID_UID)
                {
                    _downloader.setActiveLoginCredential(MyBaiduDownloader.INVALID_UID);
                }*/
                bool res = _downloader.removeLoginCredential(credential.uid);

                if (res == true)
                {
                    MessageBoxBig.show("已成功删除 " + credential.username + "的登录信息");
                    comboBox_accounts.Items[comboBox_accounts.SelectedIndex] = null;
                    comboBox_accounts.SelectedItem = null;
                }
            }

            // this.Close();
        }
    }
}