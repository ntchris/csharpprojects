﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for DupanFileListWindow.xaml
    /// </summary>
    public partial class DupanFileListWindow : Window
    {
        private MyBaiduDownloader myBaiduDownloader;//{ get; set; }

        public DupanFileListWindow()
        {
            InitializeComponent();
            this.Loaded += Window_Loaded;
        }

        private void setDataBindingForGuiComponents()
        {
            // DataContext = myDupanDownloader._dupanAccountInfo;

            // for 3 listviews, filelist, pathSelector, and downloadListview
            // listView_fileList.ItemsSource = myDupanDownloader._remoteFileList;

            //listView_fileList.DataContext = myDupanDownloader;
            //!!!listView_fileList.ItemsSource = myDupanDownloader.remoteFileListProperty;

            listview_pathSelector.ItemsSource = myBaiduDownloader._remotePathList;

            listview_download.Items.Clear();
            listview_download.ItemsSource = myBaiduDownloader.getDownloadManager().toDownloadFileListProperty;
            listview_download.DataContext = myBaiduDownloader.getDownloadManager();

            listview_completedlist.Items.Clear();
            listview_completedlist.ItemsSource = myBaiduDownloader.getDownloadManager().completedFileListProperty;
            listview_completedlist.DataContext = myBaiduDownloader.getDownloadManager();


            setDataBindingForUserInfoFromObject(textblock_dupanUsername, myBaiduDownloader._dupanAccountInfo, "username");

            setDataBindingForUserInfoFromObject(textblock_diskQuotaFree, myBaiduDownloader._dupanAccountInfo, "dupanDiskQuotaFreeGBProperty");
            setDataBindingForUserInfoFromObject(textblock_diskQuotaTotal, myBaiduDownloader._dupanAccountInfo, "dupanDiskQuotaTotalGBProperty");


            // setDataBindingForUserInfoFromObject(statusBarDownloadQueueSize, myDupanDownloader._downloadManager, "downloadQueueSizeProperty");

            Binding myBinding;

            //myBinding = new Binding("downloadQueueTotalFileSizeStringProperty");
            statusBarDownloadQueueTotalSize.DataContext = myBaiduDownloader.getDownloadManager();
            //statusBarDownloadQueueTotalSize.SetBinding(ContentProperty, myBinding);

            //myBinding = new Binding("downloadQueueTotalSpeedStringProperty");
            statusBarDownloadQueueTotalSpeed.DataContext = myBaiduDownloader.getDownloadManager();
            //statusBarDownloadQueueTotalSpeed.SetBinding(ContentProperty, myBinding);

            textBlock_downloadingFileCount.DataContext = myBaiduDownloader.getDownloadManager();

            // =============================================
            // binding user profile image avatar
            myBinding = new Binding("bitmapUserAvatarProperty");
            myBinding.Source = myBaiduDownloader._dupanAccountInfo;
            image_useravatar.SetBinding(Image.SourceProperty, myBinding);

            // =============================================
            // binding progressbar free / total
            myBinding = new Binding("dupanDiskQuotaUsedPercentProperty");
            myBinding.Source = myBaiduDownloader._dupanAccountInfo;
            pbar_dupanDiskQuota.SetBinding(ProgressBar.ValueProperty, myBinding);

            // ===============================================
            // binding data source for filelist

            setDataBindingListview_dataSource_to_user_list(listview_filelist, myBaiduDownloader, "remoteFileListProperty");



            // necessary when downloadlist  object update (not modify fields), the whole object changes.
            // setDataBindingForUserListFromObject(listview_download, myDupanDownloader._downloadManager, "toDownloadFileListProperty");
        }

        private void setDataBindingForUserInfoFromObject(TextBlock text, INotifyPropertyChanged obj, string propertyName)
        {
            Binding myBinding = new Binding(propertyName);
            myBinding.Source = obj;
            text.SetBinding(TextBlock.TextProperty, myBinding);
        }

        // let the listview 's source bind to the <>list
        private void setDataBindingListview_dataSource_to_user_list(ListView listview, INotifyPropertyChanged obj, string propertyName)
        {
            Binding myBinding = new Binding(propertyName);
            myBinding.Source = obj;
            listview.SetBinding(ListView.ItemsSourceProperty, myBinding);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            myBaiduDownloader = new MyBaiduDownloader(this);
            setDataBindingForGuiComponents();
            this.textBlock_title.Text =   MyBaiduDownloader.Credit;

            myBaiduDownloader.start();

            //afte start, account info should be available, show account info in menu
            //updateMenuAccountInfo();
        }

        private void checkBoxAll_Checked(object sender, RoutedEventArgs e)
        {
            listview_filelist.SelectAll();
        }

        private void checkBoxAll_unchecked(object sender, RoutedEventArgs e)
        {
            listview_filelist.UnselectAll();
        }

        //for entering folder

        private void listView_fileList_item_OnDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListViewItem lvi = sender as ListViewItem;
            MyBaiduDownloader.RemoteFileItem fileitem = lvi.Content as MyBaiduDownloader.RemoteFileItem;

            Console.WriteLine("Double clicked!!!! " + fileitem.server_filename);
            Console.WriteLine("new path is " + fileitem.path);
            //MyBaiduDownloader.listRemoteFilesInPathAndUpdateGUI(fileitem);

            myBaiduDownloader.enterNewRemotePathAndListAndUpdateGuiAsync(fileitem);

            listview_PathSelectorSelectNone();

            //Console.WriteLine( "selected index is! " + listview_pathSelector.SelectedIndex);
            //the path select listview should select the current path.
        }

        private void OnListViewItemPreviewMouseRightButtonDown(object sender, RoutedEventArgs e)
        {
            e.Handled = true;
        }

        private void listView_fileList_item_onSelected(object sender, RoutedEventArgs e)
        {
            return;
        }

        private void listview_PathSelectorSelectNone()
        {
            listview_pathSelector.SelectedIndex = -1;
        }

        private void buttonReturn_Click(object sender, RoutedEventArgs e)
        {
            listview_PathSelectorSelectNone();
            myBaiduDownloader.remotePathSelectorGoBack();
        }

        private void buttonRefresh_Click(object sender, RoutedEventArgs e)
        {
            myBaiduDownloader.updateWindowsGui_generalInfo_async();
            myBaiduDownloader.refreshCurrentRemotePath_and_UpdateGui_Async();
        }

        private void listview_pathSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedIndex = (sender as ListView).SelectedIndex;

            myBaiduDownloader.jumpBackToRemotePathAndListAndUpdateGui(selectedIndex);
            listview_PathSelectorSelectNone();

            //Console.WriteLine("selectedIndex is:" + selectedIndex);
        }

        // from https://code.msdn.microsoft.com/windowsdesktop/Sorting-a-WPF-ListView-by-5769086f
        private void GridViewColumnHeader_Filename_Click(object sender, RoutedEventArgs e)
        {
            commonSortListviewColumn(sender);
        }

        private void GridViewColumnHeader_size_Click(object sender, RoutedEventArgs e)
        {
            commonSortListviewColumn(sender);
        }

        private void GridViewColumnHeader_datetime_Click(object sender, RoutedEventArgs e)
        {
            commonSortListviewColumn(sender);
        }

        private ListSortDirection _sortDirection;

        // To use this common sort function, itemlist key property to sort 's name , must be the same as the header column's name
        // x:name=
        // also the data template's name must be correct
        // from https://code.msdn.microsoft.com/windowsdesktop/Sorting-a-WPF-ListView-by-5769086f
        //
        private void commonSortListviewColumn(object sender)
        {
            GridViewColumnHeader column = sender as GridViewColumnHeader;
            string columnName = column.Name;

            Console.WriteLine("column header name is " + columnName);

            _sortDirection = _sortDirection == ListSortDirection.Ascending ?
            ListSortDirection.Descending :
            ListSortDirection.Ascending;

            if (_sortDirection == ListSortDirection.Ascending)
            {
                column.Column.HeaderTemplate = Resources[columnName + "ColumnDataTemplateArrowUp"] as DataTemplate;
            }
            else
            {
                column.Column.HeaderTemplate = Resources[columnName + "ColumnDataTemplateArrowDown"] as DataTemplate;
            }

            ICollectionView view = CollectionViewSource.GetDefaultView(listview_filelist.ItemsSource);
            if (view.CanSort)
            {
                view.SortDescriptions.Clear();

                view.SortDescriptions.Add(new SortDescription("isFolder", _sortDirection));

                view.SortDescriptions.Add(new SortDescription(columnName, _sortDirection));
            }
            else
            {
                Console.WriteLine("canSort is false ??");
            }
        }

        private const int TabIndex_Browse = 0;
        private const int TabIndex_Transmission = 1;

        private void showBrowseTab()
        {
            tab_mainwin.SelectedIndex = TabIndex_Browse;
        }

        private void showTransmissionTab()
        {
            tab_mainwin.SelectedIndex = TabIndex_Transmission;
        }

        private void button_downloadManager_Click(object sender, RoutedEventArgs e)
        {
            showTransmissionTab();
        }

        private void button_dupan_Click(object sender, RoutedEventArgs e)
        {
            showBrowseTab();
        }

        private void hideAllTabItemHeader(TabControl tc)
        {
            int tabItemsCount = tc.Items.Count;

            for (int index = 0; index < tabItemsCount; index++)
            {
                TabItem ti = tc.Items[index] as TabItem;
                ti.Visibility = Visibility.Collapsed;
            }
        }

        private void buttonSwitchAccount_Click(object sender, RoutedEventArgs e)
        {

            myBaiduDownloader.showAccountSelectWindow();

            /*AccountManageWindow accountWin = new AccountManageWindow( myBaiduDownloader, myBaiduDownloader.getLoginCredentialConfig());
            Console.WriteLine("vis is " + accountWin.Visibility);
            accountWin.ShowDialog();
            */
        }

        private void menuItemMoveToFirstDownload_Click(object sender, RoutedEventArgs e)
        {
            int count = listview_download.SelectedItems.Count;

            if (count == 0)
            {
                return;
            }

            // if the selected item is already the first one, ignore
            int selectedIndex = listview_download.SelectedIndex;
            if (selectedIndex == 0)
            {
                MyBaiduDownloader.ToDownloadFileItem tdfi =
                listview_download.Items[selectedIndex] as MyBaiduDownloader.ToDownloadFileItem;
                Console.WriteLine("already the first item " + tdfi.filename);
                return;
            }

            foreach (MyBaiduDownloader.ToDownloadFileItem rfi in listview_download.SelectedItems)
            {
                if (rfi == null)
                {
                    return;
                }
                myBaiduDownloader.getDownloadManager().moveToDownloadFirst(rfi);
                Console.WriteLine("moving to first " + rfi.filename);
            }
        }

        private void menuItemMoveToLastDownload_Click(object sender, RoutedEventArgs e)
        {
            int count = listview_download.SelectedItems.Count;

            if (count == 0)
            {
                return;
            }

            // if the selected item is already the first one, ignore
            int selectedIndex = listview_download.SelectedIndex;
            int totalItems = listview_download.Items.Count;
            int lastIndex = totalItems - 1;
            MyBaiduDownloader.ToDownloadFileItem tdfi =
            listview_download.Items[selectedIndex] as MyBaiduDownloader.ToDownloadFileItem;

            if (selectedIndex == lastIndex)
            {
                Console.WriteLine("already the the last item " + tdfi.filename);
                return;
            }

            if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Downloading)
            {
                MessageBoxBig.show("Already downloading, cannot move to last, stop it first.");
                return;
            }

            foreach (MyBaiduDownloader.ToDownloadFileItem rfi in listview_download.SelectedItems)
            {
                if (rfi == null)
                {
                    return;
                }
                myBaiduDownloader.getDownloadManager().moveToDownloadLast(rfi);
                Console.WriteLine("moving to last " + rfi.filename);
            }
        }

        /*
        //Pause is pending , do not download
        private void menuItemPauseDownload_Click(object sender, RoutedEventArgs e)
        {
            foreach (MyBaiduDownloader.ToDownloadFileItem rfi in listview_download.SelectedItems)
            {
                myDupanDownloader._downloadManager.pauseDownloadOneItem(rfi);
            }
        }*/

        private void menuItemStop_Click(object sender, RoutedEventArgs e)
        {
            foreach (MyBaiduDownloader.ToDownloadFileItem tdfi in listview_download.SelectedItems)
            {
                myBaiduDownloader.getDownloadManager().stopDownloadOneItem(tdfi);
            }
        }

        private void downloadList_MenuItemDeleteTaskAndFile_Click(object sender, RoutedEventArgs e)
        {

            if (listview_download.SelectedItems.Count == 0)
            {
                return;
            }

            StringBuilder sb_toremovefilenames = new StringBuilder();
            int toRemoveCount = 0;

            Queue<MyBaiduDownloader.ToDownloadFileItem> deleteQueue = new Queue<MyBaiduDownloader.ToDownloadFileItem>();

            foreach (MyBaiduDownloader.ToDownloadFileItem tdfi in listview_download.SelectedItems)
            {
                deleteQueue.Enqueue(tdfi);
                sb_toremovefilenames.Append(tdfi.localfullpathfilename);
                sb_toremovefilenames.Append("\n");
                toRemoveCount++;
            }


            var res = MessageBoxBig.show("请确认是否真的要取消选定的下载任务并且删除已下载的文件吗? 共" +
                toRemoveCount + "个文件\n" + sb_toremovefilenames.ToString(), "警告", MessageBoxButton.OKCancel);

            if (res != MessageBoxResult.OK)
            {
                return;
            }

            const string Aria2Ext = ".aria2";
            int removedCount = 0;
            sb_toremovefilenames.Clear();
            do
            {
                MyBaiduDownloader.ToDownloadFileItem tdfi = deleteQueue.Dequeue();
                // remove from task list
                myBaiduDownloader.getDownloadManager().removeOneDownloadItemFromDownloadList(tdfi);

                // remove disk files
                if (File.Exists(tdfi.localfullpathfilename))
                {
                    sb_toremovefilenames.Append(tdfi.localfullpathfilename);
                    sb_toremovefilenames.Append("\n");
                    File.Delete(tdfi.localfullpathfilename);
                    Console.WriteLine("Deleting " + tdfi.localfullpathfilename);
                    removedCount++;
                }
                string tempfile = tdfi.localfullpathfilename + Aria2Ext;
                if (File.Exists(tempfile))
                {
                    File.Delete(tempfile);
                }


            } while (deleteQueue.Count >= 1);

            MessageBoxBig.show("已经删除文件 " + removedCount + " 个文件\n" + sb_toremovefilenames.ToString());

        }

        private void menuItemStartDownload_Click(object sender, RoutedEventArgs e)
        {
            foreach (MyBaiduDownloader.ToDownloadFileItem tdfi in listview_download.SelectedItems)
            {
                gui_start_download_item(tdfi);
            }
        }


        private void menuItemRefreshLink_Click(object sender, RoutedEventArgs e)
        {
        }

        private void menuItemFileCheck_Click(object sender, RoutedEventArgs e)
        {

            if (listview_completedlist.SelectedItems.Count <= 0)
            {
                return;
            }

            foreach (MyBaiduDownloader.ToDownloadFileItem tdfi in listview_completedlist.SelectedItems)
            {
                myBaiduDownloader.getDownloadManager().checkMd5Async(tdfi);

            }



        }

        private void menuItemRemoveTask_Click(object sender, RoutedEventArgs e)
        {

            if (listview_download.SelectedItems.Count <= 0)
            {
                return;
            }

            MyBaiduDownloader.ToDownloadFileItem tdfi;

            while (listview_download.SelectedItems.Count > 0)
            {
                tdfi = listview_download.SelectedItem as MyBaiduDownloader.ToDownloadFileItem;

                myBaiduDownloader.getDownloadManager().removeOneDownloadItemFromDownloadList(tdfi);
            }

            myBaiduDownloader.getDownloadManager().saveDownloadTaskList();
        }


        private void gui_start_download_item(MyBaiduDownloader.ToDownloadFileItem tdfi )
        {
            if (tdfi == null)
            {
                return;
            }

            myBaiduDownloader.getDownloadManager().startDownloadOneItem(tdfi);
        }

        private void embeddedButtonPlay_Click(object sender, RoutedEventArgs e)
        {
            MyBaiduDownloader.ToDownloadFileItem tdfi = getTDFI_From_EmbeddedButtonClickSender(sender);
            selectOnlyOneListviewItemWhenAnyEmbbededButtonClicked(listview_download, tdfi);
            gui_start_download_item(tdfi);


        }

        /*
        // Pause is pending, button is ||, change to pending , do not download, change to pending,
        // pending is wait for next available chance, auto start download (for MaxDownload list)
        private void embeddedButtonPause_Click(object sender, RoutedEventArgs e)
        {
            MyBaiduDownloader.ToDownloadFileItem tdfi = getTDFI_From_EmbeddedButtonClickSender(sender);
            selectOnlyOneListviewItemWhenAnyEmbbededButtonClicked(listview_download, tdfi);

            if (tdfi == null)
            {
                return;
            }
            if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Downloading)
            {
                Console.WriteLine("GUI item change from download to pending.");
                myDupanDownloader._downloadManager.pauseDownloadOneItem(tdfi);
                //pause this item and the next pending item will start.
                return;
            }

            if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Pending)
            {
                Console.WriteLine("already pause");
                return;
            }

            if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Stop)
            {
                Console.WriteLine("item change from stop to pending");
                return;
            }
        }

        */

        private void embeddedButtonStop_Click(object sender, RoutedEventArgs e)
        {
            MyBaiduDownloader.ToDownloadFileItem tdfi = getTDFI_From_EmbeddedButtonClickSender(sender);

            selectOnlyOneListviewItemWhenAnyEmbbededButtonClicked(listview_download, tdfi);

            if (tdfi == null)
            {
                return;
            }
            //if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Downloading)
            // a pending item can be stop
            Console.WriteLine("item change from download to pending.");
            myBaiduDownloader.getDownloadManager().stopDownloadOneItem(tdfi);
            return;
        }

        private static Action EmptyDelegate = delegate () { };

        private void embeddedButtonRemoveTask_Click(object sender, RoutedEventArgs e)
        {
            MyBaiduDownloader.ToDownloadFileItem tdfi = getTDFI_From_EmbeddedButtonClickSender(sender);

            if (tdfi == null)
            {
                return;
            }
            myBaiduDownloader.getDownloadManager().removeOneDownloadItemFromDownloadList(tdfi);
        }

        private void deleteTaskAndFile_Click(object sender, RoutedEventArgs e)
        {
        }

        private void embeddedButtonOpenFolder_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            MyBaiduDownloader.ToDownloadFileItem tdfi = btn.DataContext as MyBaiduDownloader.ToDownloadFileItem;
            Console.WriteLine("local path is " + tdfi.localPath);
            tdfi.openFolder();
        }


        private void embeddedButtonDonePlay_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            MyBaiduDownloader.ToDownloadFileItem tdfi = btn.DataContext as MyBaiduDownloader.ToDownloadFileItem;
            myBaiduDownloader.getDownloadManager().checkMd5Async(tdfi);
        }

        private void embeddedButtonRemoveDoneTask_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            MyBaiduDownloader.ToDownloadFileItem tdfi = btn.DataContext as MyBaiduDownloader.ToDownloadFileItem;
            myBaiduDownloader.getDownloadManager().completedListRemove(tdfi);

        }


        private MyBaiduDownloader.ToDownloadFileItem getTDFI_From_EmbeddedButtonClickSender(object buttonClickSender)
        {
            Button btn = buttonClickSender as Button;
            MyBaiduDownloader.ToDownloadFileItem tdfi = btn.DataContext as MyBaiduDownloader.ToDownloadFileItem;
            return tdfi;
        }

        private void selectOnlyOneListviewItemWhenAnyEmbbededButtonClicked(ListView lv, Object obj)
        {
            //lv.SelectedItem = null;
            //lv.SelectedItem
            lv.SelectedItem = obj;
        }

        private void buttonDownloadConfig_Click(object sender, RoutedEventArgs e)
        {
            DownloadConfigWin dcwin = new DownloadConfigWin(myBaiduDownloader);
            dcwin.ShowDialog();
        }


        private void addSelectedRemoteFilesToDownloadList(string savepath)
        {
            Mouse.OverrideCursor = Cursors.Wait;

           // can not use , blocking . StatusMessageWindow.showMessageStatic("正在处理要下载的目录和文件....");

            foreach (MyBaiduDownloader.RemoteFileItem rfi in listview_filelist.SelectedItems)
            {
                if (rfi == null)
                {
                    return;
                }

                //add files/folders into the download queue
                bool result = myBaiduDownloader.addRemoteFileToDownloadListPublic(rfi, savepath);
                if (result == false)
                {
                    MessageBoxBig.show("failed to get files in the folder. network error ? Try again later.");
                    break;
                }
            }

            myBaiduDownloader.getDownloadManager().saveDownloadTaskList();
            statusBarAddedToQueue.Content = listview_filelist.SelectedItems.Count + " 个文件已加入下载队列";

            listview_filelist.UnselectAll();
            checkBoxAll.IsChecked = false;
            Mouse.OverrideCursor = null;

        }

        private void buttonDownloadRemoteFileListDownload_Click(object sender, RoutedEventArgs e)
        {
            processFileListStartDownloadSelectedRemoteFileItems();
        }

        private void listview_filelist_menuItem_downloadRemoteFileItem(object sender, RoutedEventArgs e)
        {
            processFileListStartDownloadSelectedRemoteFileItems();
        }

        private void processFileListStartDownloadSelectedRemoteFileItems()
        {
            int count = listview_filelist.SelectedItems.Count;
            if (count == 0)
            {
                MessageBoxBig.show("先打钩选择想下载的文件或目录, 然后再按[下载]按钮");
                return;
            }

            // =========================================================================
            DownloadAskSavePathWin saveWin = new DownloadAskSavePathWin();
            saveWin.path = myBaiduDownloader.getDownloadManager()._downloadConfig.saveFileLocalPath;
            saveWin.ShowDialog();
            string savePath = saveWin.path;
            bool? saveAsDefaultPath;
            saveAsDefaultPath = saveWin.checkBox_saveAsDefaultPath.IsChecked;
            if (saveAsDefaultPath==true)
            {
                // save the window's path as config's default path
                myBaiduDownloader.getDownloadManager()._downloadConfig.saveFileLocalPath = saveWin.path;
               // !!!! myBaiduDownloader.saveAppConfig();
            }

            if (savePath == null)
            {
                // cancel button
                return;
            }
            // =========================================================================

            addSelectedRemoteFilesToDownloadList(savePath);

            //should auto start ? no , should not auto start

            // start download all pending items because we added a few files.
            showTransmissionTab();
        }


        private void menuItemCompletedListDeleteTaskAndFile_Click(object sender, RoutedEventArgs e)
        {
            MyBaiduDownloader.ToDownloadFileItem tdfi;
            do
            {
                tdfi = listview_completedlist.SelectedItem as MyBaiduDownloader.ToDownloadFileItem;
                if (tdfi == null)
                {
                    break;
                }

                myBaiduDownloader.getDownloadManager().completedListRemove(tdfi);

            } while (tdfi != null);

            myBaiduDownloader.getDownloadManager().saveCompletedFileList();
        }



        private void menuItemMd5Copy_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (MyBaiduDownloader.ToDownloadFileItem tdfi in listview_completedlist.SelectedItems)
            {

                sb.Append(tdfi.localfullpathfilename);
                sb.Append(" : " + tdfi.localMd5);
                sb.Append("\n");

            }
            Console.WriteLine(sb.ToString());


            // After this call, the data (string) is placed on the clipboard and tagged
            // with a data format of "Text".
            Clipboard.SetData(DataFormats.Text, sb.ToString());

        }


        private void  listview_filelist_menuItem_CopyMd5_RemoteFileItem(object sender, RoutedEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            foreach (MyBaiduDownloader.RemoteFileItem rfi in listview_filelist.SelectedItems)
            {

                sb.Append(rfi.path);
                sb.Append(" : " + rfi.md5);
                sb.Append("\n");
            }
            Console.WriteLine(sb.ToString());


            // After this call, the data (string) is placed on the clipboard and tagged
            // with a data format of "Text".
            Clipboard.SetData(DataFormats.Text, sb.ToString());


        }




        private void menuItemCompletedDeleteTaskAndFile_Click(object sender, RoutedEventArgs e)
        {
            if (listview_completedlist.SelectedItems.Count == 0)
            {
                return;
            }

            StringBuilder sb_toremovefilenames = new StringBuilder();
            int toRemoveCount = 0;

            Queue<MyBaiduDownloader.ToDownloadFileItem> deleteQueue = new Queue<MyBaiduDownloader.ToDownloadFileItem>();

            foreach (MyBaiduDownloader.ToDownloadFileItem tdfi in listview_completedlist.SelectedItems)
            {
                deleteQueue.Enqueue(tdfi);
                sb_toremovefilenames.Append(tdfi.localfullpathfilename);
                sb_toremovefilenames.Append("\n");
                toRemoveCount++;
            }


            var res = MessageBoxBig.show("请确认是否真的要删除选定的任务并且!!删除!!已经下载的文件吗?? 共" +
                toRemoveCount + "个文件\n" + sb_toremovefilenames.ToString(), "警告", MessageBoxButton.OKCancel);

            if (res != MessageBoxResult.OK)
            {
                return;
            }

            int removedCount = 0;
            sb_toremovefilenames.Clear();
            do
            {
                MyBaiduDownloader.ToDownloadFileItem tdfi = deleteQueue.Dequeue();
                // remove from task list

                // remove disk files
                if (File.Exists(tdfi.localfullpathfilename))
                {
                    sb_toremovefilenames.Append(tdfi.localfullpathfilename);
                    sb_toremovefilenames.Append("\n");
                    myBaiduDownloader.getDownloadManager().deleteDownloadedFileAndRedownload(tdfi);
                    removedCount++;
                }

            } while (deleteQueue.Count >= 1);

            MessageBoxBig.show("已经删除文件 " + removedCount + " 个文件\n" + sb_toremovefilenames.ToString());

        }



        private void menuItemReDownload_Click(object sender, RoutedEventArgs e)
        {
            MyBaiduDownloader.ToDownloadFileItem tdfi = listview_completedlist.SelectedItem as MyBaiduDownloader.ToDownloadFileItem;

            myBaiduDownloader.getDownloadManager().deleteDownloadedFileAndRedownload(tdfi);
        }

        private void listview_completedlist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ListView lv = sender as ListView;

            MyBaiduDownloader.ToDownloadFileItem tdfi = lv.SelectedItem as MyBaiduDownloader.ToDownloadFileItem;

            //Binding myBinding;
            //myBinding = new Binding("taskStatusUserInfoTextProperty");
            // completedStatus.DataContext = tdfi;

            // myBinding.Source = tdfi;
            // completedStatus.SetBinding(ContentProperty, myBinding);
            setDataBindingForUserInfoFromObject(completedStatusTextBlock, tdfi, "taskStatusUserInfoTextProperty");


        }

        private void button_about_Click(object sender, RoutedEventArgs e)
        {
            AboutWin win = new AboutWin();
            win.ShowDialog();
        }

        private void button_support_Click(object sender, RoutedEventArgs e)
        {
            SupportMe supportwin = new SupportMe();
            supportwin.ShowDialog();

        }


    }
}