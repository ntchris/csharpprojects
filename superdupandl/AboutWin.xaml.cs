﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for AboutWin.xaml
    /// </summary>
    public partial class AboutWin : Window
    {

        public AboutWin()
        {

            InitializeComponent();
            Owner = App.Current.MainWindow;
            textbox_featurelist.Text = MyBaiduDownloader.featureList();
        }


        private void Button_Support_Click(object sender, RoutedEventArgs e)
        {
            SupportMe supportwin = new SupportMe();
            supportwin.ShowDialog();

        }

        private void Button_OK_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }



       


    }
}
