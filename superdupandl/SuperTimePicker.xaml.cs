﻿using System.Windows.Controls;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for SuperTimePicker.xaml
    /// </summary>
    public partial class SuperTimePicker : UserControl
    {
        private const string AM = "AM";
        private const string PM = "PM";
        private const int HalfDayHours = 12;

        public SuperTimePicker()
        {
            InitializeComponent();
            combobox_limitTime_AMPM.Items.Add(AM);
            combobox_limitTime_AMPM.Items.Add(PM);
            combobox_limitTime_AMPM.SelectedIndex = 0;
            const int Min_Step = 5;
            const int HalfDayHours = 12;
            const int MinutePerHour = 60;
            for (int i = 1; i <= HalfDayHours; i++)
            {
                combobox_limitTime_Hour.Items.Add(numberToDoubleDigitString(i));
            }
            combobox_limitTime_Hour.SelectedIndex = 0;

            for (int i = 0; i < MinutePerHour; i += Min_Step)
            {
                combobox_limitTime_Min.Items.Add(numberToDoubleDigitString(i));
            }
            combobox_limitTime_Min.SelectedIndex = 0;
        }

        private string numberToDoubleDigitString(int i)
        {
            const int Ten = 10;
            string ii = i.ToString();
            // make 1...9 to 01 and 09
            if (i < Ten)
            {
                ii = "0" + ii;
            }
            return ii;
        }

        private string _Text = "pick a time";

        public string Text
        {
            get { return _Text; }
            set { this.label_pickatime.Content = value; }
        }


        // append 0 for 0 to 9.,  1 to 01, 2 to 02 ... 9 to 09
        private string numberToPrettyString(int number)
        {
            string numberString;
            if (number <= 9)
            {
                numberString = "0" + number;
            }
            else
            {
                numberString = number.ToString();
            }

            return numberString;
        }

        public System.TimeSpan timespan
        {
            get
            {
                System.TimeSpan timesp;
                int hour = int.Parse(combobox_limitTime_Hour.Text);
                int min = int.Parse(combobox_limitTime_Min.Text);
                string ampm = combobox_limitTime_AMPM.Text;
                /*
                if (hour == HalfDayHours)
                {
                    hour = 0;
                }

                if (ampm.Equals(PM))
                {
                    hour = hour + 12;
                }
                */

                if ((hour == HalfDayHours) && ampm.Equals(AM))
                {
                    hour = 0; // 12:00 AM is 0:00
                }
                else if ((hour == HalfDayHours) && ampm.Equals(PM))
                {
                    hour = HalfDayHours;  // 12:00PM is 12:00
                }
                else if (ampm.Equals(PM))
                {
                    hour = hour + HalfDayHours;
                }

                timesp = new System.TimeSpan(hour, min, 0);
                return timesp;
            }

            set
            {
                int number = value.Hours;

                if (number == 0)
                {
                    number = HalfDayHours;
                    combobox_limitTime_AMPM.Text = AM;
                }
                else if ((number == HalfDayHours))
                {
                    combobox_limitTime_AMPM.Text = PM;
                }
                else if (number > HalfDayHours)
                {
                    combobox_limitTime_AMPM.Text = PM;
                    number -= HalfDayHours;
                }

                // append zero 1 --> 01, 2-->02
                combobox_limitTime_Hour.Text = numberToPrettyString(number);

                number = value.Minutes;

                combobox_limitTime_Min.Text = numberToPrettyString(number);
            }
        }

        /*double _FontSize = 0;
        public double FontSize
        {
            get { return _FontSize; }
            set { double x = this.label_pickatime.FontSize;
                this.label_pickatime.FontSize = value; }
        }
        */
    }
}