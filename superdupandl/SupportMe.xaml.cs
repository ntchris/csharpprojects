﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for SupportMe.xaml
    /// </summary>
    public partial class SupportMe : Window
    {
        public SupportMe()
        {
            InitializeComponent();

            //doonnaattemmail
           //string donateaddr = " "+" " + "n" + "t" + "c" + "h" + "r" + "i" + "s" + "@" + "h" + "o" + "t"+"m"+"a"+"i"+"l"+"."+"c"+"o"+"m"  + " " + " ";

            //Console.WriteLine(encrypte(donateaddr));


            this.Owner = App.Current.MainWindow;

            tbppd.Text = MyBaiduDownloader.doonnaattemmail();
        }

        private void editbox_selectAll(object sender, RoutedEventArgs e)
        {
            tbppd.SelectAll();

            Clipboard.SetData(DataFormats.Text, tbppd.Text.Trim());

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
