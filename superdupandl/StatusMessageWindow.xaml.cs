﻿using System;
using System.Windows;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for StatusMessageWindow.xaml
    /// </summary>
    ///

    // must call closeStatic() to close the window!

    public partial class StatusMessageWindow : Window
    {
        private static StatusMessageWindow _msgWin;// = new StatusMessageWindow();

        public StatusMessageWindow(Window ownerWin = null)
        {
            // set owner Window of this small window ;
            Owner = Application.Current.MainWindow;

            WindowStartupLocation = WindowStartupLocation.CenterOwner;
            InitializeComponent();
        }

        public void showMessage(string message)
        {
            Owner = App.Current.MainWindow;

            Console.WriteLine("Left " + Owner.Left + ", top " + Owner.Top);
            this.textBlock.Text = message;
            this.ShowDialog();
        }

        // must call closeStatic() to close the window!
        static public void showMessageStatic(string message)
        {
            if (_msgWin == null)
            {
                _msgWin = new StatusMessageWindow();
            }

            _msgWin.textBlock.Text = message;
            _msgWin.ShowDialog();
        }

        static public void closeStatic()
        {
            if (_msgWin == null)
            {
                return;
            }
            _msgWin.Close();
            //   _messageWin.Visibility = Visibility.Collapsed;
            _msgWin = null;
        }
    }
}