﻿using System;
using System.Windows;
using System.Windows.Controls;
using static superdupandl.MyBaiduDownloader;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for DownloadConfigWin.xaml
    /// </summary>
    public partial class DownloadConfigWin : Window
    {
        private AppConfigManager.DownloadConfigAndroid _downloadConfig;
        //private AppConfigManager _appConfigMgr;
        private MyBaiduDownloader _mydownloader;

        public DownloadConfigWin(MyBaiduDownloader mydownloader)
        {
            Owner = Application.Current.MainWindow;
            _mydownloader = mydownloader;

            _downloadConfig = _mydownloader.getDownloadManager()._downloadConfig;

            InitializeComponent();
           // timepicker_start.DataContext = _downloadConfig;
           // timepicker_end.DataContext = _downloadConfig;

            this.DataContext = _downloadConfig;

            this.timepicker_start.timespan = TimeSpan.FromSeconds(_downloadConfig.timespan_SpeedLimitStartSecond );
            this.timepicker_end.timespan = TimeSpan.FromSeconds(_downloadConfig.timespan_SpeedLimitEndSecond);


        }

        private void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private bool validateAllFields()
        {
            string path = textbox_defaultSavePath.Text;

            if (!System.IO.Directory.Exists(path))
            {
                MessageBoxBig.show("省缺下载目录 " + textbox_defaultSavePath.Text + " 不存在, 请选择一个存在的目录.");
                textbox_defaultSavePath.Focus();
                return false;
            }

            if (!validateTextBox(textbox_speedFullNumber))
            {
                return false;
            }

            if (!validateTextBox(textbox_speedlimitK))
            {
                return false;
            }

            if (!validateTextBox(textbox_split))
            {
                return false;
            }

            if (!validateTextBox(textbox_connections))
            {
                return false;
            }

            return true;
        }

        private bool validateTextBox(TextBox tb)
        {
            try
            {
                int res = Int32.Parse(tb.Text);
            }
            catch (Exception  )
            {
                tb.Text = "";
                tb.Focus();
                return false;
            }
            return true;
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            if (validateAllFields())
            {
                /*
                double endTotalSecond = 0;
                if (this.timepicker_end.timespan  < this.timepicker_start.timespan)
                {
                    TimeSpan TimeSpanOneDay = new TimeSpan(24,0,0);
                    // if day end is 2AM very early, need to add 24hours
                    endTotalSecond = TimeSpanOneDay.TotalSeconds;

                }
                */
                _downloadConfig.timespan_SpeedLimitStartSecond = this.timepicker_start.timespan.TotalSeconds;
                _downloadConfig.timespan_SpeedLimitEndSecond = this.timepicker_end.timespan.TotalSeconds;

                _mydownloader.saveAppConfig();

                Close();
            }
            return;
        }

        private void Button_Default_Click(object sender, RoutedEventArgs e)
        {
        }

        private void Button_Browse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();

            //preset the browse win to the current folder, not <Desktop>
            if (System.IO.Directory.Exists(textbox_defaultSavePath.Text))
            {
                fbd.SelectedPath = textbox_defaultSavePath.Text;
            }

            System.Windows.Forms.DialogResult res = fbd.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                textbox_defaultSavePath.Text = fbd.SelectedPath;
            }
        }

        private void updateEstimateLimit1HourDownloadMB(object sender, RoutedEventArgs e)
        {
            Int64 speedLimitK = 0;
            try
            {
                speedLimitK = Int64.Parse(textbox_speedlimitK.Text);
                if (speedLimitK <= 1)
                {
                    // 0 没意义, 最低是1.
                    textbox_speedlimitK.Text = "1";
                    return;
                }

                label_estimateLimit1HoursMB.Content = speedLimitK * 3600 / 1024;
            }
            catch (Exception  )
            {
                MessageBoxBig.show("请只输入数字. 1-9 都行, 其它的不要. 0没意义");
                label_estimateLimit1HoursMB.Content = "0";
            }
        }

        private void updateEstimate1HourDownloadGB(object sender, TextChangedEventArgs e)
        {
            // provide an estimate GB for hourly and daily download
            Int64 speedFullNumber;
            int mbKB=1;
            if (combobox_fullSpeedUnit.Text.Equals("K"))
            {
                mbKB = 1024;
            }

            try
            {
                speedFullNumber = Int64.Parse(textbox_speedFullNumber.Text);
                if (speedFullNumber == 0)
                {
                    label_estimate1HoursGB.Content = "??";
                    label_estimate24HoursGB.Content = "??";
                    return;
                }

                // display
                double hourGB = 3600.0 * speedFullNumber / 1024 / mbKB;
                if (hourGB >= 100)
                {
                    label_estimate1HoursGB.Content = hourGB.ToString("N0");
                }
                else if (hourGB >= 10)
                {
                    label_estimate1HoursGB.Content = hourGB.ToString("N1");
                }
                else
                {
                    label_estimate1HoursGB.Content = hourGB.ToString("N2");
                }

                label_estimate24HoursGB.Content = (Int64)24 * 3600 / 1024 * speedFullNumber / mbKB;
            }
            catch (Exception  )
            {
                MessageBoxBig.show("请只输入数字. 0-9 都行, 其它的不要");
                label_estimate1HoursGB.Content = "0";
                label_estimate24HoursGB.Content = "0";
            }
        }

        private void enableSpeedLimitSetting(object sender, RoutedEventArgs e)
        {
            textbox_speedlimitK.IsEnabled = true;
            timepicker_start.IsEnabled = true;
            timepicker_end.IsEnabled = true;
        }

        private void disableSpeedLimitSetting(object sender, RoutedEventArgs e)
        {
            textbox_speedlimitK.IsEnabled = false;
            timepicker_start.IsEnabled = false;
            timepicker_end.IsEnabled = false;
        }

        private void Button_Reset_UserAgent_Click(object sender, RoutedEventArgs e)
        {
            textbox_useragent.Text = "";
        }

        private void Button_Reset_To_Default_Click(object sender, RoutedEventArgs e)
        {
            _downloadConfig.resetToDefaultValues();
            _mydownloader.saveAppConfig();
            Close();
        }

        private void textboxNumberOnlyOnChange(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            bool valid = validateTextBox(tb);
            if (!valid)
            {
                return;
            }
        }


        private void combobox_fullSpeedUnit_DropDownClosed(object sender, EventArgs e)
        {
            updateEstimate1HourDownloadGB(null, null);
        }


    }
}