﻿using System;
using System.Windows;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for DownloadAskSavePath.xaml
    /// </summary>
    public partial class DownloadAskSavePathWin : Window
    {
        public String path { get; set; }

        public DownloadAskSavePathWin()
        {
            
            InitializeComponent();
            textbox_path.DataContext = this;
            Owner = App.Current.MainWindow;
            DataContext = this;
        }

        private void ButtonBrowse_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.SelectedPath = textbox_path.Text;

            System.Windows.Forms.DialogResult res = fbd.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                textbox_path.Text = fbd.SelectedPath;
                path = textbox_path.Text;
            }
        }

        private void ButtonOK_Click(object sender, RoutedEventArgs e)
        {
            // no need??, already binding.  path = textbox_path.Text;
            Console.WriteLine("save path is " + path);
            
            Close();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            path = null;
            Close();
        }


    }
}