﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace superdupandl
{
    // to do: sort files by name or time , disable main win when waiting , download links

    public class MyBaiduDownloader : INotifyPropertyChanged
    {
        // MyBaiduDownloader

        public const bool DEBUG_ONLINE_All = true;
        public const bool DEBUG_ONLINE_USERINFO = true && DEBUG_ONLINE_All;
        public const bool DEBUG_ONLINE_FILELIST = true && DEBUG_ONLINE_All;
        public const bool DEBUG_ONLINE_GETDOWNLOADLINKS = true && DEBUG_ONLINE_All;
        public const bool DEBUG_USE_TRUE_DOWNLOAD_LINKS = true && DEBUG_ONLINE_All; // if true, use true real download links, false: use fake 127.0.0.1 links
        public const bool DEBUG_TRUE_DOWNLOAD_TO_COMPLETE = true && DEBUG_ONLINE_All;
        public const bool DEBUG_RUN_ARIA2C = true && DEBUG_ONLINE_All;
        public const bool DEBUG_HIDE_ARIA2C_WINDOW = true || DEBUG_ONLINE_All;


        // debug : set to false   ,  normal use : set to false
        public const bool DEBUG_PRINT_RPC_TEXT = false;

        public const int Avatar_Size = 150;
        public const UInt64 BytesPerGigaByte = 1024 * 1024 * 1024;
        public const UInt64 BytesPerMegaByte = 1024 * 1024;
        public const UInt64 BytesPerKiloByte = 1024;
        public const string UnitGB = "GB";
        public const string UnitMB = "MB";
        public const string UnitKB = "KB";
        public const string unitByte = "Byte";
        public const string DIRSIZEDISPLAY = "-"; // for dir, do not display 0 size, just display a -
        public const string ROOTSYMBOL = "/";
        public const string DISPLAY_NAME_FOR_NET_DISK_ROOT = "我的网盘";
        public const string INVALID_UID = "0";



        static string decrypte(string encrypted)
        {
            char[] bytes = encrypted.ToCharArray();
            char[] newb = encrypted.ToCharArray();
            int i = 0;

            foreach (char ch in bytes)
            {
                newb[i] = (char)(ch + '\x05');
                i++;
            }
            return new string(newb);
        }
        static void showStringByteArray(string str, string headertext = "")
        {
            char[] bytes = str.ToCharArray();
            int i = 0;

            if (!string.IsNullOrEmpty(headertext))
            {
                Console.Write(headertext + " ");
            }

            foreach (char ch in bytes)
            {

                Console.Write(" " + (int)bytes[i]);
                i++;
            }
            Console.WriteLine();
        }

        static string encrypte(string text)
        {
            char[] bytes = text.ToCharArray();
            char[] newb = text.ToCharArray();
            int i = 0;

            showStringByteArray(text, "before");

            foreach (char ch in bytes)
            {
                newb[i] = (char)(ch - '\x05');
                Console.Write(" (char) " + (int)newb[i] + ", ");

                i++;
                if ((i % 10) == 0)
                {
                    Console.WriteLine();
                }

            }
            Console.WriteLine();

            string newstr = new string(newb);
            //showStringByteArray(newstr, "after");

            return newstr;
        }


        static public string featureList()
        {
            /*
            string FeatureList = "   超级度盘下载器     \n  本软件功能一览与有关说明\n\n" +
              "1. 快速下载度盘文件, 没有某某极不合理的速度限制\n\n" +
              "2. 超快速的度盘文件浏览速度, 使用了先进的缓存技术, 当多次重复访问度盘的同一目录时, 能瞬间打开度盘的文件列表, 无需等待.\n\n" +
              "3. 网盘浏览文件时候支持按名称, 大小, 日期来排序, 可方便的查找目标文件和目录\n\n" +
              "4. 下载文件列表和完成文件列表自动保存, 关闭软件后, 依然可以继续\n\n" +
              "5. 个性化配置下载选项, 可人工限速, 防止占用太高带宽, 不影响家人室友上网. \n" +
              "    严重注意: 已有多人在网上表示, 超大量的下载度盘文件, 可能导致短时间(几天)封号甚至永久封号. 所以下载量请自我克制, 一天的下载量不要太大. 暂时来看, 一天下载十几二十GB貌似是OK的.\n" +
              "    所以, 如果需要下载大量文件(例如100GB), 请自觉限速为较小合理速度值, 或把大量文件分开几天来下载. \n" +
              "    在下载配置窗口有基于速度限制的24小时下载量估算, 可供参考.\n" +
              "    细水方能长流, 低调才能长久.\n\n" +
              "6. 已下载完毕的文件可以自动并手工再次进行MD5校验, 确保文件无误\n" +
              "   注意: 度盘文件服务端储存的MD5并不总是正确的, 有时候发现本地文件校验计算出的MD5跟服务端不符合, 不一定就是下载错误. 可以进一步检验此文件:\n" +
              "   (1) 如果目录里有sfv等校验文件, 可以校验一遍就能确定了.\n" +
              "   (2) 也可以用度盘官方客户端尝试能否极速上传该文件的方法来鉴定. 如果可以极速上传, 那就证明该文件正确.\n" +
              "   (3) 还可以尝试再次下载该文件一遍, 对比两次下载的本地MD5是否一致, 如果两次都一致, 基本可以证明下载的本地文件的确无误.\n" +
              "   (4) 如果是rar包等, 还可以尝试解压, 如果解压没问题就没问题了.  \n\n" +
              "   所以我们上传文件分享, 应该先用rar打包后再上传, 而不应该把三千个原始文件直接丢上网盘\n\n" +
              "7. 支持多用度盘户账号浏览下载和管理, 可随意切换, 可随时增加新账号和删除旧账号\n\n" +
              "8. 完整度盘账号信息显示, 度盘账号名称, 用户头像, 网盘剩余空间等\n\n" +
              "9. 自动智能处理度盘下载连接, 不会出现莫名其妙的失效或错误\n\n" +
              "10. 全图形界面, 丰富图标, 用户友好\n\n" +
              "11. 纯HTTP下载, 没有BT. (因有些地区的朋友对BT有某些顾虑.) \n\n" +
              "      感谢支持与使用!   \n\n" +
              "  作者   " + doonnaattemmail() + "      \n       2018年 4月29日";
            */
            char[] encoded = new char[]{ (char) 27,  (char) 27,  (char) 27,  (char) 36224,  (char) 32418,  (char) 24225,  (char) 30419,  (char) 19974,  (char) 36728,  (char) 22115,
 (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 5,  (char) 27,  (char) 27,  (char) 26407,  (char) 36714,
 (char) 20209,  (char) 21146,  (char) 33016,  (char) 19963,  (char) 35267,  (char) 19977,  (char) 26372,  (char) 20846,  (char) 35823,  (char) 26121,
 (char) 5,  (char) 5,  (char) 44,  (char) 41,  (char) 27,  (char) 24550,  (char) 36890,  (char) 19974,  (char) 36728,  (char) 24225,
 (char) 30419,  (char) 25986,  (char) 20209,  (char) 39,  (char) 27,  (char) 27804,  (char) 26372,  (char) 26571,  (char) 26571,  (char) 26492,
 (char) 19976,  (char) 21507,  (char) 29697,  (char) 30335,  (char) 36890,  (char) 24225,  (char) 38475,  (char) 21041,  (char) 5,  (char) 5,
 (char) 45,  (char) 41,  (char) 27,  (char) 36224,  (char) 24550,  (char) 36890,  (char) 30335,  (char) 24225,  (char) 30419,  (char) 25986,
 (char) 20209,  (char) 27978,  (char) 35267,  (char) 36890,  (char) 24225,  (char) 39,  (char) 27,  (char) 20346,  (char) 29987,  (char) 20097,
 (char) 20803,  (char) 36822,  (char) 30335,  (char) 32526,  (char) 23379,  (char) 25211,  (char) 26410,  (char) 39,  (char) 27,  (char) 24398,
 (char) 22805,  (char) 27420,  (char) 37320,  (char) 22792,  (char) 35770,  (char) 38377,  (char) 24225,  (char) 30419,  (char) 30335,  (char) 21511,
 (char) 19963,  (char) 30441,  (char) 24400,  (char) 26097,  (char) 39,  (char) 27,  (char) 33016,  (char) 30631,  (char) 38383,  (char) 25166,
 (char) 24315,  (char) 24225,  (char) 30419,  (char) 30335,  (char) 25986,  (char) 20209,  (char) 21010,  (char) 34915,  (char) 39,  (char) 27,
 (char) 26075,  (char) 38651,  (char) 31556,  (char) 24448,  (char) 41,  (char) 5,  (char) 5,  (char) 46,  (char) 41,  (char) 27,
 (char) 32588,  (char) 30419,  (char) 27978,  (char) 35267,  (char) 25986,  (char) 20209,  (char) 26097,  (char) 20500,  (char) 25898,  (char) 25340,
 (char) 25348,  (char) 21512,  (char) 31211,  (char) 39,  (char) 27,  (char) 22818,  (char) 23562,  (char) 39,  (char) 27,  (char) 26080,
 (char) 26394,  (char) 26464,  (char) 25485,  (char) 24202,  (char) 39,  (char) 27,  (char) 21482,  (char) 26036,  (char) 20410,  (char) 30335,
 (char) 26592,  (char) 25209,  (char) 30441,  (char) 26626,  (char) 25986,  (char) 20209,  (char) 21639,  (char) 30441,  (char) 24400,  (char) 5,
 (char) 5,  (char) 47,  (char) 41,  (char) 27,  (char) 19974,  (char) 36728,  (char) 25986,  (char) 20209,  (char) 21010,  (char) 34915,
 (char) 21639,  (char) 23431,  (char) 25099,  (char) 25986,  (char) 20209,  (char) 21010,  (char) 34915,  (char) 33253,  (char) 21155,  (char) 20440,
 (char) 23379,  (char) 39,  (char) 27,  (char) 20846,  (char) 38376,  (char) 36714,  (char) 20209,  (char) 21513,  (char) 39,  (char) 27,
 (char) 20376,  (char) 28977,  (char) 21482,  (char) 20192,  (char) 32482,  (char) 32488,  (char) 5,  (char) 5,  (char) 48,  (char) 41,
 (char) 27,  (char) 20005,  (char) 24610,  (char) 21265,  (char) 37192,  (char) 32617,  (char) 19974,  (char) 36728,  (char) 36868,  (char) 39028,
 (char) 39,  (char) 27,  (char) 21482,  (char) 20149,  (char) 24032,  (char) 38475,  (char) 36890,  (char) 39,  (char) 27,  (char) 38445,
 (char) 27485,  (char) 21339,  (char) 29987,  (char) 22821,  (char) 39635,  (char) 24097,  (char) 23480,  (char) 39,  (char) 27,  (char) 19976,
 (char) 24428,  (char) 21704,  (char) 23473,  (char) 20149,  (char) 23455,  (char) 21446,  (char) 19973,  (char) 32588,  (char) 41,  (char) 27,
 (char) 5,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 20000,  (char) 37320,  (char) 27875,  (char) 24842,  (char) 53,
 (char) 27,  (char) 24045,  (char) 26372,  (char) 22805,  (char) 20149,  (char) 22307,  (char) 32588,  (char) 19973,  (char) 34915,  (char) 31029,
 (char) 39,  (char) 27,  (char) 36224,  (char) 22818,  (char) 37322,  (char) 30335,  (char) 19974,  (char) 36728,  (char) 24225,  (char) 30419,
 (char) 25986,  (char) 20209,  (char) 39,  (char) 27,  (char) 21482,  (char) 33016,  (char) 23543,  (char) 33263,  (char) 30696,  (char) 26097,
 (char) 38383,  (char) 35,  (char) 20955,  (char) 22820,  (char) 36,  (char) 23548,  (char) 21490,  (char) 29973,  (char) 33262,  (char) 27699,
 (char) 20032,  (char) 23548,  (char) 21490,  (char) 41,  (char) 27,  (char) 25147,  (char) 20192,  (char) 19974,  (char) 36728,  (char) 37322,
 (char) 35826,  (char) 33253,  (char) 25100,  (char) 20806,  (char) 21041,  (char) 39,  (char) 27,  (char) 19963,  (char) 22820,  (char) 30335,
 (char) 19974,  (char) 36728,  (char) 37322,  (char) 19976,  (char) 35196,  (char) 22821,  (char) 22818,  (char) 41,  (char) 27,  (char) 26237,
 (char) 26097,  (char) 26464,  (char) 30470,  (char) 39,  (char) 27,  (char) 19963,  (char) 22820,  (char) 19974,  (char) 36728,  (char) 21308,
 (char) 20955,  (char) 20103,  (char) 21308,  (char) 66,  (char) 61,  (char) 35975,  (char) 20279,  (char) 26154,  (char) 74,  (char) 70,
 (char) 30335,  (char) 41,  (char) 5,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 25147,  (char) 20192,  (char) 39,
 (char) 27,  (char) 22909,  (char) 26519,  (char) 38651,  (char) 35196,  (char) 19974,  (char) 36728,  (char) 22818,  (char) 37322,  (char) 25986,
 (char) 20209,  (char) 35,  (char) 20358,  (char) 22909,  (char) 44,  (char) 43,  (char) 43,  (char) 66,  (char) 61,  (char) 36,
 (char) 39,  (char) 27,  (char) 35826,  (char) 33253,  (char) 35268,  (char) 38475,  (char) 36890,  (char) 20021,  (char) 36734,  (char) 23562,
 (char) 21507,  (char) 29697,  (char) 36890,  (char) 24225,  (char) 20535,  (char) 39,  (char) 27,  (char) 25105,  (char) 25221,  (char) 22818,
 (char) 37322,  (char) 25986,  (char) 20209,  (char) 20993,  (char) 24315,  (char) 20955,  (char) 22820,  (char) 26464,  (char) 19974,  (char) 36728,
 (char) 41,  (char) 27,  (char) 5,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 22307,  (char) 19974,  (char) 36728,
 (char) 37192,  (char) 32617,  (char) 31378,  (char) 21470,  (char) 26372,  (char) 22517,  (char) 20105,  (char) 36890,  (char) 24225,  (char) 38475,
 (char) 21041,  (char) 30335,  (char) 45,  (char) 47,  (char) 23562,  (char) 26097,  (char) 19974,  (char) 36728,  (char) 37322,  (char) 20267,
 (char) 31634,  (char) 39,  (char) 27,  (char) 21482,  (char) 20374,  (char) 21437,  (char) 32766,  (char) 41,  (char) 5,  (char) 27,
 (char) 27,  (char) 27,  (char) 27,  (char) 32449,  (char) 27695,  (char) 26036,  (char) 33016,  (char) 38266,  (char) 27964,  (char) 39,
 (char) 27,  (char) 20297,  (char) 35838,  (char) 25160,  (char) 33016,  (char) 38266,  (char) 20032,  (char) 41,  (char) 5,  (char) 5,
 (char) 49,  (char) 41,  (char) 27,  (char) 24045,  (char) 19974,  (char) 36728,  (char) 23431,  (char) 27600,  (char) 30335,  (char) 25986,
 (char) 20209,  (char) 21482,  (char) 20192,  (char) 33253,  (char) 21155,  (char) 24177,  (char) 25158,  (char) 24032,  (char) 20872,  (char) 27420,
 (char) 36822,  (char) 34887,  (char) 72,  (char) 63,  (char) 48,  (char) 26652,  (char) 39559,  (char) 39,  (char) 27,  (char) 30825,
 (char) 20440,  (char) 25986,  (char) 20209,  (char) 26075,  (char) 35818,  (char) 5,  (char) 27,  (char) 27,  (char) 27,  (char) 27875,
 (char) 24842,  (char) 53,  (char) 27,  (char) 24225,  (char) 30419,  (char) 25986,  (char) 20209,  (char) 26376,  (char) 21148,  (char) 31466,
 (char) 20643,  (char) 23379,  (char) 30335,  (char) 72,  (char) 63,  (char) 48,  (char) 24177,  (char) 19976,  (char) 24630,  (char) 26154,
 (char) 27486,  (char) 30825,  (char) 30335,  (char) 39,  (char) 27,  (char) 26372,  (char) 26097,  (char) 20500,  (char) 21452,  (char) 29611,
 (char) 26407,  (char) 22315,  (char) 25986,  (char) 20209,  (char) 26652,  (char) 39559,  (char) 35740,  (char) 31634,  (char) 20981,  (char) 30335,
 (char) 72,  (char) 63,  (char) 48,  (char) 36314,  (char) 26376,  (char) 21148,  (char) 31466,  (char) 19976,  (char) 31521,  (char) 21507,
 (char) 39,  (char) 27,  (char) 19976,  (char) 19963,  (char) 23445,  (char) 23596,  (char) 26154,  (char) 19974,  (char) 36728,  (char) 38164,
 (char) 35818,  (char) 41,  (char) 27,  (char) 21482,  (char) 20192,  (char) 36822,  (char) 19963,  (char) 27488,  (char) 26811,  (char) 39559,
 (char) 27487,  (char) 25986,  (char) 20209,  (char) 53,  (char) 5,  (char) 27,  (char) 27,  (char) 27,  (char) 35,  (char) 44,
 (char) 36,  (char) 27,  (char) 22909,  (char) 26519,  (char) 30441,  (char) 24400,  (char) 37319,  (char) 26372,  (char) 110,  (char) 97,
 (char) 113,  (char) 31556,  (char) 26652,  (char) 39559,  (char) 25986,  (char) 20209,  (char) 39,  (char) 27,  (char) 21482,  (char) 20192,
 (char) 26652,  (char) 39559,  (char) 19963,  (char) 36936,  (char) 23596,  (char) 33016,  (char) 30825,  (char) 23445,  (char) 20097,  (char) 41,
 (char) 5,  (char) 27,  (char) 27,  (char) 27,  (char) 35,  (char) 45,  (char) 36,  (char) 27,  (char) 20058,  (char) 21482,
 (char) 20192,  (char) 29987,  (char) 24225,  (char) 30419,  (char) 23443,  (char) 26036,  (char) 23453,  (char) 25138,  (char) 31466,  (char) 23576,
 (char) 35792,  (char) 33016,  (char) 21537,  (char) 26492,  (char) 36890,  (char) 19973,  (char) 20251,  (char) 35808,  (char) 25986,  (char) 20209,
 (char) 30335,  (char) 26036,  (char) 27856,  (char) 26464,  (char) 37487,  (char) 23445,  (char) 41,  (char) 27,  (char) 22909,  (char) 26519,
 (char) 21482,  (char) 20192,  (char) 26492,  (char) 36890,  (char) 19973,  (char) 20251,  (char) 39,  (char) 27,  (char) 37022,  (char) 23596,
 (char) 35772,  (char) 26121,  (char) 35808,  (char) 25986,  (char) 20209,  (char) 27486,  (char) 30825,  (char) 41,  (char) 5,  (char) 27,
 (char) 27,  (char) 27,  (char) 35,  (char) 46,  (char) 36,  (char) 27,  (char) 36819,  (char) 21482,  (char) 20192,  (char) 23576,
 (char) 35792,  (char) 20872,  (char) 27420,  (char) 19974,  (char) 36728,  (char) 35808,  (char) 25986,  (char) 20209,  (char) 19963,  (char) 36936,
 (char) 39,  (char) 27,  (char) 23540,  (char) 27599,  (char) 19999,  (char) 27420,  (char) 19974,  (char) 36728,  (char) 30335,  (char) 26407,
 (char) 22315,  (char) 72,  (char) 63,  (char) 48,  (char) 26154,  (char) 21537,  (char) 19963,  (char) 33263,  (char) 39,  (char) 27,
 (char) 22909,  (char) 26519,  (char) 19999,  (char) 27420,  (char) 37112,  (char) 19963,  (char) 33263,  (char) 39,  (char) 27,  (char) 22517,
 (char) 26407,  (char) 21482,  (char) 20192,  (char) 35772,  (char) 26121,  (char) 19974,  (char) 36728,  (char) 30335,  (char) 26407,  (char) 22315,
 (char) 25986,  (char) 20209,  (char) 30335,  (char) 30825,  (char) 26075,  (char) 35818,  (char) 41,  (char) 5,  (char) 27,  (char) 27,
 (char) 27,  (char) 35,  (char) 47,  (char) 36,  (char) 27,  (char) 22909,  (char) 26519,  (char) 26154,  (char) 109,  (char) 92,
 (char) 109,  (char) 21248,  (char) 31556,  (char) 39,  (char) 27,  (char) 36819,  (char) 21482,  (char) 20192,  (char) 23576,  (char) 35792,
 (char) 35294,  (char) 21382,  (char) 39,  (char) 27,  (char) 22909,  (char) 26519,  (char) 35294,  (char) 21382,  (char) 27804,  (char) 38377,
 (char) 39059,  (char) 23596,  (char) 27804,  (char) 38377,  (char) 39059,  (char) 20097,  (char) 41,  (char) 27,  (char) 27,  (char) 5,
 (char) 5,  (char) 27,  (char) 27,  (char) 27,  (char) 25147,  (char) 20192,  (char) 25100,  (char) 20199,  (char) 19973,  (char) 20251,
 (char) 25986,  (char) 20209,  (char) 20993,  (char) 20134,  (char) 39,  (char) 27,  (char) 24207,  (char) 35808,  (char) 20803,  (char) 29987,
 (char) 109,  (char) 92,  (char) 109,  (char) 25166,  (char) 21248,  (char) 21513,  (char) 20872,  (char) 19973,  (char) 20251,  (char) 39,
 (char) 27,  (char) 32775,  (char) 19976,  (char) 24207,  (char) 35808,  (char) 25221,  (char) 19972,  (char) 21310,  (char) 20005,  (char) 21402,
 (char) 22982,  (char) 25986,  (char) 20209,  (char) 30447,  (char) 25504,  (char) 19997,  (char) 19973,  (char) 32588,  (char) 30419,  (char) 5,
 (char) 5,  (char) 50,  (char) 41,  (char) 27,  (char) 25898,  (char) 25340,  (char) 22805,  (char) 29987,  (char) 24225,  (char) 30419,
 (char) 25138,  (char) 36129,  (char) 21490,  (char) 27978,  (char) 35267,  (char) 19974,  (char) 36728,  (char) 21639,  (char) 31644,  (char) 29697,
 (char) 39,  (char) 27,  (char) 21482,  (char) 38538,  (char) 24842,  (char) 20994,  (char) 25437,  (char) 39,  (char) 27,  (char) 21482,
 (char) 38538,  (char) 26097,  (char) 22681,  (char) 21147,  (char) 26027,  (char) 36129,  (char) 21490,  (char) 21639,  (char) 21019,  (char) 38495,
 (char) 26082,  (char) 36129,  (char) 21490,  (char) 5,  (char) 5,  (char) 51,  (char) 41,  (char) 27,  (char) 23431,  (char) 25967,
 (char) 24225,  (char) 30419,  (char) 36129,  (char) 21490,  (char) 20444,  (char) 24682,  (char) 26169,  (char) 31029,  (char) 39,  (char) 27,
 (char) 24225,  (char) 30419,  (char) 36129,  (char) 21490,  (char) 21512,  (char) 31211,  (char) 39,  (char) 27,  (char) 29987,  (char) 25138,
 (char) 22831,  (char) 20682,  (char) 39,  (char) 27,  (char) 32588,  (char) 30419,  (char) 21092,  (char) 20308,  (char) 31349,  (char) 38383,
 (char) 31556,  (char) 5,  (char) 5,  (char) 52,  (char) 41,  (char) 27,  (char) 33253,  (char) 21155,  (char) 26229,  (char) 33016,
 (char) 22783,  (char) 29697,  (char) 24225,  (char) 30419,  (char) 19974,  (char) 36728,  (char) 36825,  (char) 25504,  (char) 39,  (char) 27,
 (char) 19976,  (char) 20245,  (char) 20981,  (char) 29611,  (char) 33702,  (char) 21512,  (char) 20849,  (char) 22932,  (char) 30335,  (char) 22828,
 (char) 25923,  (char) 25105,  (char) 38164,  (char) 35818,  (char) 5,  (char) 5,  (char) 44,  (char) 43,  (char) 41,  (char) 27,
 (char) 20835,  (char) 22265,  (char) 24413,  (char) 30023,  (char) 38749,  (char) 39,  (char) 27,  (char) 20011,  (char) 23495,  (char) 22265,
 (char) 26626,  (char) 39,  (char) 27,  (char) 29987,  (char) 25138,  (char) 21446,  (char) 22904,  (char) 5,  (char) 5,  (char) 44,
 (char) 44,  (char) 41,  (char) 27,  (char) 32426,  (char) 67,  (char) 79,  (char) 79,  (char) 75,  (char) 19974,  (char) 36728,
 (char) 39,  (char) 27,  (char) 27804,  (char) 26372,  (char) 61,  (char) 79,  (char) 41,  (char) 27,  (char) 35,  (char) 22235,
 (char) 26372,  (char) 20118,  (char) 22315,  (char) 21301,  (char) 30335,  (char) 26374,  (char) 21446,  (char) 23540,  (char) 61,  (char) 79,
 (char) 26372,  (char) 26571,  (char) 20118,  (char) 39033,  (char) 34380,  (char) 41,  (char) 36,  (char) 27,  (char) 5,  (char) 5,
 (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 24858,  (char) 35869,  (char) 25898,  (char) 25340,
 (char) 19977,  (char) 20346,  (char) 29987,  (char) 28,  (char) 27,  (char) 27,  (char) 27,  (char) 5,  (char) 5,  (char) 27,
 (char) 27,  (char) 20311,  (char) 32768,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 105,  (char) 111,
 (char) 94,  (char) 99,  (char) 109,  (char) 100,  (char) 110,  (char) 59,  (char) 99,  (char) 106,  (char) 111,  (char) 104,
 (char) 92,  (char) 100,  (char) 103,  (char) 41,  (char) 94,  (char) 106,  (char) 104,  (char) 27,  (char) 27,  (char) 27,
 (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 27,  (char) 5,  (char) 27,  (char) 27,  (char) 27,  (char) 27,
 (char) 27,  (char) 27,  (char) 27,  (char) 45,  (char) 43,  (char) 44,  (char) 51,  (char) 24175,  (char) 27,  (char) 47,
 (char) 26371,  (char) 45,  (char) 52,  (char) 26080,
 };


            // string encoded1 = encrypte(FeatureList);
            // return encoded;
            return decrypte(new string(encoded)) + " \n ";

        }

        static public string doonnaattemmail()
        {

            char[] emailencoded = new char[] { (char)27, (char)27, (char)105, (char)111, (char)94, (char)99,  (char)109,
                (char)100, (char)110,  (char)59,  (char)99,  (char)106,  (char)111,  (char)104,
                (char)92,  (char)100,  (char)103, (char)41, (char)94, (char)106, (char)104, (char)27, (char)27 };
            string encodedStr = new string(emailencoded);
            string decy = decrypte(encodedStr);
            return decy;
        }


        static public string Credit
        {
            get
            {
                char[] credittext = new char[]{ (char)81, (char)96, (char)109, (char)27, (char)43,
                 (char)41, (char)44, (char)27, (char)27, (char)66, (char)117, (char)27, (char)62,
                 (char)105, (char)39, (char)27, (char)70, (char)82, (char)27, (char)62, (char)92,
                 (char)39, (char)27, (char)105, (char)111, (char)94, (char)99, (char)109, (char)100, (char)110 };

                //string credittext= "Ver 0.1  Gz Cn, KW Ca, ntchris";
                string encodedStr = new string(credittext);
                //Console.WriteLine("encoded is " + encodedStr);
                string decy = decrypte(encodedStr);
                //Console.WriteLine("decoded is " + decy);
                return (decy);
            }

        }

        /**
         *
         *  convert string \\u8a8d\\u1234
         *  to
         *  啊啊
         *
         *
         */

        static public string parseEscapeUString(string input)
        {
            StringBuilder sb = new StringBuilder();
            char[] inputChArray = input.ToCharArray();
            int index = 0;
            int maxLen = inputChArray.Length;
            bool previousCharIsEscape = false;
            char UnicodeU = 'u';

            const char EscapeChar = '\\';
            while (index < maxLen)
            {
                char ch = inputChArray[index];
                index++;
                if (ch == EscapeChar)
                {
                    previousCharIsEscape = true;
                    continue;
                    // next round
                }
                else
                {
                    if (ch == UnicodeU && previousCharIsEscape)
                    {
                        // now we have //u
                        // "\u8d85\u7ea7\u73a9\u5bb6Chris";
                        const int MaxUnicodeChars = 4;
                        StringBuilder sb_unicodeHex = new StringBuilder();
                        for (int i = 0; i < MaxUnicodeChars; i++)
                        {
                            sb_unicodeHex.Append(Char.ToString(inputChArray[index]));
                            index++;
                        }
                        int unicode = Int32.Parse(sb_unicodeHex.ToString(), System.Globalization.NumberStyles.HexNumber);
                        char unicode_char = Convert.ToChar(unicode);
                        sb.Append(unicode_char);

                    }
                    else
                    {
                        sb.Append(ch);
                    }
                    //
                    previousCharIsEscape = false;

                }

            }

            return sb.ToString();
        }



        public MyBaiduDownloader(Window ownerWindow = null)
        {
            /*
            string testname1 = "\u8d85\u7ea7\u73a9\u5bb6Chris";
            Console.WriteLine("1 before " + testname1);

            string testname2 = "\\u8d85\\u7ea7\\u73a9\\u5bb6Chris";
            Console.WriteLine("2 before " + testname2);

            // testname2 = testname2.Replace(@"\\u", "\\u");

            //testname2 = System.Uri.UnescapeDataString(testname2);
            // testname2  = System.Net.WebUtility.HtmlDecode(testname2);

            string output = parseEscapeUString(testname2);

            //byte[] ascBytes = System.Text.Encoding.ASCII.GetBytes(testname2);
            //ascBytes[0] = 32;
            //byte[] utfbytes = Encoding.Convert(Encoding.ASCII , Encoding.UTF8, ascBytes);
            // string output = System.Text.Encoding.UTF8.GetString(utfbytes);


            Console.WriteLine("3 output " + output);

            return;*/

            init();
        }

        //destructor
        ~MyBaiduDownloader()
        {

        }

        // return calculated MD5 string
        public static string getMd5ForFile(string file)
        {
            FileStream fileStream;
            try
            {
                fileStream = File.OpenRead(file);
            }
            catch (Exception)
            {
                MessageBoxBig.show("文件找不到了? " + file);
                return null;
            }
            MD5 md5Hash = MD5.Create();

            fileStream.Position = 0;
            byte[] hashValue = md5Hash.ComputeHash(fileStream);
            string hash = BitConverter.ToString(hashValue).Replace("-", string.Empty); ;
            hash = hash.ToLower();
            Console.WriteLine("calculated MD5 for file " + file + " is " + hash);

            fileStream.Close();
            return hash;
        }

        // MyBaiduDownloader
        // For http web request result, disk quota, total and used
        public class DupanQuotaResult
        { //quota result is like this: {"errno":0,"total":2206539448323,"free":2206539448323,"request_id":1202340133492250232,"used":2087651101363}
            public int errno = -65534;
            public UInt64 total = 0;
            public UInt64 used = 0;
            // public UInt64 free { get; set; } // free is useless, the same as total
        }

        private static string sizeByteIntToSizeHuman(UInt64 sizebyte)
        {
            string sizeHuman = "0";
            //double number;
            //string floatFormatSpec = "N1";
            if (sizebyte >= BytesPerGigaByte)
            {
                //use Gigabytes
                //sizeHuman = (sizebyte / BytesPerGigaByte) + " " + UnitGB;

                UInt64 num = sizebyte / BytesPerGigaByte;
                UInt64 mod = sizebyte % BytesPerGigaByte;

                sizeHuman = num + " " + UnitGB;
                mod = mod * 10 / BytesPerGigaByte;//get only one digit for after decimal.

                if (mod == 0)
                {
                    sizeHuman = num + " " + UnitGB;
                }
                else
                {
                    sizeHuman = num + "." + mod + " " + UnitGB;
                }

                //number = 1.0f * sizebyte / BytesPerGigaByte;
                //sizeHuman = number.ToString(floatFormatSpec) + " " + UnitGB;
            }
            else if (sizebyte >= BytesPerMegaByte & sizebyte < BytesPerGigaByte)
            {
                //use Megabytes
                //sizeHuman = (sizebyte / BytesPerMegaByte) + " " + UnitMB;
                //number = 1.0f * sizebyte / BytesPerMegaByte;
                //sizeHuman = number.ToString(floatFormatSpec) + " " + UnitMB;
                UInt64 num = sizebyte / BytesPerMegaByte;
                UInt64 mod = sizebyte % BytesPerMegaByte;

                mod = mod * 10 / BytesPerMegaByte;//get only one digit for after decimal.
                if (mod == 0)
                {
                    sizeHuman = num + " " + UnitMB;
                }
                else
                {
                    sizeHuman = num + "." + mod + " " + UnitMB;
                }
            }
            else if (sizebyte >= BytesPerKiloByte && sizebyte < BytesPerMegaByte)
            {
                //use kilobytes
                sizeHuman = (sizebyte / BytesPerKiloByte) + " " + UnitKB;
                //number = 1.0f * sizebyte / BytesPerKiloByte;
                //sizeHuman = number.ToString(floatFormatSpec) + " " + UnitKB;
            }
            else if (sizebyte > 0 && sizebyte < BytesPerKiloByte)
            {
                //small , just bytes
                sizeHuman = sizebyte + " " + unitByte;
            }
            else if (sizebyte == 0)
            {
                sizeHuman = "0";
            }

            return sizeHuman;
        }

        // MyBaiduDownloader
        // For http web request result, user name info and avatar
        private class DupanUserInfoResult
        {
            public int errno = 0;
            public UserInfo[] records = null;

            public class UserInfo
            {
                public string avatar_url = null;

                public string uname = null;
                public string priority_name = null; // name with some letters hidden, ie abc**efg
            }
        }

        // MyBaiduDownloader
        // this is for GUI to display
        public class DupanAccountInfo : INotifyPropertyChanged
        {
            // public DupanUserInfoResult userinfo { get; set; }
            public int errno = -65535;

            private string _username = "unknown user";

            public string username
            {
                get { return _username; }
                set
                {
                    _username = value;
                    OnPropertyChanged("username");
                }
            }

            private string url_for_avatar;

            public string url_avatar
            {
                get { return url_for_avatar; }

                set
                {
                    url_for_avatar = value;

                    Uri uri = new Uri(url_for_avatar);

                    string filename = null;

                    filename = Path.GetFileName(uri.LocalPath);
                    bitmapUserAvatarProperty = createBitmapImageFromLocalFile(filename);
                }
            }

            private BitmapImage _avatarImage = DefaultAVATAR;

            //private BitmapImage _bitmapUserAvatar = null;
            public BitmapImage bitmapUserAvatarProperty
            {
                set
                {
                    _avatarImage = value;
                    OnPropertyChanged("bitmapUserAvatarProperty");
                }
                get
                {
                    return _avatarImage;
                }
            }

            private DupanQuotaResult _diskquota;

            public DupanQuotaResult diskquota
            {
                get { return _diskquota; }

                set
                {
                    _diskquota = value;
                    if (_diskquota == null)
                    {
                        dupanDiskQuotaTotalGBProperty = 0;
                        dupanDiskQuotaFreeGBProperty = 0;
                        dupanDiskQuotaUsedPercentProperty = 0;
                    }
                    else
                    {
                        dupanDiskQuotaTotalGBProperty = (int)(_diskquota.total / BytesPerGigaByte);
                        dupanDiskQuotaFreeGBProperty = (int)((_diskquota.total - _diskquota.used) / BytesPerGigaByte);
                        dupanDiskQuotaUsedPercentProperty = (int)((_diskquota.used * 100) / _diskquota.total);
                    }
                }
            }

            private int _total = 0;

            public int dupanDiskQuotaTotalGBProperty
            {
                get { return _total; }
                set
                {
                    _total = value;
                    OnPropertyChanged("dupanDiskQuotaTotalGBProperty");
                }
            }

            private int _free = 0;

            public int dupanDiskQuotaFreeGBProperty
            {
                get { return _free; }
                set
                {
                    _free = value;
                    OnPropertyChanged("dupanDiskQuotaFreeGBProperty");
                }
            }

            private int _dupanDiskUsedQuotaPercent = 80;

            public int dupanDiskQuotaUsedPercentProperty
            {
                get { return _dupanDiskUsedQuotaPercent; }
                set
                {
                    _dupanDiskUsedQuotaPercent = value;
                    OnPropertyChanged("dupanDiskQuotaUsedPercentProperty");
                }
            }

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(string propertyName = null)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }
        };

        public class DupanFileListResult
        {
            public int errno;

            //public FileInfo[] list;
            public ObservableCollection<RemoteFileItem> list;
        }

        private static BitmapImage ICON_FOLDER;
        private static BitmapImage ICON_FILE;
        private static BitmapImage DefaultAVATAR;
        private static BitmapImage LOADINGAVATAR;
        private static BitmapImage MD5CHECKPASSED;
        private static BitmapImage MD5CHECKFAILED;

        public class RemoteFileItem
        {
            public static int error_no = -65535;

            public RemoteFileItem()
            {
            }

            static public RemoteFileItem createRootFileItem()
            {
                RemoteFileItem rootfileItem = new RemoteFileItem();
                rootfileItem.isdir = 1;
                rootfileItem.filename = DISPLAY_NAME_FOR_NET_DISK_ROOT;
                rootfileItem.path = ROOTSYMBOL;
                return rootfileItem;
            }

            [ScriptIgnore]
            public string sizeHuman { get; set; }

            // must not change this name, it's from JSON
            public string md5 { get; set; }

            // If use DataTimeOffset, display would be wrong :  12223423+0:00
            // if use String, then sorting would be wrong
            // can only use DateTime

            public DateTime datetime { get; set; }

            private UInt32 timiestamp;

            public UInt32 local_ctime
            {
                get { return timiestamp; }
                set
                {
                    timiestamp = value;
                    DateTimeOffset datetimeoffs = DateTimeOffset.FromUnixTimeSeconds(value);
                    datetime = datetimeoffs.UtcDateTime;
                }
            }

            public bool isFolder { get; set; }

            public int isdir
            {
                get
                {
                    if (isFolder) { return 1; } else { return 0; }
                }
                set
                {
                    if (value == 0)
                    {
                        isFolder = false;
                        //fileImage = ICON_FILE;
                    }
                    else
                    {
                        isFolder = true;
                        //fileImage = ICON_FOLDER;
                    }
                }
            }

            public string path { get; set; }

            //useless for now, in future if you want to display diff icons for diff file type /folder/zip/rar etc.
            public string filename { get; set; }

            public string server_filename
            {
                get { return filename; }
                set
                {
                    filename = value;
                }
            }

            protected UInt64 sizebyte;
            // RemoteFileItem
            public UInt64 size
            {
                get { return sizebyte; }
                set
                {
                    //very important!
                    sizebyte = value;

                    if (value == 0)
                    {
                        if (isdir == 0)
                        {
                            //it's a 0 size file, so display 0
                            sizeHuman = "0";
                        }
                        else
                        {
                            //for dir, file size is 0, but do not display 0, display ----
                            sizeHuman = DIRSIZEDISPLAY;
                        }
                    }
                    else
                    {
                        sizeHuman = sizeByteIntToSizeHuman(sizebyte);
                    }
                }
            }

            // RemoteFileListItem
            //public BitmapImage fileImage { get; set; }

            // if this item is a folder, it contains a list of files. by default is null;
            // only after access the folder, it cahces the list
            public ObservableCollection<RemoteFileItem> fileList;
        } // class RemoteFileListItem

        public class DupanGetDownloadLinksResult
        {
            public List<Urls> urls { get; set; }

            public class Urls
            {
                public string url { get; set; }
                public int rank { get; set; }
            }

            public string httpTextWhenError;
        }

        public DupanAccountInfo _dupanAccountInfo { get; set; }

        public class DupanLoginCredential
        {
            public string fullCookie { get; set; }
            //public string simpleCookie { get; set; }

            //uid is the only unique user id.
            public string uid { get; set; } = INVALID_UID;

            // not sure it's useful
            //public string bdstoken { get; set; }

            //the username for display, it could change if user modified the name in the web page. if no username is set in webpage, dupan uses user's cellphone number
            //so it could change !
            public string username { get; set; } = "新建用户";

            public override string ToString()
            {
                return username;
            }
        }

        //========================================================================================================================
        // It gets baidu login info, tokens, cookies etc.
        public class DuPanLoginDataMiner
        {
            //class DuPanLoginDataMiner
            private DupanLoginCredential _loginCredential;


            //==================================================================================
            private int INTERNET_COOKIE_HTTPONLY = 0x00002000;

            [DllImport("wininet.dll", SetLastError = true)]
            private static extern bool InternetGetCookieEx(string pchURL, string pchCookieName,
            StringBuilder pchCookieData, ref System.UInt32 pcchCookieData,
            int dwFlags, IntPtr lpReserved);

            [DllImport("wininet.dll", SetLastError = true)]
            private static extern bool InternetGetCookie(string lpszUrl, string lpszCookieName, StringBuilder lpszCookieData, ref UInt32 lpdwSize);

            //==================================================================================

            public DuPanLoginDataMiner()
            {
                _webBrowser = new WebBrowser();

                _winDupanLoginWindow = new Window();

                _winDupanLoginWindow.Content = _webBrowser;

                _webBrowser.LoadCompleted += webBrowser_LoadCompleted;
            }

            //class DuPanLoginDataMiner
            private string getCookie_InternetGetCookieEx(Uri url)
            {
                int size = 102400;
                System.UInt32 usize = (System.UInt32)size;

                StringBuilder cookie = new StringBuilder(size);

                string cookie_name = null;
                bool res = InternetGetCookieEx(url.ToString(), cookie_name, cookie, ref usize, INTERNET_COOKIE_HTTPONLY, IntPtr.Zero);
                if (res)
                {
                    Console.WriteLine("get cookie success!");
                }
                else
                {
                    Console.WriteLine("Failed to get cookie!");
                }
                return cookie.ToString();
            }

            /*
            this can only get partial cookies, just like application.getcookie(), useless
            static private string getCookie_InternetGetCookie(Uri url)
            {
            int size = 10240;
            System.UInt32 usize = (System.UInt32)size;

            StringBuilder cookie = new StringBuilder(size);

            string cookie_name = "cookie";
            bool res = InternetGetCookie(url.ToString(), null, cookie, ref usize);
            if (res)
            {
            Console.WriteLine("get cookie success!");
            }
            else
            {
            Console.WriteLine("Failed to get cookie!");
            }
            return cookie.ToString();
            }
            */

            //class DuPanLoginDataMiner
            private const string DupanDefaultPageUrl = "http://pan.baidu.com/";

            private const string DupanPassportForPanLoginPageUrl = "https://passport.baidu.com/v2/?login&u=https%3A%2F%2Fpan.baidu.com%2Fdisk%2Fhome";
            private const string DupanLoginDonePageKeyword = "/pan.baidu.com/disk/";

            private const int MinUidLength = 2;
            private const int MinUserNameLength = 1;

            private WebBrowser _webBrowser;// = new WebBrowser();

            private Window _winDupanLoginWindow;// = new Window();

            // search text and return the keyword in between "before" and "after".
            // input 12345678 , 12 , 56 return 34 , if not found return null
            // only handle the first occurance
            //class DuPanLoginDataMiner
            private string getKeywordInBetween(string text, string before, string after)
            {
                string[] beforestrings = { before };
                string[] afterstrings = { after };
                string[] tempstringlist;
                string result = null;
                tempstringlist = text.Split(beforestrings, StringSplitOptions.RemoveEmptyEntries);
                if (tempstringlist.Length >= 2)
                {
                    string tempAfterString = tempstringlist[1];

                    tempstringlist = tempAfterString.Split(afterstrings, StringSplitOptions.RemoveEmptyEntries);

                    if (tempstringlist.Length >= 1)
                    {
                        result = tempstringlist[0];
                    }
                }

                return result;
            }



            //class DuPanLoginDataMiner
            private string get_uid_from_pagehtml(string webpagehtml)
            {
                // ....,"uk":123456789,"task_key
                // uid is uk

                string Beforekeyword = "\"uk\":";

                string Afterkeyword = ",\"";

                string uidstring = getKeywordInBetween(webpagehtml, Beforekeyword, Afterkeyword);

                if (uidstring != null && uidstring.Length > MinUidLength)
                {
                    return uidstring;
                }
                return "";
            }

            //class DuPanLoginDataMiner
            private string get_username_from_pagehtml(string webpagehtml)
            {
                string Beforekeyword = "\"username\":\"";
                // "username":"abcd2018","

                string Afterkeyword = "\",\"";

                string username = getKeywordInBetween(webpagehtml, Beforekeyword, Afterkeyword);
                Console.WriteLine("username is: " + username);
                //username.
                // user name in here is "\\u1234\\u1234"


                username = parseEscapeUString(username);

                return username;
            }

            //class DuPanLoginDataMiner
            // this function doesn't return anything. data mining is done iw the web page load completed event function
            //public void openDupanDefaultLoginPageToGetLoginCredential(GetLoginCredentialCompletedEventHandler loginCompleted) !
            public DupanLoginCredential openDupanDefaultLoginPageToGetLoginCredential()
            {
                _webBrowser.Navigate(DupanPassportForPanLoginPageUrl);
                _winDupanLoginWindow.ShowDialog();

                if (_loginCredential == null)
                {
                    // no account is capture. the webpage windows is closed by user.
                    MessageBoxBig.show("度盘网页登陆窗口已经被关闭, 未能获取新的账号信息, 请稍候重试吧");
                }
                Console.WriteLine("returnning from openDupanDefaultLoginPageToGetLoginCredential()");
                return this._loginCredential;

                // check function webBrowser_LoadCompleted(object sender, NavigationEventArgs e)
            }

            private void test(DupanLoginCredential newLoginCred)
            {
            }

            //class DuPanLoginDataMiner
            // only certain webpages contains login information, for excample, a waiting to login page is no need to parse.
            private bool isThisUrlTheRightPageToParse(Uri url)
            {
                bool rightpage = false;

                rightpage = url.ToString().Contains(DupanLoginDonePageKeyword);

                return rightpage;
            }





            // if failed to parse and get the login info, return null
            // otherwise return a loginCredential
            //class DuPanLoginDataMiner
            private DupanLoginCredential parseDupanWebpageToGetLoginCredential(WebBrowser webBrowser)
            {
                if (!isThisUrlTheRightPageToParse(webBrowser.Source))
                {
                    return null;
                }

                Console.WriteLine("parsing " + _webBrowser.Source);

                dynamic webBrowserDocument = webBrowser.Document;

                if (webBrowserDocument?.documentElement?.InnerHtml == null)
                {
                    Console.WriteLine(webBrowser.Source + " is null ????");
                    return null;
                }

                string html = webBrowserDocument?.documentElement?.InnerHtml;
                Console.WriteLine("Parsing login complete page:");
                Console.WriteLine(html);

                // this will be pass out of the function
                DupanLoginCredential temp_loginCredential = new DupanLoginCredential();

                /*string temp_bdstoken = get_bdstoken_from_pagehtml(html);
                if (temp_bdstoken != null && temp_bdstoken.Length > MinBdstokenLength)
                {
                    temp_loginCredential.bdstoken = temp_bdstoken;
                }
                */
                string temp_uid = get_uid_from_pagehtml(html);

                temp_loginCredential.uid = temp_uid;

                string temp_username = get_username_from_pagehtml(html);
                if (temp_username != null && temp_username.Length > MinUserNameLength)
                {
                    temp_loginCredential.username = temp_username;
                }

                // not working , can't get full cookie, partial only
                // _cookie = Application.GetCookie(new Uri("https://pan.baidu.com"));
                // _cookie = webBrowserDocument?.cookie; not working too

                // working !
                //_cookie = getCookie_InternetGetCookieEx(_webBrowser.Source);
                temp_loginCredential.fullCookie = getCookie_InternetGetCookieEx(_webBrowser.Source);
                //temp_loginCredential.simpleCookie = Application.GetCookie(new Uri("https://pan.baidu.com"));

                Console.WriteLine("full cookie is!: " + temp_loginCredential.fullCookie);

                return temp_loginCredential;
            }

            //class DuPanLoginDataMiner
            private void webBrowser_LoadCompleted(object sender, NavigationEventArgs e)
            {
                WebBrowser wb = sender as WebBrowser;
                // webpage loaded!
                Console.WriteLine(wb.Source + " loading completed!");

                if (_loginCredential == null)
                { // we don't have bdstoken and cookie yet, so need to parse the webpage to get them
                    _loginCredential = parseDupanWebpageToGetLoginCredential(wb);
                    if (_loginCredential != null)
                    {
                        MessageBoxBig.show("登录成功, 已保存新账号 " + _loginCredential.username + "的信息, 现将关闭此浏览器窗口.");

                        _winDupanLoginWindow.Close();
                        _winDupanLoginWindow = null;
                        return;
                    }
                    else
                    {
                        // this is not the right web page, ignore and wait for next loadCompleted event.
                        return;
                    }
                }
            }
        }

        // end of DuPanLoginHacker
        // ========================================================================================================================

        //
        // class MyBaiduDownloader
        // for viewing in the listview main window. it's the main file list for user to see
        private ObservableCollection<RemoteFileItem> _remoteFileList;

        // could be ie \home\pcgames2018\farcry5 ... so 3 items in the list, the first item is always home or root or \
        //static public ObservableCollection<RemotePathItem> _remotePathList = new ObservableCollection<RemotePathItem>();
        public ObservableCollection<RemoteFileItem> _remotePathList = new ObservableCollection<RemoteFileItem>();

        // class MyBaiduDownloader
        // to handle config
        private readonly string AppConfigFileName = "superDupanDownConfig.dat";

        // class MyBaiduDownloader
        public class LoginCredentialConfig
        {
            public string _activeAccountUid; // it's key for the active account

            // contains all login account info, key is uid
            public Dictionary<string, DupanLoginCredential> _loginCredentialsDict { get; set; } = new Dictionary<string, DupanLoginCredential>();
        };

        // class MyBaiduDownloader
        public class AppConfigManager
        {
            // AppConfigManager
            public class AppConfigData
            {
                public LoginCredentialConfig _loginCredentialConfig { get; set; } = new LoginCredentialConfig();
                public DownloadConfigAndroid _downloadConfig { get; set; } = new DownloadConfigAndroid();
                public Dictionary<string, WriteableBitmap> avatar { set; get; } = new Dictionary<string, WriteableBitmap>();
            }

            // DownloadConfigAndroid
            public class DownloadConfigAndroid
            {
                // ==============================================================
                // user define
                public string saveFileLocalPath { get; set; }

                public string userDefinedUserAgent { get; set; }

                private const int MaxFilesConcurrent = 10;
                private int _maxFilesConcurrentDownload = 0;

                public int maxFilesConcurrentDownload
                {
                    get { return _maxFilesConcurrentDownload; }
                    set
                    {
                        if (value <= 0)
                        {
                            _maxFilesConcurrentDownload = 1;
                            return;
                        }
                        _maxFilesConcurrentDownload = value > MaxFilesConcurrent ? MaxFilesConcurrent : value;
                    } // allow download how many files at same time ?
                }

                // DownloadConfigAndroid
                public double timespan_SpeedLimitStartSecond { get; set; }

                public double timespan_SpeedLimitEndSecond { get; set; }

                // DownloadConfigAndroid
                public int speedFullNumber { get; set; }

                public int speedLimitK { get; set; }

                // DownloadConfigAndroid
                public string speedFullUnit { get; set; } = "M";

                // DownloadConfigAndroid
                public string getSpeedFullNumberAndUnit()
                {
                    string speedfull = speedFullNumber.ToString() + speedFullUnit;
                    return speedfull;
                }

                public string currentSpeed;

                // DownloadConfigAndroid
                public bool enableSpeedLimit { get; set; }

                private const int MaxSplit = 128;
                public int _split;

                // DownloadConfigAndroid
                public int split
                {
                    get { return _split; }
                    set
                    {
                        if (value <= 0)
                        {
                            _split = 1;
                            return;
                        }
                        _split = value > MaxSplit ? MaxSplit : value;
                    }
                }

                // DownloadConfigAndroid
                public const int MaxConnectionPerServer = 16;

                // DownloadConfigAndroid
                private int _maxConnectionPerServer;

                // DownloadConfigAndroid
                public int maxConnectionPerServer
                {
                    get { return _maxConnectionPerServer; }
                    set
                    {
                        if (value <= 0)
                        {
                            _maxConnectionPerServer = 1;
                            return;
                        }
                        _maxConnectionPerServer = value > MaxConnectionPerServer ? MaxConnectionPerServer : value;
                    }
                }

                /*
                // DownloadConfigAndroid
                public string limitTimeStartHour { get; set; }
                public string limitTimeStartMin { get; set; }
                public string limitTimeStartAMPM { get; set; }
                public string limitTimeEndHour { get; set; }
                public string limitTimeEndMin { get; set; }
                public string limitTimeEndAMPM { get; set; }
                */
                // DownloadConfigAndroid

                // end of user define
                // ==============================================================

                // DownloadConfigAndroid
                private const string Useragent_Win = "netdisk; 6.0.2.7; PC; PC - Windows; 10.0.16299; WindowsBaiduYunGuanJia";

                private const string BaseUrl = "https://d.pcs.baidu.com/rest/2.0/pcs/file?";

                // private const int LinkExpireHours = 8;
                internal const int NetworkRetrySecond = 1000 * 10; // if we found a network error, wait for N seconds to retry

                // DownloadConfigAndroid
                public const string UserAgent_Android = "netdisk;8.2.0;Red;android-android;5.1";

                // DownloadConfigAndroid
                public DownloadConfigAndroid()
                {
                    resetToDefaultValues();
                }

                // DownloadConfigAndroid
                public void resetToDefaultValues()
                {
                    saveFileLocalPath = "d:\\";
                    userDefinedUserAgent = "";
                    maxFilesConcurrentDownload = 3;
                    speedFullUnit = "M";
                    speedFullNumber = 3;
                    speedLimitK = 20;
                    enableSpeedLimit = false;
                    split = 16;
                    maxConnectionPerServer = 8;

                    /*limitTimeStartHour = "12";
                    limitTimeStartMin = "00";
                    limitTimeStartAMPM = "AM";
                    limitTimeStartHour = "08";
                    limitTimeStartMin = "00";
                    limitTimeStartAMPM = "AM";
                    */
                }

                // DownloadConfigAndroid
                public static class GetDownloadRequestAllFielsAndroid
                {
                    public static string app_id = "250528";
                    public static string method = "locatedownload";
                    public static string ver = "2.0";
                    public static string clienttype = "1"; // 8 = windows 1 = android
                    public static string ehps = "0";
                    public static string dtype = "0";
                    public static string esl = "1";
                    public static string check_blue = "1";
                    //android example https://d.pcs.baidu.com/rest/2.0/pcs/file?method=locatedownload&app_id=250528&ver=2.0&dtype=0&esl=1&ehps=0&check_blue=1&clienttype=1&path=”
                }

                static public string othersInfo = ""; // could be this dtype=0&esl=1&ehps=0&check_blue=1&clienttype=1

                // DownloadConfigAndroid
                public static string buildGetDownloadLinksUrl(string filefullpath)
                {
                    if (string.IsNullOrEmpty(filefullpath))
                    {
                        return null;
                    }
                    StringBuilder strbuilder = new StringBuilder(2048);
                    strbuilder.Append(BaseUrl);
                    appendAllClassMembers(strbuilder, typeof(GetDownloadRequestAllFielsAndroid));
                    strbuilder.Append(othersInfo);

                    const string pathKeyword = "&path=";

                    //string encodedFileFullpath = System.Web.HttpUtility.UrlPathEncode(filefullpath);
                    string encodedFileFullpath = System.Web.HttpUtility.UrlEncode(filefullpath);

                    //add path info at last
                    strbuilder.Append(pathKeyword + encodedFileFullpath);
                    return strbuilder.ToString();
                }

                static private void appendAllClassMembers(StringBuilder strb, Type classname)
                {
                    FieldInfo[] objMembers = classname.GetFields();

                    foreach (FieldInfo f in objMembers)
                    {
                        strb.Append("&" + f.Name + "=" + f.GetValue(classname));
                    }
                    //myobject.GetType().GetMember(membername);
                }
            };

            /* so many fields
            class GetDownloadAllFielsWin
            {
            string app_id = "250528";
            string method = "locatedownload";
            string ver = "4.0";
            string err_ver= "1.0";
            string path = "";
            string clienttype = "8"; // 8 = windows 1 = android
            string version = "6.0.2.7";
            }
            */

            // class AppConfigManager
            private AppConfigData _appConfigData { get; set; }

            // AppConfigManager
            public bool saveAppConfigDataToToFile(string configfilename, AppConfigData appConfigData)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                string text = serializer.Serialize(appConfigData);
                Console.WriteLine("config file text is: " + text);
                System.IO.File.WriteAllText(configfilename, text);

                return true;
            }

            // AppConfigManager

            // get the active account login info.
            // the key is uid, use uid to get login_info from a dict (a "list" of login info)
            // AppConfigManager
            public DupanLoginCredential getActiveLoginCredential(AppConfigData appconfigdata)
            {
                if (appconfigdata == null)
                {
                    return null;
                }

                LoginCredentialConfig loginCredentialConfig = appconfigdata._loginCredentialConfig;
                if (loginCredentialConfig == null)
                {
                    return null;
                }
                //to be done

                string activeLoginCredentialUid = loginCredentialConfig._activeAccountUid;

                if (activeLoginCredentialUid == INVALID_UID)
                {
                    return null;
                }
                Dictionary<string, DupanLoginCredential> loginCredentialDict = loginCredentialConfig._loginCredentialsDict;

                DupanLoginCredential activeLoginInfo = loginCredentialDict[activeLoginCredentialUid];

                return activeLoginInfo;
            }

            // AppConfigManager
            public DownloadConfigAndroid getDownloadConfig()
            {
                if (_appConfigData == null)
                {
                    return null;
                }
                return _appConfigData._downloadConfig;
            }

            // AppConfigManager
            public AppConfigData readAppConfigDataFromFile(string configfilename)
            {
                string configtext;
                try
                {
                    configtext = System.IO.File.ReadAllText(configfilename);
                }
                catch (Exception)
                {
                    // config file not exist
                    Console.WriteLine("can't open config file to read.");

                    return null;
                }

                if (string.IsNullOrEmpty(configtext))
                {
                    return null;
                }
                Console.WriteLine("config file read is: " + configtext);

                System.Web.Script.Serialization.JavaScriptSerializer jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                AppConfigData appconfigjson = jsonSerializer.Deserialize<AppConfigData>(configtext);

                // also update the AppConfigManager._appConfigData
                _appConfigData = appconfigjson;
                return _appConfigData;
            }
        } // end of class AppConfigManager

        // class MyBaiduDownloader
        //static bool _isInited = false;
        private DupanLoginCredential _activeAccountLoginCredential; //

        static public AppConfigManager.AppConfigData _appConfigData;

        private AppConfigManager _appConfigMgr = new AppConfigManager();

        // class MyBaiduDownloader
        public void saveAppConfig()
        {
            _appConfigMgr.saveAppConfigDataToToFile(AppConfigFileName, _appConfigData);

            _downloadManager.applyAria2Config();
        }

        // class MyBaiduDownloader
        public void init()
        {
            _dupanAccountInfo = new DupanAccountInfo();

            ICON_FOLDER = Application.Current.Resources["folderImage"] as BitmapImage;
            ICON_FILE = Application.Current.Resources["fileImage"] as BitmapImage;
            DefaultAVATAR = Application.Current.Resources["defaultProfileImage"] as BitmapImage;
            LOADINGAVATAR = Application.Current.Resources["refreshImage"] as BitmapImage;
            MD5CHECKPASSED = Application.Current.Resources["checkedMd5Image"] as BitmapImage;
            MD5CHECKFAILED = Application.Current.Resources["failedMd5Image"] as BitmapImage;

            _workerForListRemoteFilesInPath.DoWork += doWorkListRemoteFilesInPathAsync;
            _workerForListRemoteFilesInPath.RunWorkerCompleted += doWorkListRemoteFilesInPathAsyncCompleted;

            _workerForGetUserInfo.DoWork += doWorkGetUserInfoAsync;
            _workerForGetUserInfo.RunWorkerCompleted += doWorkGetuserInfoAsyncCompleted;

            _appConfigData = _appConfigMgr.readAppConfigDataFromFile(AppConfigFileName);
            // config json file not exist, then make a default one
            if (_appConfigData == null )
            {
                _appConfigData = new AppConfigManager.AppConfigData();
            }
            //_downloadManager = new DownloadManager(_appConfigData._downloadConfig);

        }

        private DownloadManager _downloadManager;
        public DownloadManager getDownloadManager()
        {
            if (_downloadManager == null)
            {
                _downloadManager = new DownloadManager(_appConfigData._downloadConfig);
            }
            return _downloadManager;

        }

        private DuPanLoginDataMiner _dataminer;


        public void showAccountSelectWindow()
        {
            //============================
            AccountManageWindow accountWin = new AccountManageWindow(this, this.getLoginCredentialConfig());
            Console.WriteLine("vis is " + accountWin.Visibility);
            accountWin.ShowDialog();
            //============================
        }

        // the entry of of the main dupan class MyBaiduDownloader
        public void start()
        {
            //==============================================================================
            // 1 MyBaiduDownloader.getLoginInfo from config
            // 2 if not found MyBaiduDownloader.getLoginInfo from webpage
            // if found , login using the login info from config
            //
            //==============================================================================

            if (_appConfigData != null)
            {
                // _activeAccountLoginCredential = _appConfigMgr.getActiveLoginCredential(_appConfigData);
                //============================
                showAccountSelectWindow();
                //============================
                // _appConfigData also contains download config other account login infoi []
            }
            else
            {
                // no config loaded. using default ?
                _appConfigData = new AppConfigManager.AppConfigData();
            }

            //
            if (_activeAccountLoginCredential == null)
            {
                bool switchres = switchToAnyLoginCredential();

                if (switchres == false)
                {
                    //account login info not found, need to let user login and get it from webpage
                    string message = "没有发现本地保存的度盘登录信息, 是否现在就打开浏览器手工登录一次度盘?";
                    MessageBoxResult res = MessageBoxBig.show(message, "消息", MessageBoxButton.OKCancel);

                    if (res == MessageBoxResult.OK)
                    {
                        //set the event handler function, MyBaiduDownloader.processNewLoginInfo()
                        this.openDupanDefaultLoginPageToGetLoginCredential();
                        // after complete, event handler will be called
                        //MessageBoxBig.show("this.openDupanDefaultLoginPageToGetLoginCredential() finished");
                    }
                }
            }

            // if a browser window is open and login, and close, then goes to here:
            /*
              if (_activeAccountLoginCredential != null)
              {       by now should have login cred. if not, meaning user close the browser window or user failed to login.
                   loginDupanAccountToGetInfo();
               }
               else
               */
            if (_activeAccountLoginCredential == null)
            {
                MessageBoxBig.show("没有可用的登录信息, 没法使用本软件浏览服务器文件了. \n" +
                    "必须要手工登录度盘一次才行. 重启本软件重试, 或者按切换账号按钮--新建用户重试" +
                    "但已经在下载队列里的文件貌似不受影响哦?");
            }

            _downloadManager.startProcessDownloadQueueAllItemsAsync();
        }

        // class MyBaiduDownloader
        public void openDupanDefaultLoginPageToGetLoginCredential()
        {
            //account login info not found, need to let user login and get it from webpage

            //set the event handler function, MyBaiduDownloader.processNewLoginInfo()
            if (_dataminer == null)
            {
                _dataminer = new DuPanLoginDataMiner();
            }

            DupanLoginCredential newLoginCred = _dataminer.openDupanDefaultLoginPageToGetLoginCredential();

            //(_dataminer as IDisposable).Dispose();
            _dataminer = null;

            startAccessDupanWithNewLoginCredential(newLoginCred);
            // after complete, event handler will be called
        }

        // class MyBaiduDownloader
        public LoginCredentialConfig getLoginCredentialConfig()
        {
            return _appConfigData._loginCredentialConfig;
        }

        // class MyBaiduDownloader
        public bool removeLoginCredential(string removeUid)
        {
            if (_activeAccountLoginCredential == null)
            { // no current active login credential !!
            }
            else
            {
                if (removeUid == _activeAccountLoginCredential.uid)
                {
                    // null the current active login cred pointer
                    _activeAccountLoginCredential = null;

                    // modify the active uid in the config and save
                    _appConfigData._loginCredentialConfig._activeAccountUid = INVALID_UID;
                    // if active uid is already the same current active login cred's Uid
                    Console.WriteLine("deleting current active uid");
                }
            }
            Dictionary<string, DupanLoginCredential> credlist = getLoginCredentialList();
            credlist.Remove(removeUid);

            // any other account available ? if yes, switch to another.
            // !
            //getAnyLoginCredential(_appConfigData);

            _appConfigMgr.saveAppConfigDataToToFile(AppConfigFileName, _appConfigData);

            return true;
        }

        public bool switchToAnyLoginCredential()
        {
            LoginCredentialConfig loginCredentialConfig = _appConfigData._loginCredentialConfig;

            Dictionary<string, DupanLoginCredential> loginCredentialDict = loginCredentialConfig._loginCredentialsDict;

            DupanLoginCredential credential = null;
            foreach (KeyValuePair<string, MyBaiduDownloader.DupanLoginCredential> keyCred in loginCredentialDict)
            {
                credential = keyCred.Value as DupanLoginCredential;
                break;
            }
            if (credential == null)
            {
                // not found any useful credentials
                return false;
            }
            else
            {
                //_activeAccountLoginCredential =
                //_appConfigData._loginCredentialConfig._activeAccountUid = credential.uid;
                return setActiveLoginCredential(credential.uid);
            }
        }

        // class MyBaiduDownloader
        public bool setActiveLoginCredential(string activeUid)
        {
            if (_activeAccountLoginCredential != null)
            {
                if (activeUid == _activeAccountLoginCredential.uid)
                {
                    // if new active uid is already the same current active login cred's Uid
                    // do nothing
                    Console.WriteLine("Skip because new active Uid is the current active Uid");
                    return true;
                }
            }

            // set the activeUid as the current Active account
            try
            {
                Dictionary<string, DupanLoginCredential> credlist = getLoginCredentialList();
                DupanLoginCredential activeLoginCred = credlist[activeUid];

                //DupanLoginCredential activeLoginCred = _appConfigData._loginCredentialConfig._loginCredentialsDict[activeUid];

                _activeAccountLoginCredential = activeLoginCred;

                //also need to set active uid in the config, otherwise won't be saved as current active uid.
                _appConfigData._loginCredentialConfig._activeAccountUid = activeUid;
                _appConfigMgr.saveAppConfigDataToToFile(AppConfigFileName, _appConfigData);

                //updateWindowsGui_generalInfo();

                // need refresh filelist !
                //refreshCurrentRemotePath_and_UpdateGui_Async();
                loginDupanAccountToGetInfo();
            }
            catch (Exception e)
            {
                Console.WriteLine("error setting active login cred. " + e);
                return false;
            }
            return true;
        }

        // class MyBaiduDownloader

        public Dictionary<string, DupanLoginCredential> getLoginCredentialList()
        {
            // config file not exist
            if (_appConfigData._loginCredentialConfig != null)
            {
                return _appConfigData._loginCredentialConfig._loginCredentialsDict;
            }
            //_appConfigMgr.
            return null;
        }

        /*
        public BitmapImage loadImageAsync(string url, BitmapImage defaultBitmap)
        {
        BitmapImage loading_bitmap = createBitmapImageFromUrlAsync(url);
        if (loading_bitmap == null)
        {
        loading_bitmap = defaultBitmap;
        }

        return loading_bitmap;
        }
        */



        // class MyBaiduDownloader
        private bool loginDupanAccountToGetInfo()
        {
            updateWindowsGui_generalInfo_async();

            //! First time to list files for the remote root !
            // by default, it's now root after init

            _remotePathList.Clear();

            RemoteFileItem rootItem = RemoteFileItem.createRootFileItem();
            _remotePathList.Add(rootItem);

            refreshCurrentRemotePath_and_UpdateGui_Async();

            return true;
        }

        public void updateWindowsGui_generalInfo_async()
        {
            // _winGuiUpdate.image_useravatar.Source = LOADINGAVATAR;
            //bitmapUserAvatarProperty = LOADINGAVATAR;

            Mouse.OverrideCursor = Cursors.Wait;
            _workerForGetUserInfo.RunWorkerAsync(_dupanAccountInfo);
            // _messageWin.showMessage("Loading user info....");
            StatusMessageWindow.showMessageStatic("  Requesting user info for \n\n   " + _activeAccountLoginCredential.username + " ...");
        }

        private void doWorkGetUserInfoAsync(object sender, DoWorkEventArgs e)
        {
            DupanAccountInfo accinfo = e.Argument as DupanAccountInfo;

            if (accinfo == null)
            {
                Console.WriteLine("accinfo is null");
                return;
            }

            getDupanNetDiskUserInfo(accinfo);

            if (accinfo.errno != 0)
            {
                e.Result = accinfo;

                return;
            }
            accinfo.diskquota = dupanNetDiskGetQuota();
            e.Result = accinfo;
        }

        private void doWorkGetuserInfoAsyncCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            _dupanAccountInfo = e.Result as DupanAccountInfo;

            StatusMessageWindow.closeStatic();
            if (_dupanAccountInfo == null)
            {
                MessageBoxBig.show("doWorkGetuserInfoAsyncCompleted : _dupanAccountInfo is null pointer ");
            }

            if (_dupanAccountInfo.errno == -6)
            {
                MessageBoxBig.show("无法登录, 请去度盘网页手工登录一次以查实情况. 提示: 如解绑手机/未通过实名认证则会发生此类情况.\n 错误码: " + _dupanAccountInfo.errno);
            }
            else if (_dupanAccountInfo.errno != 0)
            {
                MessageBoxBig.show("网络错误? 无法从服务器获取用户信息. 错误代码, 建议去度盘手工登录一次看看.  " + _dupanAccountInfo.errno, "错误");
            }

            if (_dupanAccountInfo.diskquota == null)
            {
                MessageBoxBig.show("网络错误? 无法从服务器获取网盘可用空间信息.", "错误");
            }
            else if (_dupanAccountInfo.diskquota.errno != 0)
            {// .diskquota is not null
                MessageBoxBig.show("网络错误? 无法从服务器获取网盘可用空间信息. 错误代码 " + _dupanAccountInfo.diskquota.errno, "错误");
            }


            if (_dupanAccountInfo.errno != 0)
            {
                _dupanAccountInfo.username = "not login";

                _dupanAccountInfo.diskquota = null;
                _activeAccountLoginCredential = null;

            }
            else
            {

                Console.WriteLine("user name is " + _dupanAccountInfo.username);
                Console.WriteLine("user avatar is " + _dupanAccountInfo.url_avatar);
            }
            Mouse.OverrideCursor = null;
        }

        static public BitmapImage createBitmapImageFromLocalFile(string filename)
        {
            Console.WriteLine("bitmap filename is " + filename);
            // working
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.DecodePixelWidth = Avatar_Size;
            //const string uri_prefix_for_local_file = "file:///./";

            try
            {
                FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);

                // Uri uri = new Uri("http:/127.0.0.1/adfadfadsfadsfadsfsd.jpg");
                //Uri uri = new Uri(uri_prefix_for_local_file+filename);

                //bitmap.UriSource = uri;
                bitmap.StreamSource = fileStream;
                bitmap.EndInit();
                bitmap.Freeze();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            return bitmap;
        }

        // class MyBaiduDownloader
        public bool jumpBackToRemotePathAndListAndUpdateGui(int selectedIndex)
        {
            //jump back to a previous item in the path selector, could be any one.

            // 1 if it's the last item, do nothing
            if (_remotePathList.Count <= 0)
            {
                // or invalid, impossible, always should have at least one tiem
                Console.WriteLine("jumpBackToRemotePathAndListAndUpdateGui. impossible, list should at least have one item. do nothing.");
                return true;
            }

            if (selectedIndex < 0)
            {
                Console.WriteLine("jumpBackToRemotePathAndListAndUpdateGui none is selected. do nothing.");
                return true;
            }

            MyBaiduDownloader.RemoteFileItem rfi = _remotePathList[selectedIndex];
            Console.WriteLine("selected item is " + rfi.path);

            int lastIndex = _remotePathList.Count - 1;
            if (selectedIndex == lastIndex)
            { // do nothing , it's already the current path/current file list
                return true;
            }
            else
            {
                // 2 remove all items after the selected item
                for (int i = lastIndex; i > selectedIndex; i--)
                {
                    _remotePathList.RemoveAt(i);
                }
                //MyBaiduDownloader.RemoteFileListItem rfi = _remotePathList[selectedIndex];
                Console.WriteLine("selected is " + rfi.path);

                listRemoteFilesInPathAndUpdateGUI(rfi);
            }
            return true;
        }

        // class MyBaiduDownloader
        public bool enterNewRemotePathAndListAndUpdateGuiAsync(RemoteFileItem fileitem)
        {
            //listRemoteFilesInPathAndUpdateGUI(fileitem);
            //workerForListRemoteFilesInPath.RunWorkerAsync();

            _remotePathList.Add(fileitem);

            // list last item in the path selector.
            listRemoteFilesInPathAndUpdateGUIAsync();

            return true;
        }

        private void listRemoteFilesInPathAndUpdateGUIAsync()
        {
            Mouse.OverrideCursor = Cursors.Wait;

            _workerForListRemoteFilesInPath.RunWorkerAsync(getCurrentPathItem());

            StatusMessageWindow.showMessageStatic("Requesting remote file list....");
        }

        // class MyBaiduDownloader
        // only list folder item
        public bool listRemoteFilesInPathAndUpdateGUI(RemoteFileItem fileitem)
        {
            //listRemoteFilesInPath(fileitem);
            _workerForListRemoteFilesInPath.RunWorkerAsync(fileitem);

            //updateWindowsGui_filelist();

            return true;
        }

        // class MyBaiduDownloader
        // only list folder item, because a file has no files insdie. only a folder contains files inside.
        public ObservableCollection<RemoteFileItem> listRemoteFilesInPath(RemoteFileItem fileitem)
        {
            if (!fileitem.isFolder)
            {
                Console.WriteLine(fileitem.server_filename + " is not a folder");
                //not consider an error
                return null;
            }

            ObservableCollection<RemoteFileItem> filelist;

            //if already cached, do not list again, if it's null, it's not cached.
            if (fileitem.fileList == null)
            { // not data cached yet, must query from server
                Console.WriteLine("no data cached yet, query from server: " + fileitem.path);

                DupanFileListResult filelistresult = dupanNetDiskGetFilelist(fileitem.path);

                if (filelistresult == null)
                {
                    // cannot get filelist from server, network error
                    return null;
                }
                filelist = filelistresult.list;

                Console.WriteLine("Query Dupan server done for path: " + fileitem.path);

                // now check the result error_no
                RemoteFileItem.error_no = filelistresult.errno;

                //cache the list
                fileitem.fileList = filelist;
            }
            else
            {
                // if already cached, do not list again
                Console.WriteLine("have cached file list, reusing filelist of path " + fileitem.path);
                filelist = fileitem.fileList;
            }

            return filelist;
        }

        // class MyBaiduDownloader
        // then the _remoteFileList is updated.
        private ObservableCollection<RemoteFileItem> listRemoteFilesInPath(string path)
        {
            DupanFileListResult filelistresult = dupanNetDiskGetFilelist(path);

            if (filelistresult == null)
            {
                // cannot get filelist from server, network error
                return null;
            }
            ObservableCollection<RemoteFileItem> filelist = filelistresult.list;
            return filelist;
        }

        // class MyBaiduDownloader
        private void getDupanNetDiskUserInfo(DupanAccountInfo dupanAccount)
        {
            //DupanAccountInfo dupanAccount = new DupanAccountInfo();
            //Freeze();

            if (DEBUG_ONLINE_USERINFO)
            {
                string url_getUserInfo = "http://pan.baidu.com/api/user/getinfo?user_list=[" + _activeAccountLoginCredential.uid + "]";
                WebClient wc = new WebClient();
                wc.Encoding = System.Text.Encoding.UTF8;
                string httpTextResult;
                try
                {
                    string res = wc.DownloadString(url_getUserInfo);
                    wc.Headers.Add(HttpRequestHeader.Cookie, _activeAccountLoginCredential.fullCookie);
                    httpTextResult = wc.DownloadString(url_getUserInfo);

                    Console.WriteLine("user info result is: " + httpTextResult);
                }
                catch (Exception)
                {
                    return;
                }
                System.Web.Script.Serialization.JavaScriptSerializer jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                DupanUserInfoResult userInfoResult = jsonSerializer.Deserialize<DupanUserInfoResult>(httpTextResult);
                try
                {
                    dupanAccount.errno = userInfoResult.errno;

                    dupanAccount.username = userInfoResult.records[0].uname;
                    // set the avatar url, automatically load the image file if it's in local already
                    dupanAccount.url_avatar = userInfoResult.records[0].avatar_url;
                }
                catch (Exception)
                {
                    return;
                }
            }
            else
            {
                dupanAccount.username = "offlineuser";
                // testing local file
                dupanAccount.url_avatar = "https://img-prod-cms-rt-microsoft-com.akamaized.net/cms/api/am/imageFileData/RE1FU5k?ver=e2c0&q=90&m=6&h=423&w=752&b=%23FFFFFFFF&f=jpg&o=f&aim=true";
                dupanAccount.errno = 0;

                System.Threading.Thread.Sleep(500);
            }

            string imagefilename = null;
            // if avatar image has not been save /cached in local file
            if (dupanAccount.bitmapUserAvatarProperty == null)
            {
                // if by now it's still null, it means local has no such file
                // save the file

                imagefilename = saveAvatarToFile(dupanAccount.url_avatar);
                dupanAccount.bitmapUserAvatarProperty = createBitmapImageFromLocalFile(imagefilename);
            }

            return;
        }

        public string saveAvatarToFile(string url)
        {
            Uri uri = new Uri(url);
            string filename = null;

            filename = Path.GetFileName(uri.LocalPath);

            Console.WriteLine("avatar file name is " + filename);

            WebClient myWebClient = new WebClient();
            myWebClient.DownloadFile(uri, filename);
            Console.WriteLine("downloading " + filename + "...");

            return filename;
        }



        // class MyBaiduDownloader
        private DupanQuotaResult dupanNetDiskGetQuota()
        {
            DupanQuotaResult quotaResult;

            if (DEBUG_ONLINE_USERINFO == true)
            {
                //const string url_checkquota = "http://pan.baidu.com/api/quota?checkfree=1"; // free is useless. just the same as total.

                const string url_checkquota = "http://pan.baidu.com/api/quota";
                WebClient wc = new WebClient();

                wc.Headers.Add(HttpRequestHeader.Cookie, _activeAccountLoginCredential.fullCookie);
                string httpTextResult;
                try
                {
                    httpTextResult = wc.DownloadString(url_checkquota);
                }
                catch (Exception e)
                {
                    return null;
                }

                Console.WriteLine("quota result is: " + httpTextResult);
                // quota result is: {"errno":0,"total":2206539448320,"free":2206539448320,"request_id":2201771658391354269,"used":2187651101363}

                System.Web.Script.Serialization.JavaScriptSerializer jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                //string httpTextResult = "{ \"errno\":0,\"total\":2206539448320,\"free\":2206539448320,\"request_id\":2202865576884769239,\"used\":2187651101363}";

                quotaResult = jsonSerializer.Deserialize<DupanQuotaResult>(httpTextResult);
            }
            else
            {
                quotaResult = new DupanQuotaResult();
                quotaResult.total = 2000 * BytesPerGigaByte;
                quotaResult.used = 1890 * BytesPerGigaByte;
                quotaResult.errno = 0;
                System.Threading.Thread.Sleep(500);
            }
            //ulong percent = quotaResult.used / (quotaResult.total / 100);
            // Console.WriteLine(quotaResult.used + " / " + quotaResult.total + " used:" + percent + "%");

            return quotaResult;
        }

        // class MyBaiduDownloader
        private DupanFileListResult dupanNetDiskGetFilelist(string path)
        {
            string url_checkquota = "http://pan.baidu.com/api/list?page=1&num=10000000&dir=" + Uri.EscapeDataString(path);

            string httpTextResult;
            if (DEBUG_ONLINE_FILELIST)
            {
                WebClient wc = new WebClient();
                wc.Encoding = System.Text.Encoding.UTF8;
                wc.Headers.Add(HttpRequestHeader.Cookie, _activeAccountLoginCredential.fullCookie);

                try
                {
                    httpTextResult = wc.DownloadString(url_checkquota);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Network error? cannot get filelist. " + e);

                    return null;
                }

                //!! for remote test, or real work

                // could be super long and super slow !
                Console.WriteLine("file list result is: " + httpTextResult);
            }
            else
            {
                //for local file test only
                httpTextResult = testReadFileList("d:\\baiduPanDown\\filelistResult2.txt");
                // can only parse a file from console output.
                System.Threading.Thread.Sleep(500);
            }

            System.Web.Script.Serialization.JavaScriptSerializer jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            DupanFileListResult filelistResult = jsonSerializer.Deserialize<DupanFileListResult>(httpTextResult);

            return filelistResult;
        }

        // class MyBaiduDownloader
        public void remotePathSelectorGoBack()
        {
            if (_activeAccountLoginCredential == null)
            {
                MessageBoxBig.show("请先选择一个账号, 或者新建一个账号. 按切换账号按钮, 选择新用户.");
                return;
            }

            //always keep the last item in the remote path selector, which is root /
            if (_remotePathList.Count <= 1)
            {
                Console.WriteLine("already in the root, do nothing, cannot go back more");
                // do nothing
                return;
            }

            int lastIndex = _remotePathList.Count - 1;

            _remotePathList.RemoveAt(lastIndex);

            //now we have a new lastIndex
            lastIndex = _remotePathList.Count - 1;

            //current path item is the last item in the list
            RemoteFileItem currentPathItem = _remotePathList[lastIndex];

            Console.WriteLine("go back to path: " + currentPathItem.path);

            listRemoteFilesInPathAndUpdateGUI(currentPathItem);
        }

        // class MyBaiduDownloader

        private BackgroundWorker _workerForListRemoteFilesInPath = new BackgroundWorker();
        private BackgroundWorker _workerForGetUserInfo = new BackgroundWorker();

        // class MyBaiduDownloader
        public void refreshCurrentRemotePath_and_UpdateGui_Async()
        {
            if (_activeAccountLoginCredential == null)
            {
                MessageBoxBig.show("没有可用的本地登录信息. 请选择另外一个用户, 或先新建一个账号:按切换账号按钮, 选择新用户.");
                return;
            }

            if (_remotePathList.Count <= 0)
            {
                Console.WriteLine("Error, no current path?? Should have added the first root item in init() and not allowed to remove. MyBaiduDownloader._remotePathList.Count ==0 ");
                return;
            }


            MyBaiduDownloader.RemoteFileItem cuurentPathItem = getCurrentPathItem();
            Console.WriteLine("refreshing current path " + cuurentPathItem.path);

            //doing refresh, so clear the cached list
            cuurentPathItem.fileList = null;
            this.listRemoteFilesInPathAndUpdateGUIAsync();
        }

        // class MyBaiduDownloader
        //which is for current path
        public RemoteFileItem getCurrentPathItem()
        {
            int lastIndex = _remotePathList.Count - 1;
            MyBaiduDownloader.RemoteFileItem currentPathItem = _remotePathList[lastIndex];

            return currentPathItem;
        }

        // class MyBaiduDownloader
        private void doWorkListRemoteFilesInPathAsync(object sender, DoWorkEventArgs e)
        {
            // get the current path.
            // = getCurrentPathItem();
            MyBaiduDownloader.RemoteFileItem currentPathItem = e.Argument as MyBaiduDownloader.RemoteFileItem;

            ObservableCollection<RemoteFileItem> filelist = listRemoteFilesInPath(currentPathItem);
            e.Result = filelist;
        }

        // class MyBaiduDownloader
        private void doWorkListRemoteFilesInPathAsyncCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            StatusMessageWindow.closeStatic();

            remoteFileListProperty = e.Result as ObservableCollection<RemoteFileItem>;
            //
            if (remoteFileListProperty == null)
            {
                MessageBoxBig.show("无法从服务器获取 < " + getCurrentPathItem().path + " > 目录的文件列表信息, 错误代码 " + RemoteFileItem.error_no, "错误");
            }
            Mouse.OverrideCursor = null;
        }

        // class MyBaiduDownloader
        static public string testReadFileList(string filefullpath)
        {
            string text;
            try
            {
                text = System.IO.File.ReadAllText(filefullpath);
            }
            catch (Exception)
            {
                // config file not exist
                Console.WriteLine("can't open file list file to read.");
                return null;
            }
            return text;
        }

        // add the new loginInfo into the dict, set the active account login info
        // class MyBaiduDownloader //more like a addNewCredential()
        private void startAccessDupanWithNewLoginCredential(DupanLoginCredential newLoginCred)
        {
            if (newLoginCred == null)
            {
                //cannot get a new login credential from last open browser
                return;
            }

            if (_appConfigData._loginCredentialConfig == null)
            {
                _appConfigData._loginCredentialConfig = new LoginCredentialConfig();
            }

            // set active uid
            _appConfigData._loginCredentialConfig._activeAccountUid = newLoginCred.uid;
            //add the new loginInfo to dict
            _appConfigData._loginCredentialConfig._loginCredentialsDict[newLoginCred.uid] = newLoginCred;

            //save to config file
            _appConfigMgr.saveAppConfigDataToToFile(AppConfigFileName, _appConfigData);

            _activeAccountLoginCredential = newLoginCred;

            //need to do a lot with the newly get loginInfo
            loginDupanAccountToGetInfo();

            _dataminer = null;
            //(_dataminer as IDisposable).Dispose();
            return;
        }

        // class MyBaiduDownloader
        public DupanLoginCredential getActiveAccountLoginCredential()
        {
            return _activeAccountLoginCredential;
        }

        // class MyBaiduDownloader
        public bool addRemoteFileToDownloadListPublic(RemoteFileItem rfi, string savepath)
        {

            addRemoteFileToDownloadList(rfi, savepath);
            //_downloadManager.saveDownloadTaskListSmart();
            return true;
        }

        // class MyBaiduDownloader
        // adddownload   add download!!
        private bool addRemoteFileToDownloadList(RemoteFileItem rfi, string savepath)
        {
            if (rfi.isFolder)
            {
                // if it's folder, add every item inside the folder, could be another folder!

                //iterate
                if (rfi.fileList == null)
                {
                    // no filelist in folder, need to list the folder first
                    Console.WriteLine("no data cached yet, query from server: " + rfi.path);
                    rfi.fileList = listRemoteFilesInPath(rfi.path);
                    if (rfi.fileList == null)
                    {
                        return false;
                    }
                    Console.WriteLine("Query server done for path: " + rfi.path);
                }

                foreach (RemoteFileItem rfi_item in rfi.fileList)
                { //recurse calling self function to add all children of the input rfi
                    bool result = addRemoteFileToDownloadList(rfi_item, Path.Combine(savepath, rfi.filename));
                    if (result == false)
                    {
                        return false;
                    }
                }
            }
            else
            {
                _downloadManager.addRemoteFileOnlyItemToDownloadList(rfi, savepath, _activeAccountLoginCredential);
                Console.WriteLine(rfi.filename);
            }

            return true;
        }

        public class DownloadManager : INotifyPropertyChanged
        {
            private const int RefreshLinksAfterHours = 5;
            private const string DOWNLOADTASKLISTFNAME = "downloadlist.dat";
            private const string COMPLETELISTFILENAME = "completedlist.dat";
            private const int FAIL_RETRY_WAIT = 5000;
            public const string Aria2Ext = ".aria2";

            ~DownloadManager()
            {
                saveDownloadTaskList();
                saveCompletedFileList();
            }



            private class Aria2cStatus
            {
                public const string ACTIVE = "active";
                public const string ERROR = "error";
                public const string COMPLETED = "complete";
            };


            public AppConfigManager.DownloadConfigAndroid _downloadConfig { get; private set; }
            private Aria2cHelper _ariaHelper;

            private BackgroundWorker _workerForDownloadList = new BackgroundWorker();

            // private Timer _timer_ariaTellStatus;
            //Timer _timer_downloadListProcess;

            // all items in this queue need to request links and launch download
            private Queue<ToDownloadFileItem> _processQueue = new Queue<ToDownloadFileItem>();

            private ToDownloadFileList _toDownloadFileList;

            private CompletedFileList _completedFileList;

            // class DownloadManager
            public bool saveCompletedFileList()
            {
                return _completedFileList.save();
            }

            // class DownloadManager
            public ObservableCollection<ToDownloadFileItem> completedFileListProperty
            {
                get { return _completedFileList.completedFileListProperty; }
            }

            // class DownloadManager
            public void checkMd5Async(ToDownloadFileItem tdfi)
            {
                _completedFileList.checkMd5Async(tdfi);
            }

            // class DownloadManager
            public void deleteDownloadedFileAndRedownload(ToDownloadFileItem tdfi)
            {
                if (File.Exists(tdfi.localfullpathfilename))
                {
                    File.Delete(tdfi.localfullpathfilename);
                    Console.WriteLine("Deleting " + tdfi.localfullpathfilename);
                }
                string tempfile = tdfi.localfullpathfilename + Aria2Ext;
                if (File.Exists(tempfile))
                {
                    File.Delete(tempfile);
                }

                tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Pending;
                toDownloadFileListProperty.Add(tdfi);


            }






            // class DownloadManager

            private class CompletedFileList
            {
                // constructor
                public CompletedFileList()
                {
                    _workerMd5Hash.DoWork += doWorkComputeHashAsync;
                    _workerMd5Hash.RunWorkerCompleted += doWorkComputeHashAsyncCompleted;
                    _workerMd5Hash.ProgressChanged += _workerMd5Hash_ProgressChanged;

                    _workerMd5Hash.WorkerReportsProgress = true;
                }

                //=================================================
                // for md5 checking

                private Queue<ToDownloadFileItem> _queue_md5 = new Queue<ToDownloadFileItem>();
                // ToDownloadFileItem _tdfi_md5_checking;

                private BackgroundWorker _workerMd5Hash = new BackgroundWorker();

                // CompletedFileList
                public void checkMd5Async(ToDownloadFileItem tdfi)
                {
                    if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Done ||
                        tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Md5Failed ||
                        tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Md5Passed ||
                        tdfi.taskStatus == ToDownloadFileItem.TaskStatus.File_Missing
                        )
                    {
                        tdfi.progress = 0;
                        _queue_md5.Enqueue(tdfi);

                        if (!_workerMd5Hash.IsBusy)
                        {
                            _workerMd5Hash.RunWorkerAsync();
                        }
                    }
                }

                // CompletedFileList

                public void completedListDeleteTaskAndFile(ToDownloadFileItem tdfi)
                {
                    int removedCount = 0;
                    // remove from task list

                    // remove disk files
                    if (File.Exists(tdfi.localfullpathfilename))
                    {
                        File.Delete(tdfi.localfullpathfilename);
                        Console.WriteLine("Deleting " + tdfi.localfullpathfilename);
                        removedCount++;
                    }
                    string tempfile = tdfi.localfullpathfilename + Aria2Ext;
                    if (File.Exists(tempfile))
                    {
                        File.Delete(tempfile);
                    }

                }

                // CompletedFileList
                private void doWorkComputeHashAsync(object sender, DoWorkEventArgs e)
                {
                    //ToDownloadFileItem tdfi = e.Argument as ToDownloadFileItem;
                    getMd5ForAllFilesInQueue();
                }

                private void getMd5ForAllFilesInQueue()
                {
                    while (_queue_md5.Count >= 1)
                    {
                        ToDownloadFileItem tdfi = _queue_md5.Peek();
                        string actualmd5 = getMd5ForFile(tdfi);

                        tdfi.localMd5 = actualmd5;

                        if (string.IsNullOrEmpty(actualmd5))
                        {
                            // local downloaded file is missing
                            tdfi.progress = 0;
                            tdfi.taskStatus = ToDownloadFileItem.TaskStatus.File_Missing;
                            tdfi.taskStatusUserInfoTextProperty = "本地文件不存在? 已经被删除了?";
                            _queue_md5.Dequeue();

                            continue;
                        }
                        tdfi.progress = 100;
                        int newstat = 0;
                        if (actualmd5.Equals(tdfi.serverMd5))
                        {
                            newstat = ToDownloadFileItem.TaskStatus.Md5Passed;
                            tdfi.taskStatusUserInfoTextProperty = "文件校验正确";
                        }
                        else
                        {
                            newstat = ToDownloadFileItem.TaskStatus.Md5Failed;
                            tdfi.taskStatusUserInfoTextProperty = "服务端Md5: " + tdfi.serverMd5 + " . 本地文件的MD5跟服务端记录的Md5不符. 但这不一定代表文件下载错误, 因为度盘的MD5有时是错的. 关于MD5的详细说明请看软件说明.";
                        }
                        _queue_md5.Dequeue();

                        tdfi.taskStatus = newstat;
                    }
                    smartSave();
                }


                // CompletedFileList
                private void _workerMd5Hash_ProgressChanged(object sender, ProgressChangedEventArgs e)
                {
                    if (_queue_md5.Count >= 1)
                    {
                        ToDownloadFileItem tdfi = _queue_md5.Peek();
                        tdfi.progress = e.ProgressPercentage;
                    }
                }

                // CompletedFileList
                private void doWorkComputeHashAsyncCompleted(object sender, RunWorkerCompletedEventArgs e)
                {
                    Console.WriteLine("MD5 check completed.s");
                }

                // CompletedFileList
                public string getMd5ForFile(ToDownloadFileItem tdfi)
                {
                    string hash = "";
                    FileStream fileStream;
                    try
                    {
                        fileStream = File.OpenRead(tdfi.localfullpathfilename);
                    }
                    catch (Exception)
                    {
                        Console.WriteLine("Cannot open file " + tdfi.filename);
                        return null;
                    }
                    const ulong BlockSize = 512 * BytesPerKiloByte;
                    long size = fileStream.Length;
                    MD5 md5Hash = MD5.Create();

                    int bytesRead = 0;
                    UInt64 totalBytesRead = 0;
                    do
                    {
                        byte[] buffer = new byte[BlockSize];
                        bytesRead = fileStream.Read(buffer, 0, buffer.Length);
                        totalBytesRead = totalBytesRead + (ulong)bytesRead;
                        if (bytesRead == 0)
                        {
                            md5Hash.TransformFinalBlock(buffer, 0, bytesRead);
                        }
                        else
                        {
                            md5Hash.TransformBlock(buffer, 0, bytesRead, null, 0);
                        }
                        _workerMd5Hash.ReportProgress((int)(totalBytesRead * 100.0 / size));
                    }
                    while (bytesRead != 0);

                    hash = BitConverter.ToString(md5Hash.Hash).Replace("-", string.Empty); ;
                    hash = hash.ToLower();

                    return hash;
                }


                //=================================================
                public ObservableCollection<ToDownloadFileItem> _filelist = new ObservableCollection<ToDownloadFileItem>();


                [ScriptIgnore]
                // CompletedFileList
                public ObservableCollection<ToDownloadFileItem> completedFileListProperty
                {
                    get { return _filelist; }

                    private set
                    {
                        _filelist = value;
                        // not necessary ?
                        OnPropertyChanged("completedFileListProperty");
                    }
                }

                // class CompletedFileList
                static public CompletedFileList load()
                {
                    string text;
                    try
                    {
                        text = System.IO.File.ReadAllText(COMPLETELISTFILENAME);
                    }
                    catch (Exception)
                    {
                        // file not exist, which is not en error
                        Console.WriteLine("can't open download queue file to read.");
                        return null;
                    }

                    if (string.IsNullOrEmpty(text))
                    {
                        return null;
                    }
                    System.Web.Script.Serialization.JavaScriptSerializer jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    CompletedFileList list;
                    try
                    {
                        list = jsonSerializer.Deserialize<CompletedFileList>(text);
                    }
                    catch (Exception)
                    {
                        list = null;
                    }

                    return list;
                }

                // class CompletedFileList
                public void add(ToDownloadFileItem donetdfi)
                {
                    if (donetdfi == null)
                    {
                        MessageBoxBig.show("CompletedFileList.add(donetdfi): donetdfi is null pointer ");

                        return;
                    }
                    _filelist.Add(donetdfi);
                    save();
                }


                // class CompletedFileList
                public void remove(ToDownloadFileItem donetdfi)
                {
                    _filelist.Remove(donetdfi);
                    smartSave();
                }

                // class CompletedFileList
                public void clear()
                {
                    _filelist.Clear();
                    save();
                }

                private const int MINSAVEINTERVALSEC = 5;
                private DateTimeOffset saveTime;
                // class CompletedFileList
                public void smartSave()
                {
                    if (saveTime == null)
                    {
                        // never saved
                        Console.WriteLine("smart save, saving for the first time");
                        save();
                        saveTime = DateTimeOffset.Now;

                    }
                    else
                    {
                        TimeSpan span = DateTimeOffset.Now.Subtract(saveTime);
                        if (span.TotalSeconds > MINSAVEINTERVALSEC)
                        {
                            Console.WriteLine("smart save, time passed " + MINSAVEINTERVALSEC);
                            save();
                            saveTime = DateTimeOffset.Now;

                        }
                        else
                        {
                            Console.WriteLine("time too short, skip saving this time");
                            // time too short don't save again
                        }
                    }
                    return;
                }

                // class CompletedFileList
                public bool save()
                {
                    try
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        string text = serializer.Serialize(this);
                        System.IO.File.WriteAllText(COMPLETELISTFILENAME, text);
                        Console.WriteLine("completedList saved!");
                    }
                    catch (Exception e)
                    {
                        // MessageBoxBig.show("Error! cannot save completedList file!! " + COMPLETELISTFILENAME + " "+ e.Message);
                        // sometimes, while doing serialization, the object is modified, so exception.
                        Console.WriteLine("error while saving completedList, modified while being serialized. "+ e.Message);
                        return false;
                    }
                    return true;
                }

                // class CompletedFileList


                //class    CompletedFileList
                public event PropertyChangedEventHandler PropertyChanged;

                protected virtual void OnPropertyChanged(string propertyName = null)
                {
                    PropertyChangedEventHandler handler = PropertyChanged;
                    if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            public void completedListRemove(ToDownloadFileItem tdfi)
            {
                _completedFileList.remove(tdfi);
            }



            //constructor DownloadManager
            public DownloadManager(AppConfigManager.DownloadConfigAndroid downloadconfig)
            {
                _downloadConfig = downloadconfig;



                this.loadDownloadTaskList();

                if (_toDownloadFileList == null)
                {
                    _toDownloadFileList = new ToDownloadFileList();
                }

                this.loadCompletedList();
                if (_completedFileList == null)
                {
                    _completedFileList = new CompletedFileList();
                }

                _workerForDownloadList.DoWork += dowork_workerForStartProcessDownloadQueueAllItemsAsync;
                _workerForDownloadList.RunWorkerCompleted += dowork_workerForStartProcessDownloadQueueAllItemsAsyncCompleted;
                /*
                _timer_ariaTellStatus = new System.Timers.Timer(QueryTellStateTimerBetweenEachItemInterval);
                _timer_ariaTellStatus.Elapsed += OnTimedEventAriaTellStatus;
                _timer_ariaTellStatus.AutoReset = true;
                _timer_ariaTellStatus.Enabled = true;
                */
            }

            // DownloadManager
            /*
            private void OnTimedEventAriaTellStatus(Object source, ElapsedEventArgs e)
            {
                _timer_ariaTellStatus.Enabled = false;

                Console.WriteLine("timer....");
                queryAria2cAllFilesStatus();

                _timer_ariaTellStatus.Enabled = true;
            }
            */

            //DownloadManager
            public void queryAria2cAllFilesStatus()
            {
                UInt64 totalSpeed = 0;
                Queue<ToDownloadFileItem> queue = new Queue<ToDownloadFileItem>();

                foreach (ToDownloadFileItem tdfi in toDownloadFileListProperty)
                {
                    queue.Enqueue(tdfi);
                }

                while (queue.Count > 0)
                {
                    ToDownloadFileItem tdfi = queue.Dequeue();

                    if (!toDownloadFileListProperty.Contains(tdfi))
                    {
                        // could have been removed from the download list
                        continue;
                    }

                    try
                    {
                        if (string.IsNullOrEmpty(tdfi.aria2c_gid))
                        {
                            // not added to aria2c yet, so no gid, so no need to query aria status
                            continue;
                        }

                        // get aria status for each tdfi
                        Aria2cHelper.Aria2cResult.TellStatusResult res = _ariaHelper.tellStatus(tdfi);
                        if (res == null)
                        {
                            // can not get result.
                            continue;
                        }

                        // set the speed
                        tdfi.speedString = res.result.downloadSpeed;

                        //========  set total speed  ========
                        UInt64 thisfilespeed = 0;
                        if (!string.IsNullOrEmpty(res.result.downloadSpeed))
                        {
                            thisfilespeed = UInt64.Parse(res.result.downloadSpeed);
                        }
                        totalSpeed += thisfilespeed;
                        //========  end of speed handling  ========

                        tdfi.completedLength = res.result.completedLength;
                        if (res.result.status.Equals(Aria2cStatus.COMPLETED))
                        {
                            if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Downloading)
                            {
                                downloadingFileCountProperty--;
                                tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Done;
                                tdfi.finishdatatime = DateTimeOffset.Now.ToString();

                            }
                        }
                        // 1. copy error message to tdfi from result
                        // 2. change status
                        // 3. remove from aria2c
                        handleFailedItems(tdfi, res);

                        // if sleep here, the foreach will take long long time , and during that, the list is likely be modified and have a exception.
                    }
                    //  end of foreach (ToDownloadFileItem tdfi in toDownloadFileListProperty)
                    catch (Exception ex)
                    {
                        // if the list is modified, it throws a exception but it's not a big deal
                        // just ignore.
                        Console.WriteLine("gid: " + tdfi.aria2c_gid + ex);
                    }

                }  // end of while (queue.Count > 0)

                downloadQueueTotalSpeedStringProperty = totalSpeed.ToString();

            }  //end of public void queryAria2cAllFilesStatus()




            private void handleFailedItems(ToDownloadFileItem tdfi, Aria2cHelper.Aria2cResult.TellStatusResult tellstatresult)
            {
                Aria2cHelper.Aria2cResult.TellStatusResult.TellStatusResultData result = tellstatresult.result;

                if (result.errorCode == 0)
                {
                    tdfi.taskStatusUserInfoTextProperty = "normal";
                    return;
                }
                else if (result.errorCode == Aria2cHelper.Aria2cResult.ErrorCode24)
                {
                    tdfi.taskStatusUserInfoTextProperty = "links expired, Auto refreshing..." + result.errorCode + " " + result.errorMessage;
                    // link expire, link invalid, need refresh, and retry
                    tdfi._downloadLinksResult = null;
                }
                else if (result.errorCode == Aria2cHelper.Aria2cResult.ErrorCode1)
                {
                    tdfi.taskStatusUserInfoTextProperty = "network error...retrying. " + result.errorCode + " " + result.errorMessage;
                    // local network error, or file not exist ?
                }
                else
                {
                    // 22 , error, wrong user, wrong cookie, so file not exist?
                    // do nothing ?
                    if (result.errorCode != 0)
                    {
                        //if it's downloading then need reduce count
                        if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Downloading)
                        {
                            tdfi.taskStatusUserInfoTextProperty = "error code " + result.errorCode + " " + result.errorMessage;
                            //_toDownloadFileList.saveDownloadTaskList();
                        }
                    }
                }

                _ariaHelper.aria2RemoveDownloadResult(tdfi);
                //!!!! tdfi.errorCode = result.errorCode;
                tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Failed;
                tdfi.setGid(null);

                downloadingFileCountProperty--;
            }


            private bool isNowInSpeedLimitTimeRange()
            {
                bool islimit = false;

                DateTimeOffset dto_now = DateTimeOffset.Now;
                TimeSpan tsp_now = dto_now.TimeOfDay;

                double nowSeconds = tsp_now.TotalSeconds;
                double startSecond = _downloadConfig.timespan_SpeedLimitStartSecond;
                double endSecond = _downloadConfig.timespan_SpeedLimitEndSecond;

                if (startSecond < endSecond)
                {
                    if ((nowSeconds > startSecond) && (nowSeconds < endSecond))
                    {
                        Console.WriteLine("1 now is in speed limit time range");
                        islimit = true;
                    }
                    else
                    {
                        Console.WriteLine("1 now is NOT in speed limit time range");
                        islimit = false;
                    }
                }
                else
                {
                    // start time is 11:00PM (23:00), end time is 2:00AM, so start time is larger than end time


                    // consider start 8:00PM , end 12:00AM
                    if (nowSeconds > startSecond || nowSeconds < endSecond)
                    {
                        Console.WriteLine("2 now is in speed limit time range");
                        islimit = true;
                    }
                    else
                    {
                        Console.WriteLine("2 now is NOT in speed limit time range");
                        islimit = false;
                    }
                }


                // 11:00PM to 2:00AM
                // 12:00 AM to 2:00AM
                // 2:00AM to 8:00 AM
                // 12:00PM to 3:00PM
                //

                return islimit;
            }

            //DownloadManager
            private string getDownloadSpeedByTime()
            {
                string newSpeed;
                if (_downloadConfig.enableSpeedLimit)
                {
                    // TimeSpan timespan_SpeedLimitStart { get; set; }
                    // TimeSpan timespan_SpeedLimitEnd { get; set; }

                    bool isLimit = isNowInSpeedLimitTimeRange();

                    if (isLimit)
                    {
                        newSpeed = newSpeed = _downloadConfig.speedLimitK.ToString() + "K";
                    }
                    else
                    {
                        newSpeed = _downloadConfig.getSpeedFullNumberAndUnit();
                    }

                    // https://stackoverflow.com/questions/11435781/a-binding-can-only-be-set-on-a-dependencyproperty-of-a-dependencyobject


                }
                else
                {
                    newSpeed = _downloadConfig.getSpeedFullNumberAndUnit();
                }

                return newSpeed;
            }

            //DownloadManager
            public bool applyAria2Config()
            {
                if (DEBUG_RUN_ARIA2C)
                {
                    //    return true;
                }

                bool resOK = _ariaHelper.setGlobalMaxConcurrentDownload(_downloadConfig.maxFilesConcurrentDownload);
                if (!resOK)
                {
                    return resOK;
                }

                resOK = setSpeedForCurrentTime();

                return resOK;
            }


            public bool setSpeedForCurrentTime()
            {
                string newspeed = getDownloadSpeedByTime();
                if (newspeed.Equals(_downloadConfig.currentSpeed))
                {
                    // same speed, no need to change
                    Console.WriteLine("Same speed, skip");
                    return true;
                }

                Console.WriteLine("Setting new speed " + newspeed);
                bool resOK = _ariaHelper.setGlobalSpeedLimit(newspeed);
                if (resOK)
                {
                    _downloadConfig.currentSpeed = newspeed;
                }
                else
                {
                    MessageBoxBig.show("Failed to set speed limit: " + newspeed);
                }
                return resOK;
            }


            //DownloadManager
            private bool aria2cInit()
            {
                bool res = true;
                if (_ariaHelper != null)
                {
                    return true;
                }

                _ariaHelper = new Aria2cHelper(_downloadConfig);

                res = _ariaHelper.runAria2c();

                if (!res)
                {
                    MessageBoxBig.show("无法创建并初始化下载服务. 文件正在被使用! 杀进程或者重启电脑试试. 又可能是某些杀毒软件误报误杀.");

                    return res;
                }
                applyAria2Config();

                if (DEBUG_RUN_ARIA2C)
                {
                    res = applyAria2Config();
                    if (!res)
                    {
                        MessageBoxBig.show("无法给下载进程应用配置文件");
                        return res;
                    }
                }

                return res;
            }

            //DownloadManager
            // need this to update GUI when filelist items changes
            public ObservableCollection<ToDownloadFileItem> toDownloadFileListProperty
            {
                get { return _toDownloadFileList.toDownloadFileListProperty; }
            } // a filelist for current folder to show.

            // class DownloadManager
            private class ToDownloadFileList
            {
                //public bool _downloadListIsUpdated { get; private set; } = false;

                private ObservableCollection<ToDownloadFileItem> _todownloadfilelist_obsCollection
                        = new ObservableCollection<ToDownloadFileItem>();

                private UInt64 getTotalDownloadQueueFileSize()
                {
                    UInt64 totalsize = 0;
                    foreach (ToDownloadFileItem tdfi in _todownloadfilelist_obsCollection)
                    {
                        totalsize += tdfi.size;
                    }
                    return totalsize;
                }

                //class  ToDownloadFileList
                public ToDownloadFileItem getFirstItemOfStatFromList(int stat)
                {
                    if (_todownloadfilelist_obsCollection == null)
                    {
                        return null;
                    }
                    if (_todownloadfilelist_obsCollection.Count <= 0)
                    {
                        return null;
                    }
                    foreach (ToDownloadFileItem tdfi in _todownloadfilelist_obsCollection)
                    {
                        if (tdfi.taskStatus == stat)
                        {
                            return tdfi;
                        }
                        //only get first item, do not continue the for.
                    }// end of foreach (ToDownloadFileItem tdfi in todownloadlist)
                    return null;
                }

                //class  ToDownloadFileList
                public void moveToLast(ToDownloadFileItem tdfi)
                {
                    if (_todownloadfilelist_obsCollection.Count <= 1)
                    {
                        // only one item , can not move
                        return;
                    }
                    int itemIndex = _todownloadfilelist_obsCollection.IndexOf(tdfi);
                    int lastIndex = _todownloadfilelist_obsCollection.Count - 1;
                    if (itemIndex < 0)
                    {
                        // already gone !
                        return;
                    }
                    if (itemIndex != lastIndex)
                    {
                        _todownloadfilelist_obsCollection.Move(itemIndex, lastIndex);
                    }
                }

                //class  ToDownloadFileList
                public void moveToFirst(ToDownloadFileItem tdfi)
                {
                    int itemIndex = _todownloadfilelist_obsCollection.IndexOf(tdfi);

                    _todownloadfilelist_obsCollection.Move(itemIndex, 0);
                }


                //class  ToDownloadFileList
                private bool downloadListAlreadyContains(ToDownloadFileItem tdfi)
                {
                    bool alreadyContains = false;
                    foreach (ToDownloadFileItem temptdfi in _todownloadfilelist_obsCollection)
                    {
                        if (temptdfi.remotePath.Equals(tdfi.remotePath) && temptdfi.filename.Equals(tdfi.filename)
                            && temptdfi.account.Equals(tdfi.account)
                            )
                        {
                            // only if the existing tdfi has same user account and same path and same file name, then it's already existing.
                            alreadyContains = true;
                            break;
                        }
                    }

                    return alreadyContains;
                }

                //class  ToDownloadFileList
                private DateTimeOffset saveTime;

                public void addToDownloadFileItemToList(ToDownloadFileItem tdfi)
                {
                    if (downloadListAlreadyContains(tdfi))
                    {
                        //already in the list, do not add duplicated files twice!
                        Console.WriteLine("already contains, do not add again " + tdfi.filename);
                        return;
                    }
                    _todownloadfilelist_obsCollection.Add(tdfi);

                    downloadQueueTotalFileSizeInt += tdfi.size;
                    // so to update GUI queue number
                    downloadQueueItemsCountProperty = (_todownloadfilelist_obsCollection.Count);
                    smartSave();
                }

                //class  ToDownloadFileList
                public void RemoveToDownloadFileItemToList(ToDownloadFileItem tdfi)
                {
                    _todownloadfilelist_obsCollection.Remove(tdfi);

                    if (_todownloadfilelist_obsCollection.Count == 0)
                    {
                        downloadQueueTotalFileSizeInt = 0;
                    }
                    else
                    {
                        downloadQueueTotalFileSizeInt = getTotalDownloadQueueFileSize();
                    }
                    if (downloadQueueTotalFileSizeInt < 0)
                    {
                        downloadQueueTotalFileSizeInt = 0;
                    }

                    downloadQueueItemsCountProperty = _todownloadfilelist_obsCollection.Count;
                    smartSave();
                }

                //class  ToDownloadFileList
                public bool saveDownloadTaskList()
                {

                    //set false first so it can be saved to file
                    //after load it's still false so meaning it's clean

                    try
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        string text = serializer.Serialize(this);
                        System.IO.File.WriteAllText(DOWNLOADTASKLISTFNAME, text);
                        Console.WriteLine("saveDownloadTaskList, download tasklist saved!");
                        saveTime = DateTimeOffset.Now;
                    }
                    catch (Exception)
                    {
                        MessageBoxBig.show("cannot save download list file " + DOWNLOADTASKLISTFNAME);
                        return false;
                    }

                    return true;
                }

                // class ToDownloadFileList
                private const int MINSAVEINTERVALSEC = 5;

                public bool smartSave()
                {
                    bool res = true;
                    if (saveTime == null)
                    {
                        // never saved
                        Console.WriteLine("smart save, saving for the first time");
                        res = saveDownloadTaskList();
                    }
                    else
                    {
                        TimeSpan span = DateTimeOffset.Now.Subtract(saveTime);
                        if (span.TotalSeconds > MINSAVEINTERVALSEC)
                        {
                            Console.WriteLine("smart save, time passed " + MINSAVEINTERVALSEC);
                            res = saveDownloadTaskList();
                        }
                        else
                        {
                            // time too short don't save again
                        }
                    }
                    return res;
                }

                // class ToDownloadFileList
                static public ToDownloadFileList loadDownloadTaskList()
                {
                    string downloadququetext;
                    try
                    {
                        downloadququetext = System.IO.File.ReadAllText(DOWNLOADTASKLISTFNAME);
                    }
                    catch (Exception)
                    {
                        // file not exist, which is not en error
                        Console.WriteLine("can't open download queue file to read.");
                        return null;
                    }

                    if (string.IsNullOrEmpty(downloadququetext))
                    {
                        return null;
                    }
                    System.Web.Script.Serialization.JavaScriptSerializer jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

                    ToDownloadFileList toDownloadFileList = jsonSerializer.Deserialize<ToDownloadFileList>(downloadququetext);

                    return toDownloadFileList;
                }

                //class  ToDownloadFileList
                // need this to update GUI when filelist items changes
                public ObservableCollection<ToDownloadFileItem> toDownloadFileListProperty
                {
                    get { return _todownloadfilelist_obsCollection; }

                    set
                    {
                        _todownloadfilelist_obsCollection = value;
                        OnPropertyChanged("toDownloadFileListProperty");
                    }
                } // a filelist for current folder to show.

                //class  ToDownloadFileList
                private int _downloadQueueItemsCount = 0;

                public int downloadQueueItemsCountProperty
                {
                    set
                    {
                        _downloadQueueItemsCount = value;
                        OnPropertyChanged("downloadQueueItemsCountProperty");
                    }
                    get
                    {
                        return _downloadQueueItemsCount;
                    }
                }

                //class  ToDownloadFileList
                private ulong _downloadQueueTotalFileSizeInt = 0;

                public ulong downloadQueueTotalFileSizeInt
                {
                    get { return _downloadQueueTotalFileSizeInt; }
                    set
                    {
                        _downloadQueueTotalFileSizeInt = value;
                        //also update the string value
                        downloadQueueTotalFileSizeStringProperty = sizeByteIntToSizeHuman(value);
                    }
                }

                //class  ToDownloadFileList
                private string _downloadQueueTotalFileSizeString = "";

                public string downloadQueueTotalFileSizeStringProperty
                {
                    set
                    {
                        _downloadQueueTotalFileSizeString = value;
                        OnPropertyChanged("downloadQueueTotalFileSizeStringProperty");
                    }
                    get
                    {
                        return _downloadQueueTotalFileSizeString;
                    }
                }



                //class  ToDownloadFileList
                public event PropertyChangedEventHandler PropertyChanged;

                protected virtual void OnPropertyChanged(string propertyName = null)
                {
                    PropertyChangedEventHandler handler = PropertyChanged;
                    if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            // class MyBaidu

            // class DownloadManager
            public void addRemoteFileOnlyItemToDownloadList(RemoteFileItem rfi, string savepath, DupanLoginCredential logincredential)
            {
            
               /* !!!! 
                * bool aria2cInitOK = this.aria2cInit();
                if (!aria2cInitOK)
                {
                    MessageBoxBig.show("即将关闭本软件...");
                    App.Current.Shutdown();
                }
                */
                ToDownloadFileItem tdfi = new ToDownloadFileItem(rfi, savepath, logincredential.uid);
                _toDownloadFileList.addToDownloadFileItemToList(tdfi);


                //downloadQueueTotalFileSizeInt += tdfi.size;
                // so to update GUI queue number
                //downloadQueueItemsCountProperty = (_toDownloadFileList.Count).ToString();
                downloadQueueTotalFileSizeStringProperty = _toDownloadFileList.downloadQueueTotalFileSizeStringProperty;
            }




            // class DownloadManager
            // should check tdfi.status for failure
            private void processOneToDownloadFileItem(ToDownloadFileItem tdfi)
            {
                // check existing link first
                bool linkExpired = true; ;

                if (tdfi.getLoginCredential() == null)
                {
                    tdfi.taskStatusUserInfoTextProperty = tdfi.account + ": 账户不存在";

                    return;
                }


                // if the item has no download links, ask server
                linkExpired = isToDownloadFileItemLinksExpired(tdfi);
                if (linkExpired)
                {
                    Console.WriteLine("link expired, need get link for " + tdfi.filename + ", total downloading " + downloadingFileCountProperty);
                }


                if (tdfi._downloadLinksResult == null || linkExpired)
                { // no link , need to get links again.
                    bool resOK = getDownloadLinksForFile(tdfi);
                    if (!resOK)
                    {
                        tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Failed;
                        tdfi.taskStatusString = ToDownloadFileItem.NETWORKERROR;
                        Console.WriteLine("get link error for " + tdfi.filename);
                        return;
                    }
                    else
                    {
                        Console.WriteLine("get link OK " + tdfi.filename + ", total downloading " + downloadingFileCountProperty);

                    }
                }
                else
                {
                    Console.WriteLine("no need get link again. start downloading " + tdfi.filename + ", total downloading " + downloadingFileCountProperty);
                }

                Console.WriteLine("Links OK now Starting real download work");

                bool res = true;
                //it's not added yet.
                if (string.IsNullOrEmpty(tdfi.aria2c_gid))
                {
                    res = _ariaHelper.aria2AddNewDownload(tdfi);
                    if (res == false)
                    {
                        tdfi.taskStatusUserInfoTextProperty = "cannot connect to background service when try to add download!";
                        tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Failed;
                    }
                    else
                    {
                        downloadingFileCountProperty++;
                        tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Downloading;
                    }
                }
                else
                {   // already has GID meaning it's already in the aria2c.
                    res = _ariaHelper.aria2UnPause(tdfi);
                    if (res == false)
                    {
                        downloadingFileCountProperty--;
                        tdfi.taskStatusUserInfoTextProperty = "cannot connect to background service when try to unpause!";
                        tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Failed;
                    }
                    else
                    {
                        downloadingFileCountProperty++;
                        tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Downloading;
                    }
                }

                return;
            }

            // class DownloadManager
            public void addAllListItemsToProcessQueue()
            {
                int tempPendingCount = 0;
                if (_processQueue.Count == 0)
                {
                    foreach (ToDownloadFileItem tdfi in toDownloadFileListProperty)
                    {
                        if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Pending)
                        {
                            // only enqueue this.downloadingFileCountProperty pending items , don't enqueue all
                            if ((tempPendingCount + downloadingFileCountProperty) < this._downloadConfig.maxFilesConcurrentDownload)
                            {
                                _processQueue.Enqueue(tdfi);
                                tempPendingCount++;
                            }
                        }
                        else if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Failed ||
                                  tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Done)
                        {
                            // only enqueue certain state items
                            _processQueue.Enqueue(tdfi);
                        }
                    }
                }
            }


            public void processAllDownloadListPendingItems()
            {
                while (_processQueue.Count >= 1)
                {
                    processDownloadListOnePendingItem();
                }
            }

            // class DownloadManager
            public void processDownloadListOnePendingItem()
            {
                if (_processQueue.Count == 0)
                {
                    return;
                }
                Console.WriteLine("_processQueue.Count is " + _processQueue.Count);

                ToDownloadFileItem tdfi = _processQueue.Peek();
                // at this time, this item may have been remove from the download list, need to check before start work on it
                if (!toDownloadFileListProperty.Contains(tdfi))
                {  // just ignore
                    _processQueue.Dequeue();
                    return;
                }
                if (tdfi == null)
                {   // no more
                    return;
                }

                if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Pending)
                {
                    Console.WriteLine("process one pending item");
                    if (downloadingFileCountProperty < _downloadConfig.maxFilesConcurrentDownload)
                    {
                        processOneToDownloadFileItem(tdfi);
                    }

                    // regardless, need to dequeue, otherwise block everything behind
                    // another option:  dequeue all pending stat items since we are already full

                    _processQueue.Dequeue();
                }
                else
                {
                    // tdfi with all other stats
                    _processQueue.Dequeue();

                    return;
                }

                return;
            }



            //while (toDownloadFileListProperty.Count >= 0)

            // class DownloadManager
            public void startProcessDownloadQueueAllItemsAsync()
            {
                _workerForDownloadList.RunWorkerAsync();
            }


            private int checkSpeedTimerCounter = 0;

            // class DownloadManager
            private void dowork_workerForStartProcessDownloadQueueAllItemsAsync(object sender, DoWorkEventArgs e)
            {
                //when this worker is busy , disable timer to query status
                // _timer_ariaTellStatus.Enabled = false;

                // only when it's empty queue, otherwise do nothing

                if (toDownloadFileListProperty.Count > 0)
                {
                    bool aria2cInitOK = aria2cInit();
                    if (!aria2cInitOK)
                    {
                        MessageBoxBig.show("即将关闭本软件...");
                        App.Current.Shutdown();
                    }
                }



                addAllListItemsToProcessQueue();

                processAllDownloadListPendingItems();
                // processDownloadListOnePendingItem();


                queryAria2cAllFilesStatus();


                addAllListItemsToProcessQueue();

                //_timer_ariaTellStatus.Enabled = true;
                const int ThreadSleepIntervalSecond = 3;
                const int CheckSpeedIntervalSecond = 60;

                //every 20*2 sec = 40 seconds, check speed
                if (checkSpeedTimerCounter * ThreadSleepIntervalSecond >= CheckSpeedIntervalSecond)
                {
                    Console.WriteLine("Checking scheduled time speed limit");
                    checkSpeedTimerCounter = 0;
                    setSpeedForCurrentTime();
                }

                checkSpeedTimerCounter++;
                System.Threading.Thread.Sleep(ThreadSleepIntervalSecond * 1000);
            }

            // class DownloadManager
            private void dowork_workerForStartProcessDownloadQueueAllItemsAsyncCompleted(object sender, RunWorkerCompletedEventArgs e)
            {
                // must be in main thread
                processAllFailedAndDoneItems();

                startProcessDownloadQueueAllItemsAsync();
            }

            private void processAllFailedAndDoneItems()
            {
                while (_processQueue.Count > 0)
                {
                    ToDownloadFileItem tdfi = _processQueue.Peek();

                    // debug only, remove from the download queue and add to completed queue
                    if (!DEBUG_TRUE_DOWNLOAD_TO_COMPLETE)
                    {
                        // =======================================
                        // debug only
                        // =======================================

                        tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Done;
                        // clean up the tdfi useless fields.
                        tdfi._downloadLinksResult = null;

                        tdfi.finishdatatime = DateTimeOffset.Now.ToString();

                        _completedFileList.add(tdfi);

                        removeOneDownloadItemFromDownloadList(tdfi);
                        if (_processQueue.Count > 0)
                        {
                            _processQueue.Dequeue();
                        }
                        _completedFileList.checkMd5Async(tdfi);

                        playCompletedSound();
                    }
                    else if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Failed)
                    {
                        Console.WriteLine("process one failed item");
                        if (tdfi.taskStatusString == ToDownloadFileItem.NETWORKERROR)
                        {
                            // file not found or something, retry won't help.
                        }
                        else
                        {
                            tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Pending;
                            this.moveToDownloadLast(tdfi);
                        }

                        _processQueue.Dequeue();
                    }
                    else if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Done)
                    {
                        Console.WriteLine("process one done item");
                        tdfi._downloadLinksResult = null;
                        tdfi.linkTimeStamp = DateTimeOffset.FromUnixTimeSeconds(0);

                        // remove from todownloadlist, add to completed list

                        tdfi.setGid(null);
                        // remove done item from todownload list and add to completed list
                        _completedFileList.add(tdfi);

                        // ==================================================
                        _completedFileList.checkMd5Async(tdfi);

                        // ==================================================

                        removeOneDownloadItemFromDownloadList(tdfi);
                        _processQueue.Dequeue();
                        playCompletedSound();
                    }
                    else
                    {
                        // important to return; or endless loop
                        _processQueue.Dequeue();
                    }
                }
            }


            private void playCompletedSound()
            {
                var typename = this.GetType();
                string nameOfNameSpace = typename.Namespace;

                string completesound = nameOfNameSpace + ".Resources.downloadcomplete.wav";
                Assembly assembly = Assembly.GetExecutingAssembly();
                var soundfilestream = assembly.GetManifestResourceStream(completesound);

                System.Media.SoundPlayer snd = new System.Media.SoundPlayer(soundfilestream);
                snd.Play();
            }


            // class DownloadManager
            private bool isToDownloadFileItemLinksExpired(ToDownloadFileItem tdfi)
            {
                bool linkExpired = true;
                if (tdfi._downloadLinksResult == null)
                {
                    //no link result, need to get links, so it's expired.
                    return true;
                }

                //Don't use dateTime obj , when it's deserialized, the datetime is using UTC, messed up the time.
                DateTimeOffset nowtime = DateTimeOffset.Now;

                TimeSpan span = nowtime.Subtract(tdfi.linkTimeStamp);

                if (span.TotalHours > DownloadManager.RefreshLinksAfterHours)
                {
                    linkExpired = true;
                }
                else
                {
                    linkExpired = false;
                }
                return linkExpired;
            }

            // class DownloadManager
            private bool getDownloadLinksForFile(ToDownloadFileItem tdfi)
            {
                DupanGetDownloadLinksResult linksResult = requestDownloadLinkFromServer(tdfi);

                if (linksResult == null)
                {
                    Console.WriteLine("getDownloadLinksForFile(): internal error, requestDownloadLinkFromServer() returned null ");
                    return false;
                }

                if (!string.IsNullOrEmpty(linksResult.httpTextWhenError))
                {
                    //error
                    Console.WriteLine("Cannot get download links, http result is " + linksResult.httpTextWhenError);
                    tdfi.taskStatusUserInfoTextProperty = linksResult.httpTextWhenError;
                    // need to set the result http text
                    tdfi._downloadLinksResult = linksResult;
                    return false;
                }

                // success
                tdfi._downloadLinksResult = linksResult;

                tdfi.linkTimeStamp = DateTimeOffset.Now;

                return true;
            }

            // class DownloadManager
            public DupanGetDownloadLinksResult requestDownloadLinkFromServer(ToDownloadFileItem tdfi)
            {
                DupanGetDownloadLinksResult downloadLinksResult = null;
                downloadLinksResult = new DupanGetDownloadLinksResult();

                if (tdfi.getLoginCredential() == null)
                {
                    downloadLinksResult.httpTextWhenError = "requestDownloadLinkFromServer() no login credential available for " + tdfi.filename;
                    return downloadLinksResult;
                }

                string getFileLinksUrl = AppConfigManager.DownloadConfigAndroid.buildGetDownloadLinksUrl(tdfi.remotePath);
                Console.WriteLine("get file links url is " + getFileLinksUrl);

                string useragent = _downloadConfig.userDefinedUserAgent;
                if (string.IsNullOrEmpty(useragent))
                {
                    useragent = AppConfigManager.DownloadConfigAndroid.UserAgent_Android;
                }

                //for windows version . string time = //time=1523815506 win ver need timestamp and a lot of other stuffs
                //const string urlGetDownloadLink_From_NetClientWinPart2 = " & devuid = BDIMXV2 % 2DO % 5FCD4E0E6ED11B492FACD1D022562FD6B9 % 2DC % 5F0 % 2DD % 5FFEB4075C080803257189 % 2DM % 5F902B3437EBA2 % 2DV % 5F8A14B64C & rand = 7e84c9ba313dbe467164571b56f520cdd5b189c8 & time = 1523339266 & vip = 0 & logid = JwAxADUAMgAzADMAMQA4ADAAOQA5ACwAMQA5ADIALgAxADYAOAAuADAALgAyADAAOAAsADgA % 0AMQA2ADQALAAxADAAMgAwADIAJwA % 3D & dp - logid = 1597314799930379817 HTTP / 1.1&path =";
                //&path =% 2FbaiduAutoSpeed % 2Esikuli % 2Erar

                string httpTextResult;


                if (DEBUG_ONLINE_GETDOWNLOADLINKS)
                {
                    try
                    {
                        WebClient wc = new WebClient();
                        wc.Encoding = System.Text.Encoding.UTF8;
                        wc.Headers.Add(HttpRequestHeader.Cookie, tdfi.getLoginCredential().fullCookie);
                        wc.Headers.Add(HttpRequestHeader.UserAgent, useragent);
                        httpTextResult = wc.DownloadString(getFileLinksUrl);
                        Console.WriteLine("Download links result is:\n " + httpTextResult);
                    }
                    catch (Exception ex)
                    {
                        // return downloadLinksResult = null means network error
                        downloadLinksResult.httpTextWhenError = ex.Message;
                        return downloadLinksResult;
                    }
                }
                else
                {
                    //for local file test only
                    httpTextResult = testReadFileList("d:\\baiduPanDown\\getfilelinksResult.txt");
                    // can only parse a file from console output.
                    System.Threading.Thread.Sleep(2000);
                }

                // have the http result text, try to parse it, sometimes it could be error text
                try
                {
                    // simulate a parsing error httpTextResult = asfadsfads + httpTextResult;
                    System.Web.Script.Serialization.JavaScriptSerializer jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    downloadLinksResult = jsonSerializer.Deserialize<DupanGetDownloadLinksResult>(httpTextResult);
                }
                catch (Exception)
                {
                    // have to pass the error http text out.
                    downloadLinksResult.httpTextWhenError = "cannot parse http result:" + httpTextResult;
                }

                return downloadLinksResult;
            }

            // class DownloadManager
            // only change order, do not start download, but also change pause, failed to pending.
            public void moveToDownloadFirst(ToDownloadFileItem tdfi)
            {
                if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Downloading)
                {
                    // even it's already downloading, still can move it to first
                }

                if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Pending)
                {
                }
                else if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Stop)
                {
                    tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Pending;
                }
                else if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Failed)
                {
                    tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Pending;
                }

                _toDownloadFileList.moveToFirst(tdfi);
            }

            // class DownloadManager
            // only change order, do not start or stop download, but also change pause, failed to pending.
            public void moveToDownloadLast(ToDownloadFileItem tdfi)
            {
                if (tdfi.taskStatus == ToDownloadFileItem.TaskStatus.Downloading)
                {
                    Console.WriteLine("although it's already downloading, cannot move to last, pause it first.");
                    return;
                }
                _toDownloadFileList.moveToLast(tdfi);
            }


            // class DownloadManager
            public void stopDownloadOneItem(ToDownloadFileItem tdfi)
            {
                if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Downloading)
                {
                    _ariaHelper.aria2Pause(tdfi);
                    //aria2c
                    tdfi.taskStatus = MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Stop;
                    downloadingFileCountProperty--;
                    moveToDownloadLast(tdfi);

                    Console.WriteLine("Stop download " + tdfi.filename);
                }
                else if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Pending)
                {
                    tdfi.taskStatus = MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Stop;
                    moveToDownloadLast(tdfi);
                }
                else if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Failed)
                {
                    tdfi.taskStatus = MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Stop;
                    moveToDownloadLast(tdfi);
                }
            }

            // class DownloadManager
            //
            public void startDownloadOneItem(ToDownloadFileItem tdfi)
            {
                if (tdfi == null)
                {
                    return;
                }

                if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Downloading)
                {
                    Console.WriteLine("Already downloading " + tdfi.filename);

                    // already downloading
                    return;
                }
                if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Stop ||
                tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Pending ||
                tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Failed
                )
                {
                    tdfi.taskStatus = MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Pending;

                    Console.WriteLine("Start download " + tdfi.filename);

                    if (downloadingFileCountProperty < _downloadConfig.maxFilesConcurrentDownload)
                    {
                        // should also move the item ahead, in front of any other pending / stop items
                        //==================================================
                        moveTdfiForwardBefore(tdfi, ToDownloadFileItem.TaskStatus.Stop);
                        moveTdfiForwardBefore(tdfi, ToDownloadFileItem.TaskStatus.Pending);
                        moveTdfiForwardBefore(tdfi, ToDownloadFileItem.TaskStatus.Failed);

                        //==================================================
                    }
                }
            }

            // move the item forward ,so it stays in front of the stop or pending items
            private void moveTdfiForwardBefore(ToDownloadFileItem tdfi, int stat)
            {
                ToDownloadFileItem firstTdfiOfStat = _toDownloadFileList.getFirstItemOfStatFromList(stat);
                int firstIndexOfStat = _toDownloadFileList.toDownloadFileListProperty.IndexOf(firstTdfiOfStat);
                if (firstIndexOfStat < 0)
                {   // not exist, so no need to move
                    return;
                }
                int currentIndex = _toDownloadFileList.toDownloadFileListProperty.IndexOf(tdfi);
                if (currentIndex > firstIndexOfStat)
                {
                    _toDownloadFileList.toDownloadFileListProperty.Move(currentIndex, firstIndexOfStat);
                }
            }


            // class DownloadManager
            private void removeOneDownloadItemFromAria(ToDownloadFileItem tdfi)
            {
                Aria2cHelper.Aria2cResult.TellStatusResult res = queryFileAriaStatus(tdfi);
                if (res != null)
                {
                    if (res.result.status.Equals(Aria2cStatus.ACTIVE))
                    {
                        _ariaHelper.aria2Remove(tdfi);
                    }
                    else if (res.result.status.Equals(Aria2cStatus.ERROR))
                    {
                        _ariaHelper.aria2RemoveDownloadResult(tdfi);
                    }
                    else if (res.result.status.Equals(Aria2cStatus.COMPLETED))
                    {
                        _ariaHelper.aria2RemoveDownloadResult(tdfi);
                    }
                }
            }

            // class DownloadManager
            public void removeOneDownloadItemFromDownloadList(ToDownloadFileItem tdfi)
            {
                Console.WriteLine("Removing a downloading item");

                if (!string.IsNullOrEmpty(tdfi.aria2c_gid))
                {
                    // not gid then no need to remove from aria2c
                    removeOneDownloadItemFromAria(tdfi);
                }

                if (tdfi.taskStatus == MyBaiduDownloader.ToDownloadFileItem.TaskStatus.Downloading)
                {
                    downloadingFileCountProperty--;
                    tdfi.taskStatus = ToDownloadFileItem.TaskStatus.Stop;
                    //  Already downloading need to stop first, or it's still in the processQueue to get links. "
                    Console.WriteLine("Already downloading need to stop first, or it's still in the processQueue to get links. " + tdfi.filename);

                    _toDownloadFileList.RemoveToDownloadFileItemToList(tdfi);

                    downloadQueueTotalFileSizeStringProperty = _toDownloadFileList.downloadQueueTotalFileSizeStringProperty;
                    // need to start the next pending item
                    //startProcessDownloadQueueAllItems();

                    // remove from _processQueue
                    return;
                }
                else
                {
                    _toDownloadFileList.RemoveToDownloadFileItemToList(tdfi);
                    downloadQueueTotalFileSizeStringProperty = _toDownloadFileList.downloadQueueTotalFileSizeStringProperty;
                }
                //saveDownloadTaskListSmart();
            }

            private Aria2cHelper.Aria2cResult.TellStatusResult queryFileAriaStatus(ToDownloadFileItem tdfi)
            {
                Aria2cHelper.Aria2cResult.TellStatusResult res = _ariaHelper.tellStatus(tdfi);
                return res;
            }

            // class DownloadManager
            public bool saveDownloadTaskList()
            {
                return _toDownloadFileList.saveDownloadTaskList();
            }

            // class DownloadManager
            public bool saveDownloadTaskListSmart()
            {
                return _toDownloadFileList.smartSave();
            }

            // class DownloadManager
            public void loadCompletedList()
            {
                this._completedFileList = CompletedFileList.load();
            }


            // class DownloadManager
            //  load and continue the alreadying running tasks
            public void loadDownloadTaskList()
            {
                this._toDownloadFileList = ToDownloadFileList.loadDownloadTaskList();

                if (_toDownloadFileList != null)
                {
                    // change all downloading to pending.
                    do
                    {
                        ToDownloadFileItem firstTdfi = _toDownloadFileList.getFirstItemOfStatFromList(ToDownloadFileItem.TaskStatus.Downloading);
                        if (firstTdfi == null)
                        {
                            // no more
                            break;
                        }
                        firstTdfi.taskStatus = ToDownloadFileItem.TaskStatus.Pending;
                    } while (true);
                }
                downloadingFileCountProperty = 0;
            }



            // class DownloadManager
            public void testAria2c()
            {
                _ariaHelper.aria2cGetVersion();
                _ariaHelper.aria2cGetGlobalOption();
                _ariaHelper.setGlobalSpeedLimit("20k");
            }

            // class DownloadManager
            //=================================================================================
            private class Aria2cHelper
            {
                private AppConfigManager.DownloadConfigAndroid _downloadConfig;
                private Aria2cConfig _aria2cConfig;

                public Aria2cHelper(AppConfigManager.DownloadConfigAndroid downloadconfig)
                {
                    _downloadConfig = downloadconfig;
                    _aria2cConfig = new Aria2cConfig(downloadconfig);
                }

                public bool createAria2cExeFile()
                {
                    bool res = true;
                    var typename = this.GetType();

                    string nameOfNameSpace = typename.Namespace;
                    //note, if without a ext name like "bin"  source tree will likely ignore the file to stage and commit
                    string Aria2cResourceName = nameOfNameSpace + ".Resources.aria2cDotExe.bin";
                    Assembly assembly = Assembly.GetExecutingAssembly();

                    try
                    {
                        var inputStream = assembly.GetManifestResourceStream(Aria2cResourceName);

                        FileStream outputStream = File.OpenWrite(MyBaiduDownloader.DownloadManager.Aria2cHelper.Aria2cConfig.Aria2cexeFullPath);

                        inputStream.CopyTo(outputStream);

                        inputStream.Close();

                        outputStream.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Can not create aria2c file. " + ex);
                        res = false;
                    }
                    return res;
                }

                // destructor to closeo the aria2c app exe.
                ~Aria2cHelper()
                {
                    this.closeAria2c();

                    if (File.Exists(Aria2cConfig.Aria2cexeFullPath))
                    {
                        try
                        {
                            File.Delete(Aria2cConfig.Aria2cexeFullPath);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("cannot delete aria2c file, run by other process ?");
                        }
                    }
                }

                // https://aria2.github.io/manual/en/html/   online manual
                private WebClient _webclient = new WebClient();  // for rpc

                private class Aria2cConfig
                {
                    // aria2c.exe”, “--enable-rpc=true --rpc-allow-origin-all=true --rpc-listen-port=50000 --conf-path=aria2.conf -q --disable-ipv6=true --rpc-secret=” ＋ aria2c_secret , , 1)
                    //K:\GreenSoft\aria2c

                    public const string Aria2cexeFullPath = "aria2c.exe";

                    private const ulong Aria2cexeSize = 4874240;  // size of aria2 1.33.1 win64

                    public const string Aria2cPort = "39000";
                    private const string RpcListenPortKeyword = "rpc-listen-port";
                    private const string MaxOverallDownloadLimitKeyword = "max-overall-download-limit";

                    private const string AutoSaveIntervalKeyword = "auto-save-interval";

                    private const string AutoSaveIntervalSecond = "40";

                    private const string DiskCacheSizeOptionKeyword = "disk-cache";
                    private const string SplitKeyword = "split";
                    private const string MaxConnectionPerServerKeyword = "max-connection-per-server";
                    public const string Aria2cMaxConcurrentDownloadKeyword = "max-concurrent-downloads";
                    public const string UserAgentKeyword = "user-agent";
                    public const string RpcSecretKeyWord = "rpc-secret";
                    public const string RpcSecret = "S_up#ze%rzP*aQ,n.!D?o(wn_z-a=q3;1235z#";
                    public const string EnableRpcKeyword = "enable-rpc";
                    public const string DirKeyword = "dir";
                    public const string RpcAllowOriginAllKeyword = "rpc-allow-origin-all";
                    public const string DisableIpV6Keyword = "disable-ipv6";
                    public const string ContinueKeyword = "continue";
                    public const string RpcListenAllKeyword = "rpc-listen-all";
                    public const string MinSpliteSize = "min-split-size";
                    public const string ContentDispostionDefaultUtf8 = "content-disposition-default-utf8";


                    static private string commandOptions = "";

                    //+ " " + aria2cMaxConcurrentDownload + _downloadConfig.maxFilesConcurrentDownload;
                    public const string aria2cbase = "http://127.0.0.1:" + Aria2cPort + "/jsonrpc";

                    public string useragent;

                    private AppConfigManager.DownloadConfigAndroid _downloadConfig;

                    //Aria2cHelper
                    public Aria2cConfig(AppConfigManager.DownloadConfigAndroid downloadconfig)
                    {
                        _downloadConfig = downloadconfig;
                        useragent = _downloadConfig.userDefinedUserAgent;
                        if (string.IsNullOrEmpty(useragent))
                        {
                            useragent = AppConfigManager.DownloadConfigAndroid.UserAgent_Android;
                        }
                    }

                    //Aria2cConfig
                    static private void buildCommandOptions(string key, string value)
                    {
                        commandOptions = commandOptions + " --" + key + "=" + value;
                    }

                    //Aria2cConfig
                    static private void buildCommandOptions(string key, int value)
                    {
                        commandOptions = commandOptions + " --" + key + "=" + value.ToString();
                    }

                    //Aria2cConfig
                    static private void buildCommandOptions(string key, uint value)
                    {
                        commandOptions = commandOptions + " --" + key + "=" + value.ToString();
                    }


                    string processPath(string path)
                    {
                        string root = Path.GetPathRoot(path);
                        string pathWithSpace = Path.GetDirectoryName(path);
                        string outputpath = "";
                        if (string.IsNullOrEmpty(pathWithSpace))
                        {
                            outputpath = root;
                        }
                        else
                        {
                            outputpath = Path.Combine(root, pathWithSpace);
                        }
                        return outputpath;
                    }
                    //Aria2cConfig
                    public string getCommandOptions()
                    {
                        buildCommandOptions(AutoSaveIntervalKeyword, AutoSaveIntervalSecond);
                        buildCommandOptions(DiskCacheSizeOptionKeyword, "64M");
                        //   buildCommandOptions(DirKeyword, "\""+_downloadConfig.saveFileLocalPath+"\"");

                        // buildCommandOptions(DirKeyword, _downloadConfig.saveFileLocalPath);

                        string path = processPath(_downloadConfig.saveFileLocalPath);
                        buildCommandOptions(DirKeyword, path);


                        //buildCommandOptions(DirKeyword, "f:\"\\abc abd\"");

                        buildCommandOptions(DirKeyword, "f:\"\\abc abd\"");


                        buildCommandOptions(EnableRpcKeyword, "true");
                        buildCommandOptions(RpcSecretKeyWord, RpcSecret);
                        buildCommandOptions(RpcListenPortKeyword, Aria2cPort);

                        buildCommandOptions(RpcAllowOriginAllKeyword, "true");
                        buildCommandOptions(DisableIpV6Keyword, "true");

                        buildCommandOptions(ContinueKeyword, "true");
                        buildCommandOptions(RpcListenAllKeyword, "false");
                        buildCommandOptions(Aria2cMaxConcurrentDownloadKeyword, _downloadConfig.maxFilesConcurrentDownload);
                        buildCommandOptions(MaxConnectionPerServerKeyword, _downloadConfig.maxConnectionPerServer);
                        buildCommandOptions(SplitKeyword, _downloadConfig.split);
                        buildCommandOptions(UserAgentKeyword, useragent);
                        buildCommandOptions(MaxOverallDownloadLimitKeyword, _downloadConfig.getSpeedFullNumberAndUnit());
                        buildCommandOptions(MinSpliteSize, "1048576"); // Min split size is 1M

                        // important to set true, otherwise Chinese filename would be junk
                        buildCommandOptions(ContentDispostionDefaultUtf8, "true");


                        return commandOptions;
                    }
                }

                // end of Aria2cConfig  ===================

                //Aria2cHelper
                private Process _aria2cProcess = null;

                //Aria2cHelper
                public bool runAria2c()
                {
                    if (!DEBUG_RUN_ARIA2C)
                    {
                        return true;
                    }

                    bool res = createAria2cExeFile();
                    if (!res)
                    {
                        // failed to create
                        return res;
                    }
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    //startInfo.CreateNoWindow = true;  useless
                    startInfo.FileName = Aria2cConfig.Aria2cexeFullPath;

                    string options = _aria2cConfig.getCommandOptions();
                    // Console.WriteLine("aria2c starting option is  "+ options);

                    startInfo.Arguments = options;
                    if (DEBUG_HIDE_ARIA2C_WINDOW)
                    {
                        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    }
                    try
                    {
                        _aria2cProcess = Process.Start(startInfo);
                    }
                    catch (Exception)
                    {
                        MessageBoxBig.show("aria is missing.");
                    }

                    // in case some parameter is wrong, aria2c would exit right away
                    System.Threading.Thread.Sleep(2000);
                    if (_aria2cProcess.HasExited)
                    {

                        return false;
                    }

                    return true;
                }

                //Aria2cHelper
                public void closeAria2c()
                {
                    if (_aria2cProcess != null)
                    {
                        if (!_aria2cProcess.HasExited)
                        {
                            // _aria2cProcess.CloseMainWindow();
                            // _aria2cProcess.Close();
                            _aria2cProcess.Kill(); // async


                            // if don't wait, then it can not be delted.
                            _aria2cProcess.WaitForExit();
                        }
                    }
                }

                //Aria2cHelper
                public class Aria2cResult
                {
                    //"errorCode":"24","errorMessage":"Authorization failed."
                    public const int ErrorCode24 = 24; //  link expired, need refresh links, Authorization failed.

                    // "errorCode":"1","errorMessage":"Network problem has occurred. cause:
                    // No connection could be made because the target machine actively refused it.
                    public const int ErrorCode1 = 1;

                    // sometimes result is just a string, gid
                    public class GidResult
                    {
                        public string result;
                    }

                    public class TellStatusResult
                    {
                        //public string id;

                        public class TellStatusResultData
                        {
                            public UInt64 completedLength;
                            public string downloadSpeed;
                            public int errorCode;
                            public string errorMessage;
                            public string status;
                        }

                        public TellStatusResultData result;
                    }
                }

                //Aria2cHelper
                private class Aria2cJsonObject
                {
                    public string jsonrpc = "2.0";
                    public string id = "qwer";
                    public string method { get; set; }
                    private string token = Aria2cConfig.RpcSecret;

                    public const string KeyGlobalOptionMaxDownloadSpeed = "max-overall-download-limit";
                    public const string KeyGlobalOptionMaxConcurrentDownloads = "max-concurrent-downloads";

                    public const string KeyToken = "token";

                    public List<Object> @params = new List<Object>();

                    // options is private so it won't show up in the json itself, contents only show up inside the params.
                    private Dictionary<string, string> options;

                    private List<string> _list;

                    //Aria2cJsonObject
                    public void setOption(string option, string value)
                    {
                        if (options == null)
                        {
                            options = new Dictionary<string, string>();

                            @params.Add(options);
                        }
                        options.Add(option, value);
                    }

                    //Aria2cJsonObject
                    public void addParams(string param)
                    {
                        @params.Add(param);
                    }

                    //Aria2cJsonObject
                    public void addParams(string param, string value)
                    {
                        @params.Add(param + ":" + value);
                    }

                    //Aria2cJsonObject
                    public void addParams(List<string> list)
                    {
                        @params.Add(list);
                    }

                    //Aria2cJsonObject
                    public void addUrl(string url)
                    {
                        if (_list == null)
                        {
                            _list = new List<string>();
                            addParams(_list);
                        }
                        _list.Add(url);
                    }

                    //Aria2cJsonObject
                    public void addGid(string gid)
                    {
                        /* if (_list == null)
                         {
                             _list = new List<string>();
                             addParams(_list);
                         }
                         _list.Add(gid);
                         */

                        addParams(gid);
                    }

                    //Aria2cJsonObject
                    // constructor Aria2cJsonObject
                    public Aria2cJsonObject()
                    {
                        addParams(KeyToken, Aria2cConfig.RpcSecret);
                    }

                    //Aria2cJsonObject
                    //                    private JavaScriptSerializer _serializer = new JavaScriptSerializer();

                    //Aria2cJsonObject
                    public string getJsonText()
                    {
                        string jsontext = _serializer.Serialize(this);
                        //Console.WriteLine("json text is: " + jsontext);
                        return jsontext;
                    }
                }

                //Aria2cJsonObject ===================================

                private static JavaScriptSerializer _serializer = new JavaScriptSerializer();

                private string rpcFunctionCall(Aria2cJsonObject json)
                {
                    Console.WriteLine("rpcFunctionCall for " + json.method);
                    string httpTextResult = null;
                    string callText = json.getJsonText();
                    if (DEBUG_PRINT_RPC_TEXT)
                    {
                        Console.WriteLine(json.method + " rpc call text:" + callText);
                    }
                    try
                    {
                        // very important !
                        _webclient.Encoding = System.Text.Encoding.UTF8;
                        httpTextResult = _webclient.UploadString(Aria2cConfig.aria2cbase, callText);
                        if (DEBUG_PRINT_RPC_TEXT)
                        {
                            Console.WriteLine(json.method + " rpc result text: " + httpTextResult);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("exception, e " + e);
                        Console.WriteLine(" cannot connect to RPC service.");
                        return httpTextResult;
                    }

                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        Console.WriteLine(json.method + " cannot get reply.");
                        return httpTextResult;
                    }

                    Console.WriteLine("done: rpcFunctionCall for " + json.method);

                    return httpTextResult;
                }

                //Aria2cHelper
                public Aria2cResult.TellStatusResult tellStatus(ToDownloadFileItem tdfi)
                {
                    if (string.IsNullOrEmpty(tdfi.aria2c_gid))
                    {
                        Console.WriteLine("can not run TellStatus for " + tdfi.filename + " because GID is null");
                        return null;
                    }

                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.tellStatus";

                    json.addGid(tdfi.aria2c_gid);

                    string httpTextResult;
                    httpTextResult = rpcFunctionCall(json);
                    Aria2cResult.TellStatusResult statusResult;
                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        Console.WriteLine("no httptext result for tellStatus");
                        return null;
                    }
                    else
                    {
                        statusResult = _serializer.Deserialize<Aria2cResult.TellStatusResult>(httpTextResult);
                    }
                    return statusResult;
                }

                //Aria2cHelper
                public bool setGlobalSpeedLimit(string speed)
                {
                    Console.WriteLine("setGlobalSpeedLimit..." + speed);

                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.changeGlobalOption";
                    json.setOption(Aria2cJsonObject.KeyGlobalOptionMaxDownloadSpeed, speed);

                    string httpTextResult;
                    httpTextResult = rpcFunctionCall(json);
                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        Console.WriteLine("setGlobalSpeedLimit error. ");
                        return false;
                    }
                    return true;
                }

                //Aria2cHelper
                public bool setGlobalMaxConcurrentDownload(int concurrent)
                {
                    Console.WriteLine("setGlobalMaxConcurrentDownload..." + concurrent);

                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.changeGlobalOption";
                    json.setOption(Aria2cJsonObject.KeyGlobalOptionMaxConcurrentDownloads, concurrent.ToString());

                    string httpTextResult;
                    httpTextResult = rpcFunctionCall(json);
                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        Console.WriteLine("setGlobalMaxConcurrentDownload error. ");
                        return false;
                    }
                    return true;
                }

                // only remove normal item, not error items
                public bool aria2Remove(ToDownloadFileItem tdfi)
                {
                    Console.WriteLine("aria2Remove " + tdfi.filename);
                    if (string.IsNullOrEmpty(tdfi.aria2c_gid))
                    {
                        Console.WriteLine("can not run Remove for " + tdfi.filename + " because GID is null");
                        return false;
                    }
                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.remove";
                    json.addGid(tdfi.aria2c_gid);
                    string httpTextResult = rpcFunctionCall(json);

                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        return false;
                    }

                    Aria2cResult.GidResult result = _serializer.Deserialize<Aria2cResult.GidResult>(httpTextResult);
                    Console.WriteLine(" result " + result);
                    return true;
                }

                // aria2c's pause is my stop
                public bool aria2Pause(ToDownloadFileItem tdfi)
                {
                    Console.WriteLine("aria2Pause " + tdfi.filename);
                    if (string.IsNullOrEmpty(tdfi.aria2c_gid))
                    {
                        Console.WriteLine("can not run Pause for " + tdfi.filename + " because GID is null");
                        return false;
                    }
                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.pause";
                    json.addGid(tdfi.aria2c_gid);
                    string httpTextResult = rpcFunctionCall(json);

                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        return false;
                    }

                    Aria2cResult.GidResult result = _serializer.Deserialize<Aria2cResult.GidResult>(httpTextResult);
                    Console.WriteLine(" result " + result);
                    return true;
                }

                // aria2c's Unpause is my play (with gid, it's resume)
                public bool aria2UnPause(ToDownloadFileItem tdfi)
                {
                    Console.WriteLine("aria2UnPause " + tdfi.filename);
                    if (string.IsNullOrEmpty(tdfi.aria2c_gid))
                    {
                        Console.WriteLine("can not run UnPause for " + tdfi.filename + " because GID is null");
                        return false;
                    }

                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.unpause";
                    json.addGid(tdfi.aria2c_gid);
                    string httpTextResult = rpcFunctionCall(json);

                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        return false;
                    }

                    Aria2cResult.GidResult result = _serializer.Deserialize<Aria2cResult.GidResult>(httpTextResult);
                    Console.WriteLine(" result " + result);
                    return true;
                }

                // to remove failed items / stopped / completed item, use this
                public bool aria2RemoveDownloadResult(ToDownloadFileItem tdfi)
                {
                    Console.WriteLine("aria2Remove " + tdfi.filename);
                    if (string.IsNullOrEmpty(tdfi.aria2c_gid))
                    {
                        Console.WriteLine("can not run RemoveDownloadResult for " + tdfi.filename + " because GID is null");
                        return false;
                    }

                    // aria2.remove([secret,] gid)
                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.removeDownloadResult";

                    json.addGid(tdfi.aria2c_gid);
                    string httpTextResult = rpcFunctionCall(json);

                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        return false;
                    }

                    Aria2cResult.GidResult result = _serializer.Deserialize<Aria2cResult.GidResult>(httpTextResult);

                    Console.WriteLine(" result " + result);
                    return true;
                }



                //Aria2cHelper
                public bool aria2AddNewDownload(ToDownloadFileItem tdfi)
                {
                    Console.WriteLine("aria2AddNewDownload " + tdfi.filename);
                    if (!string.IsNullOrEmpty(tdfi.aria2c_gid))
                    {   // this one is different than others, must not have gid. only null is OK.
                        Console.WriteLine("can not AddNewDownload for " + tdfi.filename + " because it already has GID " + tdfi.aria2c_gid);
                        return false;
                    }
                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.addUri";

                    int i;
                    int linksCount = tdfi._downloadLinksResult.urls.Count;

                    for (i = 0; i < linksCount; i++)
                    {
                        string url = (tdfi._downloadLinksResult.urls[i] as DupanGetDownloadLinksResult.Urls).url;

                        if (!DEBUG_USE_TRUE_DOWNLOAD_LINKS)
                        {
                            url = "http://127.0.0.1/asdfasdfadsfdsafdsafdsafdasfadsfds" + i;
                        }
                        // json.addUrl(unicodeToUtf8(url));a
                        json.addUrl(url);
                    }

                    json.setOption("dir", tdfi.localPath);

                    Console.WriteLine("Save path is " + tdfi.localPath);

                    string httpTextResult;

                    httpTextResult = rpcFunctionCall(json);

                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        return false;
                    }

                    Aria2cResult.GidResult result = _serializer.Deserialize<Aria2cResult.GidResult>(httpTextResult);

                    string gid = result.result;

                    tdfi.setGid(gid);

                    return true;
                }

                //Aria2cHelper
                public bool aria2cGetVersion()
                {
                    Aria2cJsonObject json = new Aria2cJsonObject();

                    json.method = "aria2.getVersion";

                    string httpTextResult;
                    httpTextResult = rpcFunctionCall(json);

                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        return false;
                    }
                    return true;
                }

                //Aria2cHelper
                public bool aria2cGetGlobalOption()
                {
                    Aria2cJsonObject json = new Aria2cJsonObject();
                    json.method = "aria2.getGlobalOption";
                    string httpTextResult;
                    httpTextResult = rpcFunctionCall(json);

                    if (string.IsNullOrEmpty(httpTextResult))
                    {
                        return false;
                    }
                    return true;
                }
            }  // end of   class Aria2cHelper

            //=================================================================================

            private string _downloadQueueTotalSpeedString = "0";

            public string downloadQueueTotalSpeedStringProperty
            {
                get { return _downloadQueueTotalSpeedString; }

                set
                {
                    try
                    {
                        _downloadQueueTotalSpeedString = sizeByteIntToSizeHuman(UInt64.Parse(value));
                    }
                    catch (Exception)
                    {
                        _downloadQueueTotalSpeedString = "";
                    }
                    OnPropertyChanged("downloadQueueTotalSpeedStringProperty");
                }
            }

            private string _downloadQueueTotalFileSizeStringProperty;

            public string downloadQueueTotalFileSizeStringProperty
            {
                get
                {
                    return _toDownloadFileList.downloadQueueTotalFileSizeStringProperty;
                }
                set
                {
                    _downloadQueueTotalFileSizeStringProperty = value;
                    OnPropertyChanged("downloadQueueTotalFileSizeStringProperty");
                }
            }


            // class DownloadManager
            private int _downloadingFileCount = 0;

            public int downloadingFileCountProperty
            {
                get { return _downloadingFileCount; }
                set
                {
                    _downloadingFileCount = value;
                    OnPropertyChanged("downloadingFileCountProperty");
                }
            }


            // class DownloadManager
            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(string propertyName = null)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }
        } // end of class DownloadManager

        //=======================================================================================================================
        public class ToDownloadFileItem : INotifyPropertyChanged
        {
            public class TaskStatus
            {
                public const int Pending = 0; // waiting to start , due to max download file at sametime is limited.
                public const int Downloading = 1; // downloading
                public const int Stop = 2; // stop, do not resume download even nothing else is downloading
                public const int Done = 3; // Done, do not download even nothing else is downloading
                public const int Failed = 4; // Fail, do not download even nothing else is downloading
                public const int Md5Failed = 5; // MD5 not match, but don't worry, doesn't necessary mean the file is bad
                public const int Md5Passed = 6; // MD5 passed
                public const int File_Missing = 7; // local file missing
            }


            // constructor of ToDownloadFileItem
            public ToDownloadFileItem(RemoteFileItem rfi, string savepath, string account_Uid)
            {
                filename = rfi.filename;
                localPath = savepath;

                size = rfi.size;
                sizeHuman = rfi.sizeHuman;
                remotePath = rfi.path;
                serverMd5 = rfi.md5;
                _loginCredential = null;
                this.accountUid = account_Uid;
                DupanLoginCredential login = getLoginCredential();
                if (login != null)
                {
                    account = login.username;
                }
                else
                {
                    account = "";
                }
                //this._loginCredential = _appConfigData._loginCredentialConfig._loginCredentialsDict[account_Uid] ;

                this.taskStatus = TaskStatus.Pending;
                //for test only
                progress = 0;
            }

            // constructor
            public ToDownloadFileItem()
            {
                // this is needed for json serialize
            }
            //ToDownloadFileItem
            public string finishdatatime { get; set; }
            private DupanLoginCredential _loginCredential;

            public DupanLoginCredential getLoginCredential()
            {
                if (_loginCredential == null)
                {
                    try
                    {
                        _loginCredential = _appConfigData._loginCredentialConfig._loginCredentialsDict[accountUid];
                        //_loginCredential = _appConfigData._loginCredentialConfig._loginCredentialsDict["asdfc"];
                    }
                    catch (Exception ex)
                    {
                        _loginCredential = null;
                        //MessageBoxBig.show("用户信息 "+accountUid+ " 不存在, 已经被删除了?");
                        Console.WriteLine("用户信息 " + accountUid + " 不存在, 已经被删除了?");
                    }
                }
                return _loginCredential;
            }

            public string accountUid { get; set; }

            public string filename { get; set; }
            [ScriptIgnore]
            public string sizeHuman { get; set; }

            public string remotePath { get; set; }
            public string serverMd5 { get; set; }

            public string localfullpathfilename { get { return Path.Combine(this.localPath, this.filename); } }

            // class ToDownloadFileItem
            private string remotePathShort
            {
                get { return Path.GetDirectoryName(remotePath); }
            }

            // class ToDownloadFileItem
            public void openFolder()
            {
                string argument;
                // if file exists, open explorer and select it.
                if (System.IO.File.Exists(this.localfullpathfilename))
                {
                    argument = " /select,  \"" + this.localfullpathfilename + "\"";
                }
                else
                {
                    // file not exist, only open the dir
                    argument = Path.GetDirectoryName(this.localfullpathfilename);
                }

                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();

                // Console.WriteLine("argument " + argument);

                startInfo.FileName = "explorer";
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal;
                startInfo.Arguments = argument;
                System.Diagnostics.Process.Start(startInfo);
            }

            // class ToDownloadFileItem
            // should not save this to diskfile because gid is meaningless after app close and aria2c restart.
            [ScriptIgnore]
            public string aria2c_gid { get; private set; }
            [ScriptIgnore]
            public string taskTextInfo { get; set; }

            public void setGid(string gid)
            {
                aria2c_gid = gid;
            }

            // for result from tellstatus
            //public int errorCode;

            public string localPath { get; set; }
            private ulong _size;
            public ulong size
            {
                get { return _size; }
                set
                {
                    _size = value;
                    sizeHuman = sizeByteIntToSizeHuman(value);
                }
            }

            //string _taskStatusUserInfoText;
            public const string NETWORKERROR = "Network Error";

            public const string SERVERERROR = "Server ERROR";


            private string _localMd5;

            public string localMd5
            {
                get { return _localMd5; }
                set
                {
                    _localMd5 = value;
                    OnPropertyChanged("localMd5");
                }
            }


            private string _taskStatusUserInfoText;

            public string taskStatusUserInfoTextProperty
            {
                get
                {
                    return _taskStatusUserInfoText;
                }
                set
                {
                    _taskStatusUserInfoText = value;
                    OnPropertyChanged("taskStatusUserInfoTextProperty");
                }
            }

            private int _taskStatus;

            public int taskStatus
            {
                get { return _taskStatus; }
                set
                {
                    _taskStatus = value;

                    //also set the status text property
                    string text = "unknown";
                    switch (_taskStatus)
                    {
                        case TaskStatus.Pending:
                            text = nameof(TaskStatus.Pending);
                            break;

                        case TaskStatus.Downloading:
                            text = nameof(TaskStatus.Downloading);
                            break;

                        case TaskStatus.Stop:
                            text = nameof(TaskStatus.Stop);
                            break;

                        case TaskStatus.Done:
                            text = nameof(TaskStatus.Done);
                            break;

                        case TaskStatus.Failed:
                            text = nameof(TaskStatus.Failed);
                            break;

                        case TaskStatus.Md5Failed:
                            text = "Md5 Failed";

                            break;

                        case TaskStatus.Md5Passed:
                            text = "Md5 Passed";
                            break;

                        case TaskStatus.File_Missing:
                            text = "File Missing";
                            break;

                        default:
                            text = _taskStatus.ToString();
                            break;
                    };

                    taskStatusString = text;
                    OnPropertyChanged("taskStatus");
                }
            }

            private string _taskStatusString;

            public string taskStatusString
            {
                get
                {
                    return _taskStatusString;
                }
                internal set
                {
                    _taskStatusString = value;
                    OnPropertyChanged("taskStatusString");
                }
            }

            // for display in the download task list only. so user know the download is by which account
            // so can limit the download amount for one account in one day, not to be too much, otherwise could be blocked by Baidu
            public string account { get; set; }

            private string _speedString;

            [ScriptIgnore]
            public string speedString
            {
                get { return _speedString; }
                internal set
                {
                    if (value != null && value.Equals("0"))
                    {
                        _speedString = "";
                    }
                    else
                    {
                        try
                        {
                            _speedString = sizeByteIntToSizeHuman(UInt64.Parse(value));
                        }
                        catch (Exception)
                        {
                            _speedString = value;
                        }
                    }
                    OnPropertyChanged("speedString");
                }
            }

            private UInt64 _completedLength;
            [ScriptIgnore]
            public UInt64 completedLength
            {
                get { return _completedLength; }
                set
                {
                    _completedLength = value;
                    progress = (int)(_completedLength * 100.0 / size);
                }
            }

            // percentage
            private int _progress;
            //[ScriptIgnore]
            public int progress
            {
                get { return _progress; }
                set
                {
                    _progress = value;
                    OnPropertyChanged("progress");
                }
            }

            public DateTimeOffset linkTimeStamp;

            //when it's being saved in downloadlist, the list is still empty, so it won't be saved, it's good, we dont' need to save the urls, they expire over time
            //whe
            public DupanGetDownloadLinksResult _downloadLinksResult { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(string propertyName = null)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }; // end of ToDownloadFileItem

        // class MyBaiduDownloader
        // need this to update GUI when filelist items changes

        public ObservableCollection<RemoteFileItem> remoteFileListProperty
        {
            get { return _remoteFileList; }

            set
            {
                _remoteFileList = value;
                OnPropertyChanged("remoteFileListProperty");
            }
        } // a filelist for current folder to show.

        // class MyBaiduDownloader

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}// end of class MyBaiduDownloader

/*
 *  to do list
 *  1. delete animation .... ???
 *  2. get link error report  done
 *  3. network error need keep retry   done
 *  4. aria2c
 *  5. download config save load
 *  6. download queue save load   done 90%
 *  7  load download list, then need to start download
 **/
