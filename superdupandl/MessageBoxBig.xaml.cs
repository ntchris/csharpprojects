﻿using System.Windows;
using System.Windows.Controls;

namespace superdupandl
{
    /// <summary>
    /// Interaction logic for MessageBoxBig.xaml
    /// </summary>
    public partial class MessageBoxBig : Window
    {
        private static MessageBoxBig _messageBoxWin;
        private MessageBoxResult _result;

        private const int TextLenToSetBiggerWindow = 160;
        private const int TextLenToShowScrollBar = 1024;
        private const int DefaultSmallWindowWidth = 600;
        private const int LargerWindowWidth = 850;

        public MessageBoxBig()
        {
            InitializeComponent();
            Owner = App.Current.MainWindow;
        }

        static public MessageBoxResult show(string message, string title = "", MessageBoxButton button = MessageBoxButton.OK)
        {
            if (_messageBoxWin == null)
            {
                _messageBoxWin = new MessageBoxBig();
            }

            if (message.Length > TextLenToSetBiggerWindow)
            {
                _messageBoxWin.Width = LargerWindowWidth;
            }
            else
            {
                _messageBoxWin.Width = DefaultSmallWindowWidth;
            }

            if (message.Length > MessageBoxBig.TextLenToShowScrollBar)
            {
                _messageBoxWin.scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Visible;
            }
            else
            {
                _messageBoxWin.scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
            }
            _messageBoxWin.Title = title;
            _messageBoxWin.textblockMessage.Text = message;

            if (button == MessageBoxButton.OKCancel)
            {
                _messageBoxWin.button_Cancel.Visibility = Visibility.Visible;
                _messageBoxWin.button_Cancel.IsDefault = true;
                _messageBoxWin.button_Cancel.IsCancel = true;
            }
            else
            {
                //  == MessageBoxButton.OK or everything else, all treat as OK
                _messageBoxWin.button_Cancel.Visibility = Visibility.Collapsed;
                _messageBoxWin.button_OK.IsDefault = true;
                _messageBoxWin.button_OK.IsCancel = true;
            }

            _messageBoxWin.ShowDialog();

            MessageBoxResult res = _messageBoxWin._result;

            _messageBoxWin = null;

            return res;
        }

        private void button_OK_Click(object sender, RoutedEventArgs e)
        {
            _result = MessageBoxResult.OK;
            Visibility = Visibility.Collapsed;
            //Close();
        }

        private void button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            _result = MessageBoxResult.Cancel;
            Visibility = Visibility.Collapsed;
            //Close();
        }
    }
}